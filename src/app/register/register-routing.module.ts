import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AgreementComponent } from "./agreement/agreement.component";
import { RegisterComponent } from "./register.component";

const routes: Routes = [
    { path: 'jobseeker', component: RegisterComponent},
    { path: 'company', component: RegisterComponent},
    { path: 'agreement', component:  AgreementComponent}
]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]

})
export class RegisterRoutingModule {}