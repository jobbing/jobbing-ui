import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { HeaderComponent } from "../shared/header/header.component";
import { PublicHeaderComponent } from "../shared/public-header/public-header.component";
import { SharedModule } from "../shared/shared.module";
import { RegisterFormComponent } from "./register-form/register-form.component";
import { RegisterRoutingModule } from "./register-routing.module";
import { RegisterComponent } from "./register.component";
import { AgreementComponent } from './agreement/agreement.component';

@NgModule({
    imports: [
        ReactiveFormsModule,
        RegisterRoutingModule,
        CommonModule,
        SharedModule
    ],
    declarations: [
        RegisterComponent,
        RegisterFormComponent,
        AgreementComponent
    ],
    providers:[
        HeaderComponent,
        PublicHeaderComponent
        
    ]

})
export class RegisterModule { }