import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AuthService } from 'src/app/core/auth/auth.service';
import { AuthActions, fromAuthStore } from 'src/app/core/auth/store';
import { HolderType } from 'src/app/core/model/holder-type';
import { NotificationService } from 'src/app/core/notification/notification.service';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {

  registerForm!: FormGroup;
  isCompany = false;
  holderType: HolderType;
  passwordMatch = true;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<fromAuthStore.AuthState>,
    private router: Router
  ) {
    this.createForm();
    this.holderType = HolderType.COMPANY;
    this.setHoldertype();
   }

  ngOnInit(): void {
    if(this.router.url.includes("jobseeker")){
      this.isCompany = false;
    }
    else if(this.router.url.includes("company")){
      this.isCompany = true;
    }
  }

  navigateToAgreement(){
    this.router.navigate(["register/agreement"]);
  }

  register(){
    if(this.registerForm.get("password")?.value == this.registerForm.get("confirmPassword")?.value){
      this.store.dispatch(
          AuthActions.register(
            {register: {
              name: this.registerForm.get("name")?.value,
              companyName: this.registerForm.get("companyName")?.value,
              username: this.registerForm.get("username")?.value,
              email: this.registerForm.get("email")?.value,
              password: this.registerForm.get("password")?.value,
              },
              holderType: this.holderType
            }));
    }
    else{
      this.passwordMatch = false;
    }
  }

  private createForm(){
    this.registerForm = this.formBuilder.group({
      companyName: new FormControl(null),
      name: new FormControl(null , [Validators.required]),
      username: new FormControl(null , Validators.required),
      email: new FormControl(null , [Validators.required, Validators.email]),
      password: new FormControl(null , Validators.required),
      confirmPassword:new FormControl(null , Validators.required),
    },
    {
      validator: this.MustMatch('password', 'confirmPassword')
    });
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
  
      if (matchingControl.errors && !matchingControl.errors['mustMatch']) {
        return;
      }
  
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  private setHoldertype() {
    this.router.url.includes("jobseeker") ?
      this.holderType = HolderType.JOBSEEKER
      : this.holderType = HolderType.COMPANY;
  }

}
