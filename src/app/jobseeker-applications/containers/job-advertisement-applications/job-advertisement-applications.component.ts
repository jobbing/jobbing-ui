import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { MatchStateType } from 'src/app/shared/enums/match-state-type.enum';
import { JobAdvertisementDto } from '../../model/job-advertisement-dto';
import { fromJobseekApplicationsStore, JobseekerApplicationsActions, JobseekerApplicationsSelectors } from '../../store';

@Component({
  selector: 'app-job-advertisement-applications',
  templateUrl: './job-advertisement-applications.component.html',
  styleUrls: ['./job-advertisement-applications.component.scss']
})
export class JobAdvertisementApplicationsComponent implements OnInit {

  jobAdvertisements$!: Observable<JobAdvertisementDto[]>;
  matchStateTypes = Object.values(MatchStateType);
  matchStateTypeKeys = Object.keys(MatchStateType);
  myDatas$: Observable<GetMyDatasDto | null>;
  jobseekerId!: number;
  

  constructor(
    private store: Store<fromJobseekApplicationsStore.JobseekerApplicationsState>,
    private authStore: Store<fromAuthStore.AuthState>
  ) { 
    this.jobAdvertisements$ = this.store.pipe(select(JobseekerApplicationsSelectors.selectJobAdvertisements));
    this.myDatas$ = this.authStore.pipe(select(AuthSelectors.selectMyDatas))
  }

  ngOnInit(): void {
  }

  onChange(value: any){
    if(value != null){
      let selected = this.matchStateTypeKeys[this.matchStateTypes.indexOf(value.value)];
      this.getMyJobseekerId();
      this.store.dispatch(
        JobseekerApplicationsActions.listJobAdvertisements({
          advertisementStateFilter:{
            jobseekerId: this.jobseekerId,
            matchStateType: selected as MatchStateType
          }
        })
      )
    }

  }

  private  getMyJobseekerId(){
    this.myDatas$.subscribe(event => {this.jobseekerId = event?.jobseekerId!});

  }
}
