import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementApplicationsComponent } from './job-advertisement-applications.component';

describe('JobAdvertisementApplicationsComponent', () => {
  let component: JobAdvertisementApplicationsComponent;
  let fixture: ComponentFixture<JobAdvertisementApplicationsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementApplicationsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementApplicationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
