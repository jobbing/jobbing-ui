import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { JobAdvertisementActions } from 'src/app/shared/job-advertisement/store';
import { InterestedCompanyDto } from '../../model/interested-company-dto';
import { fromJobseekApplicationsStore, JobseekerApplicationsActions, JobseekerApplicationsSelectors } from '../../store';

@Component({
  selector: 'app-company-applications',
  templateUrl: './company-applications.component.html',
  styleUrls: ['./company-applications.component.scss']
})
export class CompanyApplicationsComponent implements OnInit {

  companies$!: Observable<InterestedCompanyDto[]>
  myDatas$: Observable<GetMyDatasDto | null>;
  jobseekerId!: number;

  constructor(
    private store: Store<fromJobseekApplicationsStore.JobseekerApplicationsState>,
    private authStore: Store<fromAuthStore.AuthState>
  ) {
    this.myDatas$ = this.authStore.pipe(select(AuthSelectors.selectMyDatas)),
    this.companies$ = this.store.pipe(select(JobseekerApplicationsSelectors.selectInterestedCompanies))
   }

  ngOnInit(): void {
    this.getMyJobseekerId();
    this.store.dispatch(
      JobseekerApplicationsActions.listInterestedCompanies({
        jobseekerId: this.jobseekerId
      })
    )
  }

  private  getMyJobseekerId(){
    this.myDatas$.subscribe(event => {this.jobseekerId = event?.jobseekerId!});

  }

  acceptCompanyEvent(companyId: number){
    this.store.dispatch(
      JobseekerApplicationsActions.jobseekerAcceptCompany({
        simpleMatchDto:{
          companyId: companyId,
          jobseekerId:this.jobseekerId
        }
      })
    )
  }

  rejectCompanyEvent(companyId: number){
    this.store.dispatch(
      JobseekerApplicationsActions.jobseekerRejectCompany({
        simpleMatchDto:{
          companyId: companyId,
          jobseekerId: this.jobseekerId
        }
      })
    )
  }
}
