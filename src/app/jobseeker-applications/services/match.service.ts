import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { SimpleMatchDto } from "../model/simple-match-dto";

@Injectable()
export class MatchService{

    private readonly URL = '/match';

    constructor(
        private http: HttpClient
    ) { }

    jobseekerAcceptCompany(simpleMatchDto: SimpleMatchDto){
        let reqUrl = `${this.URL}/jobseekeer-accept-company`;
        return this.http.post<number>(reqUrl, simpleMatchDto);
    }

    jobseekerRejectCompany(simpleMatchDto: SimpleMatchDto){
        let reqUrl = `${this.URL}/jobseekeer-reject-company`;
        return this.http.post<number>(reqUrl, simpleMatchDto);
    }

    companyAcceptJobseeker(simpleMatchDto: SimpleMatchDto){
        let reqUrl = `${this.URL}/company-accept-jobseeker`;
        return this.http.post<number>(reqUrl, simpleMatchDto);
    }

    companyRejectJobseeker(simpleMatchDto: SimpleMatchDto){
        let reqUrl = `${this.URL}/company-reject-jobseeker`;
        return this.http.post<number>(reqUrl, simpleMatchDto);
    }

}