import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AdvertisementStateFilter } from "../model/advertisement-state-filter";
import { InterestedCompanyDto } from "../model/interested-company-dto";
import { JobAdvertisementDto } from "../model/job-advertisement-dto";


@Injectable()
export class JobseekerApplicationsService{

    private readonly URL = '/my-job-advertisements';

    constructor(
        private http: HttpClient
    ) { }

    listJobAdvertisements(advertisementStateFilter: AdvertisementStateFilter){
        let reqUrl = `${this.URL}/list`;
        return this.http.post<JobAdvertisementDto[]>(reqUrl, advertisementStateFilter);
    }

    listInterestedCompanies(jobseekerId: number){
        let reqUrl = `${this.URL}/interested-companies?jobseekerId=`+jobseekerId;
        return this.http.get<InterestedCompanyDto[]>(reqUrl);
    }
}