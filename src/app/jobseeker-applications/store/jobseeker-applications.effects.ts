import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, switchMap } from "rxjs";
import { JobseekerApplicationsActions } from ".";
import { JobseekerApplicationsService } from "../services/jobseeker-applications.service";
import { MatchService } from "../services/match.service";

@Injectable()
export class JobseekerApplicationsEffects{

    listJobAdvertisements$ = createEffect(() => this.actions$
    .pipe(
        ofType(JobseekerApplicationsActions.listJobAdvertisements),
        switchMap(action =>
            this.jobseekerApplicationsService.listJobAdvertisements(action.advertisementStateFilter)
            .pipe(
                map(jobAdvertisementDto => JobseekerApplicationsActions.listJobAdvertisementsSuccess({jobAdvertisementDto}))
            ))
    ));

    listInterestedCompanyies$ = createEffect(() => this.actions$
    .pipe(
        ofType(JobseekerApplicationsActions.listInterestedCompanies),
        switchMap(action =>
            this.jobseekerApplicationsService.listInterestedCompanies(action.jobseekerId)
            .pipe(
                map(interestedCompanyDto => JobseekerApplicationsActions.listInterestedCompaniesSuccess({interestedCompanyDto}))
            ))
    ));

    jobseekerAcceptCompany$ = createEffect(() => this.actions$
    .pipe(
        ofType(JobseekerApplicationsActions.jobseekerAcceptCompany),
        switchMap(action =>
            this.matchService.jobseekerAcceptCompany(action.simpleMatchDto)
            .pipe(
                map(jobseekerId => JobseekerApplicationsActions.listInterestedCompanies({jobseekerId}))
            ))
    ));

    jobseekerRejectCompany$ = createEffect(() => this.actions$
    .pipe(
        ofType(JobseekerApplicationsActions.jobseekerRejectCompany),
        switchMap(action =>
            this.matchService.jobseekerRejectCompany(action.simpleMatchDto)
            .pipe(
                map(jobseekerId => JobseekerApplicationsActions.listInterestedCompanies({jobseekerId}))
            ))
    ));

    constructor(
        private actions$: Actions,
        private jobseekerApplicationsService: JobseekerApplicationsService,
        private matchService: MatchService
    ){}
} 