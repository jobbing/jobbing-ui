import { createAction, props } from "@ngrx/store";
import { FailData } from "src/app/core/model/fail-message.model";
import { JobseekerDto } from "src/app/manage-jobseekers/model/jobseeker-dto";
import { AdvertisementStateFilter } from "../model/advertisement-state-filter";
import { InterestedCompanyDto } from "../model/interested-company-dto";
import { JobAdvertisementDto } from "../model/job-advertisement-dto";
import { SimpleMatchDto } from "../model/simple-match-dto";

const prefix = '[jobseeker-applications]';

export const listJobAdvertisements = createAction(
    `${prefix} list job advertisements`,
    props<{ advertisementStateFilter: AdvertisementStateFilter}>()
);

export const listJobAdvertisementsSuccess = createAction(
    `${prefix} list job advertisements success`,
    props<{ jobAdvertisementDto: JobAdvertisementDto[] }>()
);

export const listJobAdvertisementsFail = createAction(
    `${prefix} list job advertisements fail`,
    props<{ failData: FailData }>()
);

export const listInterestedCompanies = createAction(
    `${prefix} list interested companies`,
    props<{ jobseekerId: number}>()
);

export const listInterestedCompaniesSuccess = createAction(
    `${prefix} list interested companies success`,
    props<{ interestedCompanyDto: InterestedCompanyDto[] }>()
);

export const listInterestedCompaniesFail = createAction(
    `${prefix} list interested companies fail`,
    props<{ failData: FailData }>()
);

export const jobseekerAcceptCompany = createAction(
    `${prefix} jobseeker accept company`,
    props<{ simpleMatchDto: SimpleMatchDto}>()
);

export const jobseekerAcceptCompanySuccess = createAction(
    `${prefix} jobseeker accept company success`,
);

export const jobseekerAcceptCompanyFail = createAction(
    `${prefix} jobseeker accept company fail`,
    props<{ failData: FailData }>()
);

export const jobseekerRejectCompany = createAction(
    `${prefix} jobseeker reject company`,
    props<{  simpleMatchDto: SimpleMatchDto }>()
);

export const jobseekerRejectCompanySuccess = createAction(
    `${prefix} jobseeker reject company success`
);

export const jobseekerRejectCompanyFail = createAction(
    `${prefix} jobseeker reject company fail`,
    props<{ failData: FailData }>()
);

