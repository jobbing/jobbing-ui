import { createReducer, on } from "@ngrx/store";
import { JobseekerApplicationsActions } from ".";
import { InterestedCompanyDto } from "../model/interested-company-dto";
import { JobAdvertisementDto } from "../model/job-advertisement-dto";


export const JobseekerApplicationsFeatureKey = 'jobseeker-applications';

export interface JobseekerApplicationsState {
    advertisementsDtoList: JobAdvertisementDto[],
    interestedCompaniesDtoList: InterestedCompanyDto[],
}

export const initialState: JobseekerApplicationsState = {
    advertisementsDtoList: [],
    interestedCompaniesDtoList: [],
}

export const ManageJobAdvertisementsReducer = createReducer(
    initialState,
    on(JobseekerApplicationsActions.listJobAdvertisementsSuccess, (state, action) => ({
        ...state,
        advertisementsDtoList: action.jobAdvertisementDto
    })),
    on(JobseekerApplicationsActions.listInterestedCompaniesSuccess, (state, action) => ({
        ...state,
        interestedCompaniesDtoList: action.interestedCompanyDto
    })),
    
);