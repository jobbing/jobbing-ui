import { createFeatureSelector, createSelector } from "@ngrx/store";
import { JobseekerApplicationsFeatureKey, JobseekerApplicationsState } from "./jobseeker-applications.reducers";

const selectJobseekerApplicationsState = createFeatureSelector<JobseekerApplicationsState>(JobseekerApplicationsFeatureKey);

export const selectJobAdvertisements = createSelector(selectJobseekerApplicationsState, (state) => state.advertisementsDtoList);

export const selectInterestedCompanies = createSelector(selectJobseekerApplicationsState, (state) => state.interestedCompaniesDtoList);