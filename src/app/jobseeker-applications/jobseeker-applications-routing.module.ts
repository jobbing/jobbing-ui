import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CompanyApplicationsComponent } from "./containers/company-applications/company-applications.component";
import { JobAdvertisementApplicationsComponent } from "./containers/job-advertisement-applications/job-advertisement-applications.component";

const routes: Routes = [
    {path: 'job-advertisement-applications/jobseeker', component: JobAdvertisementApplicationsComponent},
    {path: 'company-applications/jobseeker', component: CompanyApplicationsComponent},
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class JobseekerApplicationsRoutingModule {}