import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { HeaderComponent } from "../shared/header/header.component";
import { JobAdvertisementApplicationsComponent } from './containers/job-advertisement-applications/job-advertisement-applications.component';
import { CompanyApplicationsComponent } from './containers/company-applications/company-applications.component';
import { JobseekerApplicationsRoutingModule } from "./jobseeker-applications-routing.module";
import { SharedModule } from "../shared/shared.module";
import { SubNavigationComponent } from './components/sub-navigation/sub-navigation.component';
import { JobAdvertisementOneComponent } from './components/job-advertisement-one/job-advertisement-one.component';
import { CompanyOneComponent } from './components/company-one/company-one.component';
import { StoreModule } from "@ngrx/store";
import { fromJobseekApplicationsStore } from "./store";
import { EffectsModule } from "@ngrx/effects";
import { JobseekerApplicationsEffects } from "./store/jobseeker-applications.effects";
import { JobseekerApplicationsService } from "./services/jobseeker-applications.service";
import { MatchService } from "./services/match.service";

@NgModule({
    imports:[
        ReactiveFormsModule,
        CommonModule,
        SharedModule,
        JobseekerApplicationsRoutingModule,
        StoreModule.forFeature(
            fromJobseekApplicationsStore.JobseekerApplicationsFeatureKey,
            fromJobseekApplicationsStore.ManageJobAdvertisementsReducer
        ),
        EffectsModule.forFeature([JobseekerApplicationsEffects])
    ],
    declarations:[
        JobAdvertisementApplicationsComponent,
        CompanyApplicationsComponent,
        SubNavigationComponent,
        JobAdvertisementOneComponent,
        CompanyOneComponent
  ],
    providers:[
        MatchService,
        JobseekerApplicationsService,
        HeaderComponent
    ]
})

export class JobseekerApplicationsModule{}