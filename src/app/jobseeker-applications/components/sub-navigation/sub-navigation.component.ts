import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sub-navigation',
  templateUrl: './sub-navigation.component.html',
  styleUrls: ['./sub-navigation.component.scss']
})
export class SubNavigationComponent implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  navigateToCompanies(){
    this.router.navigate(["jobseeker-applications","company-applications","jobseeker"]);
  }

  navigateToJobAdvertisements(){
    this.router.navigate(["jobseeker-applications","job-advertisement-applications","jobseeker"]);
  }

}
