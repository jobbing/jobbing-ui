import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { InterestedCompanyDto } from '../../model/interested-company-dto';

@Component({
  selector: 'app-company-one',
  templateUrl: './company-one.component.html',
  styleUrls: ['./company-one.component.scss']
})
export class CompanyOneComponent implements OnInit {

  @Input() company!: InterestedCompanyDto;
  @Output() acceptCompanyEvent: EventEmitter<number>;
  @Output() rejectCompanyEvent: EventEmitter<number>;

  constructor(
    private router: Router
  ) {
    this.acceptCompanyEvent = new EventEmitter();
    this.rejectCompanyEvent = new EventEmitter();
   }

  ngOnInit(): void {
  }

  acceptCompany(id: number){
    this.acceptCompanyEvent.emit(id);
  }

  rejectCompany(id: number){
    this.rejectCompanyEvent.emit(id);
  }

  navigateToCompany(){
    this.router.navigate(['profile','company',this.company.id,'jobseeker']);
  }

}
