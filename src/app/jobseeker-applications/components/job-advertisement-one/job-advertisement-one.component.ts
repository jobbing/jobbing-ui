import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JobAdvertisementDto } from '../../model/job-advertisement-dto';

@Component({
  selector: 'app-job-advertisement-one',
  templateUrl: './job-advertisement-one.component.html',
  styleUrls: ['./job-advertisement-one.component.scss']
})
export class JobAdvertisementOneComponent implements OnInit {

  @Input() jobAdvertisementSummary!: JobAdvertisementDto;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  navigateToJobAdvertisement(){
    this.router.navigate(['job-advertisement',this.jobAdvertisementSummary.id,'jobseeker']);
  }  

}
