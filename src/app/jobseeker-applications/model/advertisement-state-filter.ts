import { MatchStateType } from "src/app/shared/enums/match-state-type.enum";

export interface AdvertisementStateFilter{

    jobseekerId: number,
    matchStateType: MatchStateType
}