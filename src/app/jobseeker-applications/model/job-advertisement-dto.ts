export interface JobAdvertisementDto{
 
    id: number,
    name: string,
    logo: string,
    summary: string

}