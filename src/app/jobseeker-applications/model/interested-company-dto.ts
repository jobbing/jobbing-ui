export interface InterestedCompanyDto{

    id: number,
    name: string,
    logo: string

}