import { ApplicantDto } from "./applicants-dto";

export interface CompanyManageJobAdvertisementsDto {

    id: number,
    name: string,
    summary: string,
    applicants: ApplicantDto[]

}