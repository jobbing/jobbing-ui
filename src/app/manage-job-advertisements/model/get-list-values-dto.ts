import { CityDto } from "./city-dto";
import { WorkCategoryDto } from "./work-category-dto";
import { WorkTypeDto } from "./workTypeDto";

export interface GetListValuesDto{
    cities: CityDto[],
    workTypes: WorkTypeDto[],
    workCategories: WorkCategoryDto[]
}