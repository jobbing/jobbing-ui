import { FormGroup } from "@angular/forms";
import { CityType } from "src/app/shared/enums/city-type.enum";
import { WorkCategory } from "src/app/shared/enums/work-category.enum";
import { WorkingType } from "src/app/shared/enums/work-type.enum";

export interface NewJobAdvertisementDto{

    workingTypeSelectedList: WorkingType[];
    citySelectedList: CityType[];
    workCategorySelectedList: WorkCategory[];
    modifyForm: FormGroup;

}