import { WorkingType } from "src/app/shared/enums/work-type.enum";

export interface WorkTypeDto{

    id: number,
    workType: WorkingType

}