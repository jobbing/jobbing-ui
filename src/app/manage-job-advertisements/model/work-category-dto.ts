import { WorkCategory } from "src/app/shared/enums/work-category.enum";

export interface WorkCategoryDto{

    id: number,
    name: WorkCategory

}