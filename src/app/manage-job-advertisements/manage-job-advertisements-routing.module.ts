import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MyJobAdvertisementsComponent } from "./containers/my-job-advertisements/my-job-advertisements.component";
import { NewJobAdvertisementComponent } from "./containers/new-job-advertisement/new-job-advertisement.component";

const routes: Routes = [
    {path: 'my-job-advertisements/company', component: MyJobAdvertisementsComponent},
    {path: 'new-job-advertisement/company', component: NewJobAdvertisementComponent},
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class ManageJobAdvertisementsRoutingModule {}