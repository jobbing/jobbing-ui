import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyJobAdvertisementsComponent } from './my-job-advertisements.component';

describe('MyJobAdvertisementsComponent', () => {
  let component: MyJobAdvertisementsComponent;
  let fixture: ComponentFixture<MyJobAdvertisementsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyJobAdvertisementsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MyJobAdvertisementsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
