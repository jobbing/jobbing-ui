import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { CompanyManageJobAdvertisementsDto } from '../../model/company-manage-job-advertisement-dto';
import { jobseekerMatchDto } from '../../model/jobseeker-match-dto';
import { fromManageJobAdvertisementsReducers, ManageJobAdvertisementsAction, ManageJobAdvertisementsSelectors } from '../../store';

@Component({
  selector: 'app-my-job-advertisements',
  templateUrl: './my-job-advertisements.component.html',
  styleUrls: ['./my-job-advertisements.component.scss']
})
export class MyJobAdvertisementsComponent implements OnInit {

  jobAdvertisementsWithAppliciants$: Observable<CompanyManageJobAdvertisementsDto[]>;
  myData$: Observable<GetMyDatasDto | null>;
  companyId!: number;

  constructor(
    private authStore: Store<fromAuthStore.AuthState>,
    private store: Store<fromManageJobAdvertisementsReducers.ManageJobAdvertisementsState>,
    private router: Router
  ) { 
    
    this.myData$ = this.authStore.pipe(select(AuthSelectors.selectMyDatas));
    this.getCompanyId();
    this.jobAdvertisementsWithAppliciants$ = this.store.pipe(select(ManageJobAdvertisementsSelectors.selectJobAdvertisementsWithAppliciants));
  }

  ngOnInit(): void {
    this.getJobAdvertisementsWithAppliciants();
  }

 
  navigateToCreateJobAdvertisement(){
    this.router.navigate(["manage-job-advertisements","new-job-advertisement","company"]);
  }

  acceptJobseekerEvent(jobseekerMatchDto: jobseekerMatchDto){
    this.store.dispatch(
      ManageJobAdvertisementsAction.companyAcceptJobseekerByAdvertisement({
        jobseekerMatchDto:{
          jobAdvertisementId: jobseekerMatchDto.jobAdvertisementId,
          jobseekerId: jobseekerMatchDto.jobseekerId
        }
      })
    )
  }

  rejectJobseekerEvent(jobseekerMatchDto: jobseekerMatchDto){
    this.store.dispatch(
      ManageJobAdvertisementsAction.companyRejectJobseekerByAdvertisement({
        jobseekerMatchDto:{
          jobAdvertisementId: jobseekerMatchDto.jobAdvertisementId,
          jobseekerId: jobseekerMatchDto.jobseekerId
        }
      })
    )
  }

  private getJobAdvertisementsWithAppliciants(){
    this.store.dispatch(
      ManageJobAdvertisementsAction.getJobAdvertisementsWithApplicants({
        companyId: this.companyId, 
        paging:{
          pageStart: 10,
          pageSize: 10,
        }
      })
    )
  }

  private getCompanyId(){
    this.myData$.subscribe(event => this.companyId = event?.companyId!);
  }

}
