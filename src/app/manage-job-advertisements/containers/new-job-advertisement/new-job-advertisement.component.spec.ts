import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewJobAdvertisementComponent } from './new-job-advertisement.component';

describe('NewJobAdvertisementComponent', () => {
  let component: NewJobAdvertisementComponent;
  let fixture: ComponentFixture<NewJobAdvertisementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewJobAdvertisementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewJobAdvertisementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
