import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { fromJobAdvertisementStore, JobAdvertisementActions, JobAdvertisementSelectors } from 'src/app/shared/job-advertisement/store';
import { GetListValuesDto } from '../../model/get-list-values-dto';

@Component({
  selector: 'app-new-job-advertisement',
  templateUrl: './new-job-advertisement.component.html',
  styleUrls: ['./new-job-advertisement.component.scss']
})
export class NewJobAdvertisementComponent implements OnInit {

  listValues$: Observable<GetListValuesDto | null>;

  constructor(
    private router: Router,
    private store: Store<fromJobAdvertisementStore.JobAdvertisementState>
  ) { 
    this.listValues$ = this.store.pipe(select(JobAdvertisementSelectors.selectListValues));
  }

  ngOnInit(): void {
    this.store.dispatch(
      JobAdvertisementActions.getListValues()
    )
  }

  newJobAdvertisementEvent(newJobAdvertisementDto: any){
    this.store.dispatch(
      JobAdvertisementActions.saveJobAdvertisement({
        saveJobAdvertisementDto:{
          companyId: 1,
          name: newJobAdvertisementDto.modifyForm.name,
          place: newJobAdvertisementDto.modifyForm.place,
          summary: newJobAdvertisementDto.modifyForm.summary,
          textEditor: newJobAdvertisementDto.modifyForm.textEditor,
          newWorkCategoryNameList: [],
          workCategoryIdList: newJobAdvertisementDto.workCategorySelectedList,
          workTypeIdList: newJobAdvertisementDto.workingTypeSelectedList,
          cityIdList: newJobAdvertisementDto.citySelectedList
          
        }
      })
    )

    this.router.navigate(["manage-job-advertisements","my-job-advertisements","company"])
  }

}
