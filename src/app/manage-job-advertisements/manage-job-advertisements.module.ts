import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { HeaderComponent } from "../shared/header/header.component";
import { SharedModule } from "../shared/shared.module";
import { MyJobAdvertisementsComponent } from './containers/my-job-advertisements/my-job-advertisements.component';
import { JobAdvertisementWithAppliciantsComponent } from './components/job-advertisement-with-appliciants/job-advertisement-with-appliciants.component';
import { ManageJobAdvertisementsRoutingModule } from "./manage-job-advertisements-routing.module";
import { StoreModule } from "@ngrx/store";
import { fromManageJobAdvertisementsReducers } from "./store";
import { EffectsModule } from "@ngrx/effects";
import { ManageJobAdvertisementsEffects } from "./store/manage-job-advertisements.effects";
import { ManageJobAdvertisementsService } from "./services/manage-job-advertisements.service";
import { MatchByJobAdvertisementService } from "./services/match-by-job-advertisement.service";
import { NewJobAdvertisementComponent } from './containers/new-job-advertisement/new-job-advertisement.component';
import { NewJobAdvertisementFormComponent } from './components/new-job-advertisement-form/new-job-advertisement-form.component';
import { QuillModule } from "ngx-quill";

@NgModule({
    imports:[
        ReactiveFormsModule,
        ManageJobAdvertisementsRoutingModule,
        CommonModule,
        SharedModule,
        StoreModule.forFeature(
            fromManageJobAdvertisementsReducers.ManageJobAdvertisementsFeatureKey,
            fromManageJobAdvertisementsReducers.ManageJobAdvertisementsReducer
        ),
        EffectsModule.forFeature([ManageJobAdvertisementsEffects]),
        QuillModule
    ],
    declarations:[
        MyJobAdvertisementsComponent,
        JobAdvertisementWithAppliciantsComponent,
        NewJobAdvertisementComponent,
        NewJobAdvertisementFormComponent
  ],
    providers:[
        MatchByJobAdvertisementService,
        ManageJobAdvertisementsService,
        HeaderComponent
    ]
})

export class ManageJobAdvertisementsModule{}