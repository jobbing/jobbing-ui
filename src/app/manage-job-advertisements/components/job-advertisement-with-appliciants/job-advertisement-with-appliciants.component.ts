import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MatchStateType } from 'src/app/shared/enums/match-state-type.enum';
import { ApplicantDto } from '../../model/applicants-dto';
import { CompanyManageJobAdvertisementsDto } from '../../model/company-manage-job-advertisement-dto';
import { jobseekerMatchDto } from '../../model/jobseeker-match-dto';

@Component({
  selector: 'app-job-advertisement-with-appliciants',
  templateUrl: './job-advertisement-with-appliciants.component.html',
  styleUrls: ['./job-advertisement-with-appliciants.component.scss']
})
export class JobAdvertisementWithAppliciantsComponent implements OnInit {

  @Input() jobAdvertisement!: CompanyManageJobAdvertisementsDto;
  @Output() acceptJobseekerEvent: EventEmitter<jobseekerMatchDto>;
  @Output() rejectJobseekerEvent: EventEmitter<jobseekerMatchDto>;


  accepted: ApplicantDto[] = [];
  rejected: ApplicantDto[] = [];
  inProgress: ApplicantDto[] = [];

  constructor(
    private router: Router
  ) { 
    this.acceptJobseekerEvent = new EventEmitter();
    this.rejectJobseekerEvent = new EventEmitter();
  }

  ngOnInit(): void {
    this.divideAppliciants();
  }

  openProfile(applicant: ApplicantDto){
    this.router.navigate(["profile","jobseeker",applicant.jobseekerId,"company"]);
  }

  openJobAdvertisement(){
    this.router.navigate(['/job-advertisement', this.jobAdvertisement.id,"company"]);
  }

  rejectJobseeker(applicant: ApplicantDto){
    this.rejectJobseekerEvent.emit({jobAdvertisementId: applicant.jobAdvertisementId, jobseekerId: applicant.jobseekerId})
    const index: number = this.inProgress.indexOf(applicant);
    this.inProgress.splice(index, 1);
    if(!this.rejected.includes(applicant)){
      this.rejected.push(applicant);
    }
  }

  acceptJobseeker(applicant: ApplicantDto){
    this.acceptJobseekerEvent.emit({jobAdvertisementId: applicant.jobAdvertisementId, jobseekerId: applicant.jobseekerId})
    const index: number = this.inProgress.indexOf(applicant);
    this.inProgress.splice(index, 1);
    if(!this.accepted.includes(applicant)){
      this.accepted.push(applicant);
    }
  }

  private divideAppliciants(){
    this.jobAdvertisement.applicants.forEach(
      applicant => {
        if(applicant.matchState == MatchStateType.MATCH){
          this.accepted.push(applicant);
        }
        else if(applicant.matchState == MatchStateType.COMPANY_REJECT){
          this.rejected.push(applicant);
        }
        else if(applicant.matchState == MatchStateType.JOBSEEKER_ACCEPT){
          this.inProgress.push(applicant);
        }
      }
      );
  }

}

