import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementWithAppliciantsComponent } from './job-advertisement-with-appliciants.component';

describe('JobAdvertisementWithAppliciantsComponent', () => {
  let component: JobAdvertisementWithAppliciantsComponent;
  let fixture: ComponentFixture<JobAdvertisementWithAppliciantsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementWithAppliciantsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementWithAppliciantsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
