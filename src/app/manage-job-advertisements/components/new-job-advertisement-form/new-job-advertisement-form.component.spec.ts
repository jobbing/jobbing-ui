import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewJobAdvertisementFormComponent } from './new-job-advertisement-form.component';

describe('NewJobAdvertisementFormComponent', () => {
  let component: NewJobAdvertisementFormComponent;
  let fixture: ComponentFixture<NewJobAdvertisementFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewJobAdvertisementFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewJobAdvertisementFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
