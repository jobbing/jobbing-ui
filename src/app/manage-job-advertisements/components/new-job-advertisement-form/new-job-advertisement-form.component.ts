import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';
import { Observable } from 'rxjs';
import { fromAuthStore, AuthActions } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { HolderType } from 'src/app/core/model/holder-type';
import { CityType, CITY_TYPES } from 'src/app/shared/enums/city-type.enum';
import { WORK_CATEGORIES, WorkCategory } from 'src/app/shared/enums/work-category.enum';
import { WorkingType, WORKING_TYPES } from 'src/app/shared/enums/work-type.enum';
import { CompanyGetJobAdvertisementDto } from 'src/app/shared/job-advertisement/model/company-get-job-advertisement-dto';
import { fromJobAdvertisementStore } from 'src/app/shared/job-advertisement/store';
import { CityDto } from '../../model/city-dto';
import { GetListValuesDto } from '../../model/get-list-values-dto';
import { NewJobAdvertisementDto } from '../../model/new-job-advertisement-dto';
import { WorkCategoryDto } from '../../model/work-category-dto';
import { WorkTypeDto } from '../../model/workTypeDto';

@Component({
  selector: 'app-new-job-advertisement-form',
  templateUrl: './new-job-advertisement-form.component.html',
  styleUrls: ['./new-job-advertisement-form.component.scss']
})
export class NewJobAdvertisementFormComponent implements OnInit {

  @Input() listValues!: GetListValuesDto | null;
  @Output() newJobAdvertisementEvent: EventEmitter<any>;
  myData$!: Observable<GetMyDatasDto>;

  workingTypes = WORKING_TYPES;
  workTypeDtoSelectedList: number[] = [];
  workingTypeSelectedList: WorkingType[] = [];
  cities = CITY_TYPES;
  cityDtoSelectedList: number[] = [];
  citySelectedList: CityType[] = [];
  workCategories = WORK_CATEGORIES;
  workCategoryDtoSelectedList: number[] = [];
  workCategorySelectedList: WorkCategory[] = [];
  type = false;
  category = false;
  city = false;
  companyId!: number;

  modifyForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authStore: Store<fromAuthStore.AuthState>,
  ) {
    this.newJobAdvertisementEvent = new EventEmitter();
    this.authStore.dispatch(
      AuthActions.getMyDatas({
        holderType: HolderType.COMPANY
      })
    )
    this.createForm();
   }

  ngOnInit(): void {
  }

  getCompanyId(){
    this.myData$.subscribe(event => this.companyId= event.companyId);
  }

  save(){

    if(this.modifyForm.valid){

      for(let workTypeName of this.workingTypeSelectedList){
        if(this.listValues?.workTypes[this.workingTypeSelectedList.indexOf(workTypeName)])
        this.workTypeDtoSelectedList.push(this.listValues?.workTypes[this.workingTypeSelectedList.indexOf(workTypeName)].id);
      }
      for(let cityName of this.citySelectedList){
        if(this.listValues?.cities[this.citySelectedList.indexOf(cityName)])
        this.cityDtoSelectedList.push(this.listValues?.cities[this.citySelectedList.indexOf(cityName)].id);
      }
      for(let workCategoryName of this.workCategorySelectedList){
        if(this.listValues?.workCategories[this.workCategorySelectedList.indexOf(workCategoryName)])
        this.workCategoryDtoSelectedList.push(this.listValues?.workCategories[this.workCategorySelectedList.indexOf(workCategoryName)].id);
      }

      this.newJobAdvertisementEvent.emit({
        modifyForm: this.modifyForm.value,
        citySelectedList: this.cityDtoSelectedList,
        workCategorySelectedList: this.workCategoryDtoSelectedList,
        workingTypeSelectedList: this.workTypeDtoSelectedList
      })
    }
  }

  selectedCities(value: any){
    if(this.citySelectedList.includes(value)){
      return true;
    }
    else{
      return false;
    }
  }

  selectedTypes(value: any){
    if(this.workingTypeSelectedList.includes(value)){
      return true;
    }
    else{
      return false;
    }
  }

  selectedCategoryes(value: any){
    if(this.workCategorySelectedList.includes(value)){
      return true;
    }
    else{
      return false;
    }
  }

  checkCitySelected(value: any){
    if(this.citySelectedList.includes(value)){
      this.removeCityFromList(value);
    }
    else{
      this.addCityToList(value);
    }
  }

  checkWorkingTypeSelected(value: any){
    if(this.workingTypeSelectedList.includes(value)){
      this.removeWorkingTypeFromList(value);
    }
    else{
      this.addWorkingTypeToList(value);
    }
  }

  checkWorkCategorySelected(value: any){
    if(this.workCategorySelectedList.includes(value)){
      this.removeWorkCategoryFromList(value);
    }
    else{
      this.addWorkCategoryToList(value);
    }
  }

  private addWorkCategoryToList(value: any){
    this.workCategorySelectedList.push(value);
  }
  
  private removeWorkCategoryFromList(value: any){
    const index: number = this.workCategorySelectedList.indexOf(value);
    this.workCategorySelectedList.splice(index, 1);

  }

  private addCityToList(value: any){
    this.citySelectedList.push(value);
  }
  
  private removeCityFromList(value: any){
    const index: number = this.citySelectedList.indexOf(value);
    this.citySelectedList.splice(index, 1);
  }

  private addWorkingTypeToList(value: any){
    this.workingTypeSelectedList.push(value);
  }
  
  private removeWorkingTypeFromList(value: any){
    const index: number = this.workingTypeSelectedList.indexOf(value);
    this.workingTypeSelectedList.splice(index, 1);
  }

  openCity(){
    this.city = true;
    this.category = false;
    this.type = false;
  }

  openCategory(){
    this.city = false;
    this.category = true;
    this.type = false;
  }
  
  openType(){
    this.city = false;
    this.category = false;
    this.type = true;
  }

  private createForm() {
    this.modifyForm = this.formBuilder.group({
      name: new FormControl(null, Validators.required),
      place: new FormControl(null, Validators.required),
      summary: new FormControl(null, Validators.required),
      textEditor: new FormControl(null)
    });
  }

}
