import { createAction, props } from "@ngrx/store";
import { FailData } from "src/app/core/model/fail-message.model";
import { CompanyManageJobAdvertisementsDto } from "../model/company-manage-job-advertisement-dto";
import { jobseekerMatchDto } from "../model/jobseeker-match-dto";

const prefix = '[manage-job-advertisements]';

export const getJobAdvertisementsWithApplicants = createAction(
    `${prefix} get job advertisements with appliciants`,
    props<{ companyId: number, paging: any }>()
);

export const getJobAdvertisementsWithApplicantsSuccess = createAction(
    `${prefix} get job advertisements with appliciants success`,
    props<{ companyManageJobAdvertisementsDto: CompanyManageJobAdvertisementsDto[] }>()
);

export const getJobAdvertisementsWithApplicantsFail = createAction(
    `${prefix} get job advertisements with appliciants fail`,
    props<{ failData: FailData }>()
);

export const companyAcceptJobseekerByAdvertisement = createAction(
    `${prefix} company accept jobseeker by advertisement`,
    props<{ jobseekerMatchDto: jobseekerMatchDto}>()
);

export const companyAcceptJobseekerByAdvertisementSuccess = createAction(
    `${prefix} company accept jobseeker by advertisement success`
);

export const companyAcceptJobseekerByAdvertisementFail = createAction(
    `${prefix} company accept jobseeker by advertisement fail`,
    props<{ failData: FailData }>()
);

export const companyRejectJobseekerByAdvertisement = createAction(
    `${prefix} company reject jobseeker by advertisement`,
    props<{ jobseekerMatchDto: jobseekerMatchDto}>()
);

export const companyRejectJobseekerByAdvertisementSuccess = createAction(
    `${prefix} company reject jobseeker by advertisement success`
);

export const companyRejectJobseekerByAdvertisementFail = createAction(
    `${prefix} company reject jobseeker by advertisement fail`,
    props<{ failData: FailData }>()
);