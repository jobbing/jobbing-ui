import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, switchMap } from "rxjs";
import { ManageJobAdvertisementsAction } from ".";
import { ManageJobAdvertisementsService } from "../services/manage-job-advertisements.service";
import { MatchByJobAdvertisementService } from "../services/match-by-job-advertisement.service";

@Injectable()
export class ManageJobAdvertisementsEffects{

    getJobAdvertisementsWithAppliciants$ = createEffect(() => this.actions$
    .pipe(
        ofType(ManageJobAdvertisementsAction.getJobAdvertisementsWithApplicants),
        switchMap(action =>
            this.manageJobAdvertisementsService.getJobAdvertisementsWithApplicants(action.companyId, action.paging)
            .pipe(
                map(companyManageJobAdvertisementsDto => ManageJobAdvertisementsAction.getJobAdvertisementsWithApplicantsSuccess({companyManageJobAdvertisementsDto}))
            ))
    ));

    companyAcceptJobseekerByAdvertisement$ = createEffect(() => this.actions$
    .pipe(
        ofType(ManageJobAdvertisementsAction.companyAcceptJobseekerByAdvertisement),
        switchMap(action =>
            this.matchByJobAdvertisementService.companyAcceptJobseekerByAdvertisement(action.jobseekerMatchDto)
            .pipe(
                map(() => ManageJobAdvertisementsAction.companyAcceptJobseekerByAdvertisementSuccess())
            ))
    ));

    companyRejectJobseekerByAdvertisement$ = createEffect(() => this.actions$
    .pipe(
        ofType(ManageJobAdvertisementsAction.companyRejectJobseekerByAdvertisement),
        switchMap(action =>
            this.matchByJobAdvertisementService.companyRejectJobseekerByAdvertisement(action.jobseekerMatchDto)
            .pipe(
                map(() => ManageJobAdvertisementsAction.companyRejectJobseekerByAdvertisementSuccess())
            ))
    ));


    constructor(
        private actions$: Actions,
        private manageJobAdvertisementsService: ManageJobAdvertisementsService,
        private matchByJobAdvertisementService: MatchByJobAdvertisementService,
    ){}
}

