import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ManageJobAdvertisementsFeatureKey, ManageJobAdvertisementsState } from "./manage-job-advertisements.reducers";

const selectManageJobAdvertisementsState = createFeatureSelector<ManageJobAdvertisementsState>(ManageJobAdvertisementsFeatureKey);

export const selectJobAdvertisementsWithAppliciants = createSelector(selectManageJobAdvertisementsState, (state) => state.companyManageJobAdvertisementsDto);

