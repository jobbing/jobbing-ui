import { createReducer, on } from "@ngrx/store";
import { ManageJobAdvertisementsAction } from ".";
import { CompanyManageJobAdvertisementsDto } from "../model/company-manage-job-advertisement-dto";


export const ManageJobAdvertisementsFeatureKey = 'manage-job-advertisements';

export interface ManageJobAdvertisementsState {
    companyManageJobAdvertisementsDto: CompanyManageJobAdvertisementsDto[];
}

export const initialState: ManageJobAdvertisementsState = {
    companyManageJobAdvertisementsDto: [],
}

export const ManageJobAdvertisementsReducer = createReducer(
    initialState,
    on(ManageJobAdvertisementsAction.getJobAdvertisementsWithApplicantsSuccess, (state, action) => ({
        ...state,
        companyManageJobAdvertisementsDto: action.companyManageJobAdvertisementsDto
    })),
    
);