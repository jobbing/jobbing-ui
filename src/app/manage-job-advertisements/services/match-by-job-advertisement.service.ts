import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CompanyManageJobAdvertisementsDto } from "../model/company-manage-job-advertisement-dto";
import { jobseekerMatchDto } from "../model/jobseeker-match-dto";

@Injectable()
export class MatchByJobAdvertisementService{

    private readonly URL = '/match-by-advertisement';

    constructor(
        private http: HttpClient
    ) { }

    companyAcceptJobseekerByAdvertisement(jobseekerMatchDto: jobseekerMatchDto){
        let reqUrl = `${this.URL}/company-accept-jobseeker-by-advertisement`;
        return this.http.post<void>(reqUrl, jobseekerMatchDto);
    }

    companyRejectJobseekerByAdvertisement(jobseekerMatchDto: jobseekerMatchDto){
        let reqUrl = `${this.URL}/company-reject-jobseeker-by-advertisement`;
        return this.http.post<void>(reqUrl, jobseekerMatchDto);
    }

    jobseekerAcceptJobseekerByAdvertisement(jobseekerMatchDto: jobseekerMatchDto){
        let reqUrl = `${this.URL}/jobseeker-accept-job-advertisement`;
        return this.http.post<void>(reqUrl, jobseekerMatchDto);
    }

    jobseekerRejectJobseekerByAdvertisement(jobseekerMatchDto: jobseekerMatchDto){
        let reqUrl = `${this.URL}/jobseeker-reject-job-advertisement`;
        return this.http.post<void>(reqUrl, jobseekerMatchDto);
    }
}