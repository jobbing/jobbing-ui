import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { CompanyManageJobAdvertisementsDto } from "../model/company-manage-job-advertisement-dto";

@Injectable()
export class ManageJobAdvertisementsService{

    private readonly URL = '/company-manage-job-advertisements';

    constructor(
        private http: HttpClient
    ) { }

    getJobAdvertisementsWithApplicants(companyId: Number, paging: any){
        let reqUrl = `${this.URL}/page?page=${paging.pageStart}&size=${paging.pageSize}&companyId=`+companyId;
        return this.http.post<CompanyManageJobAdvertisementsDto[]>(reqUrl, companyId);
    }
}