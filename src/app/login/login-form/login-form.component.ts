import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AuthActions, fromAuthStore } from 'src/app/core/auth/store';
import { HolderType } from 'src/app/core/model/holder-type';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  loginForm!: FormGroup;
  hide = false;
  holderType: HolderType;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<fromAuthStore.AuthState>,
    private router: Router

  ) {
    this.createForm();
    this.holderType = HolderType.COMPANY;
    this.setHoldertype();
  }

  ngOnInit(): void {

  }

  login() {
      if(this.loginForm.valid){
      this.store.dispatch(
        AuthActions.login(
          {
            userLogin: {
              password: this.loginForm.get("password")?.value,
              username: this.loginForm.get("username")?.value
            },
            holderType: this.holderType
          }
        ));
    }
}

  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  private setHoldertype() {
    this.router.url.includes("jobseeker") ?
      this.holderType = HolderType.JOBSEEKER
      : this.holderType = HolderType.COMPANY;
  }

}
