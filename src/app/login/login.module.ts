import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from '@angular/forms';
import { MAT_DATE_LOCALE } from "@angular/material/core";
import { TranslateModule } from "@ngx-translate/core";
import { PublicHeaderComponent } from "../shared/public-header/public-header.component";
import { SharedModule } from "../shared/shared.module";
import { LoginFormComponent } from "./login-form/login-form.component";
import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./login.component";

@NgModule({
    imports: [
        ReactiveFormsModule,
        LoginRoutingModule,
        CommonModule,
        SharedModule,
        TranslateModule.forRoot()
    ],
    declarations: [
        LoginComponent,
        LoginFormComponent
    ],
    providers: [
        PublicHeaderComponent
    ]
})
export class LoginModule{}