import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HeaderComponent } from "../shared/header/header.component";
import { SharedModule } from "../shared/shared.module";
import { JobseekerSettingsComponent } from './containers/jobseeker-settings/jobseeker-settings.component';
import { SettingsRoutingModule } from "./settings-routing.module";
import { SettingsSideNavComponent } from './components/settings-side-nav/settings-side-nav.component';
import { CompanySettingsComponent } from './containers/company-settings/company-settings.component';
import { SettingsBasicComponent } from './components/settings-basic/settings-basic.component';
import { StoreModule } from "@ngrx/store";
import { fromSettingsStore } from "./store";
import { EffectsModule } from "@ngrx/effects";
import { SettingsEffects } from "./store/settings.effects";
import { SettingsService } from "./services/settings.service";
import { ReactiveFormsModule } from "@angular/forms";


@NgModule({
    imports:[
        ReactiveFormsModule,
        SettingsRoutingModule,
        SharedModule,
        CommonModule,
        StoreModule.forFeature(
            fromSettingsStore.settingsFeatrureKey,
            fromSettingsStore.settingsReducer
        ),
        EffectsModule.forFeature([SettingsEffects])
    ],
    declarations:[
        JobseekerSettingsComponent,
        SettingsSideNavComponent,
        CompanySettingsComponent,
        SettingsBasicComponent
  ],
    providers:[
        HeaderComponent,
        SettingsService
    ]
})

export class SettingsModule{ }