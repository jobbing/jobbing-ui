import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { UserData } from 'src/app/core/model/user-data';
import { fromSettingsStore, SettingsActions, SettingsSelectors } from '../../store';

@Component({
  selector: 'app-jobseeker-settings',
  templateUrl: './jobseeker-settings.component.html',
  styleUrls: ['./jobseeker-settings.component.scss']
})
export class JobseekerSettingsComponent implements OnInit {

  userData$!: Observable<UserData | null>;
  myData$: Observable<GetMyDatasDto | null>;
  myData!: GetMyDatasDto | null;

  constructor(
    private store: Store<fromSettingsStore.SettingsState>,
    private authStore: Store<fromAuthStore.AuthState>
  ) {
    this.myData$ = this.store.pipe(select(AuthSelectors.selectMyDatas));
    this.userData$ = this.store.pipe(select(SettingsSelectors.selectUserData));
   }

  ngOnInit(): void {
    this.getMyData();
    if(this.myData){
      this.store.dispatch(
        SettingsActions.loadUserData({username:this.myData?.username})
      )
    }
  }

  changeName(changeNameForm: any){
    if(this.myData){
    this.store.dispatch(
      SettingsActions.changeName({
        changeNameDto:{
          username: this.myData.username,
          newName: changeNameForm.name
        }
      }
      )
    )}
  }

  changeUsername(changeUsernameForm: any){
    if(this.myData){
    this.store.dispatch(
      SettingsActions.changeUsername({
        changeUserameDto:{
          username:this.myData.username,
          newUsername: changeUsernameForm.newUsername
        }
      })
    )
  }
  }

  changePassword(changePasswordForm: any){
    if(this.myData){
    this.store.dispatch(
      SettingsActions.changePassword({
        changePasswordDto:{
          username: this.myData.username,
          oldPassword: changePasswordForm.oldPassword,
          newPassword: changePasswordForm.newPassword
        }
      })
    )
    }
  }

  changeEmail(changeEmailForm: any){
    if(this.myData){
    this.store.dispatch(
      SettingsActions.changeEmail({
        changEmailDto:{
          username: this.myData.username,
          newEmail: changeEmailForm.newEmail
        }
      })
    )
  }
  }

  private getMyData(){
    this.myData$.subscribe(data => this.myData = data);
  }

}
