import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthActions, AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { getMyDatas } from 'src/app/core/auth/store/auth.actions';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { HolderType } from 'src/app/core/model/holder-type';
import { UserData } from 'src/app/core/model/user-data';
import { fromSettingsStore, SettingsActions, SettingsSelectors } from '../../store';

@Component({
  selector: 'app-company-settings',
  templateUrl: './company-settings.component.html',
  styleUrls: ['./company-settings.component.scss']
})
export class CompanySettingsComponent implements OnInit {

  userData$!: Observable<UserData | null>;
  myData$!: Observable<GetMyDatasDto | null>;
  username!: string;

  constructor(
    private store: Store<fromSettingsStore.SettingsState>,
    private authStore: Store<fromAuthStore.AuthState>
  ) { 
    this.myData$ = this.store.pipe(select(AuthSelectors.selectMyDatas))
    this.userData$ = this.store.pipe(select(SettingsSelectors.selectUserData));
    
  }

  ngOnInit(): void {
    this.authStore.dispatch(
      AuthActions.getMyDatas({holderType:HolderType.COMPANY})
    )
    this.getUsername();
    this.store.dispatch(
      SettingsActions.loadUserData({username: this.username})
    )
  }

  
  changeName(changeNameForm: any){
    this.store.dispatch(
      SettingsActions.changeName({
        changeNameDto:{
          username: this.username,
          newName: changeNameForm.name
        }
      }
      )
    )
  }

  changeUsername(changeUsernameForm: any){
    this.store.dispatch(
      SettingsActions.changeUsername({
        changeUserameDto:{
          username: this.username,
          newUsername: changeUsernameForm.newUsername
        }
      })
    )
  }

  changePassword(changePasswordForm: any){
    this.store.dispatch(
      SettingsActions.changePassword({
        changePasswordDto:{
          username: this.username,
          oldPassword: changePasswordForm.oldPassword,
          newPassword: changePasswordForm.newPassword
        }
      })
    )
  }

  changeEmail(changeEmailForm: any){
    this.store.dispatch(
      SettingsActions.changeEmail({
        changEmailDto:{
          username: this.username,
          newEmail: changeEmailForm.newEmail
        }
      })
    )
  }

  getUsername(){
    this.myData$.subscribe(event => this.username = event!.username)
  }

}
