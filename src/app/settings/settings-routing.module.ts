import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CompanySettingsComponent } from "./containers/company-settings/company-settings.component";
import { JobseekerSettingsComponent } from "./containers/jobseeker-settings/jobseeker-settings.component";


const routes: Routes = [
    { path: 'jobseeker', component: JobseekerSettingsComponent},
    { path: 'company', component: CompanySettingsComponent}
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class SettingsRoutingModule {}