import { createAction, props } from "@ngrx/store";
import { ChangeEmailDto } from "src/app/core/model/change-email-dto";
import { ChangeNameDto } from "src/app/core/model/change-name-dto";
import { ChangePasswordDto } from "src/app/core/model/change-password-dto";
import { ChangeUsernameDto } from "src/app/core/model/change-username-dto";
import { FailData } from "src/app/core/model/fail-message.model";
import { UserData } from "src/app/core/model/user-data";



const prefix = '[settings]';

export const loadUserData = createAction(
    `${prefix} load user data`,
    props<{ username: String}>()
);

export const loadUserDataSuccess = createAction(
    `${prefix} load user data success`,
    props<{ userData: UserData }>()
);

export const loadUserDataFail = createAction(
    `${prefix} load user data fail`,
    props<{ failData: FailData }>()
);

export const changeName = createAction(
    `${prefix} change name`,
    props<{ changeNameDto: ChangeNameDto}>()
);

export const changeNameSuccess = createAction(
    `${prefix} change name success`
);

export const changeNameFail = createAction(
    `${prefix} change name fail`,
    props<{ failData: FailData}>()
);

export const changeUsername = createAction(
    `${prefix} change username`,
    props<{ changeUserameDto: ChangeUsernameDto}>()
);

export const changeUsernameSuccess = createAction(
    `${prefix} change username success`
);

export const changeUsernameFail = createAction(
    `${prefix} change username fail`,
    props<{ failData: FailData}>()
);


export const changePassword = createAction(
    `${prefix} change PasschangePassword`,
    props<{ changePasswordDto: ChangePasswordDto}>()
);

export const changePasswordSuccess = createAction(
    `${prefix} change password success`
);

export const changePasswordFail = createAction(
    `${prefix} change password fail`,
    props<{ failData: FailData}>()
);


export const changeEmail = createAction(
    `${prefix} change email`,
    props<{ changEmailDto: ChangeEmailDto}>()
);

export const changeEmailSuccess = createAction(
    `${prefix} change email success`
);

export const changeEmailFail = createAction(
    `${prefix} change email fail`,
    props<{ failData: FailData}>()
);