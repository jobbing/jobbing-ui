import { Injectable } from "@angular/core";
import { Actions, concatLatestFrom, createEffect, ofType } from "@ngrx/effects";
import { map, switchMap } from "rxjs";
import { SettingsActions } from ".";
import { SettingsService } from "../services/settings.service";


@Injectable()
export class SettingsEffects{

    loadUserData$ = createEffect(() => this.actions$
        .pipe(
            ofType(SettingsActions.loadUserData),
            switchMap(action =>
                this.settingsService.loadUserData(action.username)
                    .pipe(
                        map(userData => SettingsActions.loadUserDataSuccess({ userData }))
                    )
            )
        ));

    changeName$ = createEffect(() => this.actions$
        .pipe(
            ofType(SettingsActions.changeName),
            switchMap(action =>
                this.settingsService.changeName(action.changeNameDto)
                    .pipe(
                        map(() => SettingsActions.changeNameSuccess())
                    )
            )
        ));

    changeUsername$ = createEffect(() => this.actions$
        .pipe(
            ofType(SettingsActions.changeUsername),
            switchMap(action =>
                this.settingsService.changeUsername(action.changeUserameDto)
                    .pipe(
                        map(() => SettingsActions.changeUsernameSuccess())
                    )
            )
        ));

    changePassword$ = createEffect(() => this.actions$
        .pipe(
            ofType(SettingsActions.changePassword),
            switchMap(action =>
                this.settingsService.changePassword(action.changePasswordDto)
                    .pipe(
                        map(() => SettingsActions.changePasswordSuccess())
                    )
            )
        ));

    changeEmail$ = createEffect(() => this.actions$
        .pipe(
            ofType(SettingsActions.changeEmail),
            switchMap(action =>
                this.settingsService.changeEmail(action.changEmailDto)
                    .pipe(
                        map(() => SettingsActions.changeEmailSuccess())
                    )
            )
        ));

    constructor(
        private actions$: Actions,
        private settingsService: SettingsService
    ){}

}