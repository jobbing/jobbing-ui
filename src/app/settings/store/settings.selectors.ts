import { createFeatureSelector, createSelector } from "@ngrx/store";
import { settingsFeatrureKey, SettingsState } from "./settings.reducers";



const selectsettingsState = createFeatureSelector<SettingsState>(settingsFeatrureKey);

export const selectUserData = createSelector(selectsettingsState, (state) => state.userData);
