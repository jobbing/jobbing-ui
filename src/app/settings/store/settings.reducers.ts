import { createReducer, on } from "@ngrx/store";
import { UserData } from "src/app/core/model/user-data";
import { SettingsActions } from ".";



export const settingsFeatrureKey = 'settings';

export interface SettingsState{
    userData: UserData | null;
}

export const initialState: SettingsState = {
    userData: null,
}

export const settingsReducer = createReducer(
    initialState,
    on(SettingsActions.loadUserDataSuccess, (state, action) => ({
        ...state,
        userData: action.userData
    }))
);