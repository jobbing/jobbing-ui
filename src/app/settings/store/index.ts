export * as SettingsActions from './settings.actions';
export * as fromSettingsStore from './settings.reducers';
export * as SettingsSelectors from './settings.selectors';

