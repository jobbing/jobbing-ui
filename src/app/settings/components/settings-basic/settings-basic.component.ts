import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserData } from 'src/app/core/model/user-data';
import { SettingsType } from 'src/app/shared/enums/settings-type.enum';

@Component({
  selector: 'app-settings-basic',
  templateUrl: './settings-basic.component.html',
  styleUrls: ['./settings-basic.component.scss']
})
export class SettingsBasicComponent implements OnInit {

  @Output() changeNameEvent: EventEmitter<any>; 
  @Output() changeUsernameEvent: EventEmitter<any>; 
  @Output() changePasswordEvent: EventEmitter<any>; 
  @Output() changeEmailEvent: EventEmitter<any>; 
  @Input() userData!: UserData;
  modifyData = false;
  toSet = '';
  changeNameForm!: FormGroup;
  changeUsernameForm!: FormGroup;
  changePasswordForm!: FormGroup;
  changeEmailForm!: FormGroup;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
  ) {
    this.changeNameEvent = new EventEmitter();
    this.changeUsernameEvent = new EventEmitter();
    this.changePasswordEvent = new EventEmitter();
    this.changeEmailEvent = new EventEmitter();
   }

  ngOnInit(): void {
    this.createChangeNameForm();
    this.createChangeUsernameForm();
    this.createChangePasswordForm();
    this.createChangeEmailForm();
  }

  changeName(){
    this.changeNameEvent.emit(this.changeNameForm.value);
  }

  changeUsername(){
    this.changeUsernameEvent.emit(this.changeUsernameForm.value);
  }

  changePassword(){
    this.changePasswordEvent.emit(this.changePasswordForm.value);
  }

  changeEmail(){
    this.changeEmailEvent.emit(this.changeEmailForm.value);
  }

  isCompany(){
    if (this.router.url.includes("company")) {
      return true;
    }
    else{
      return false;
    }
  }

  modifyPopupStatusChange(data: string){
    if(data === 'exit'){
      this.modifyData = false;
    }
    else{
      this.modifyData = true;
      if( data == 'name'){
        this.toSet = SettingsType.name;
      }
      else if( data == 'username'){
        this.toSet = SettingsType.username;
      }
      else if( data == 'email'){
        this.toSet = SettingsType.email;
      }
      else if( data == 'password'){
        this.toSet = SettingsType.password;
      }
    }
  }

  private createChangeNameForm(){
    this.changeNameForm = this.formBuilder.group({
      name: [null, Validators.required]
    })
  }

  private createChangeUsernameForm(){
    this.changeUsernameForm = this.formBuilder.group({
      newUsername: [null, Validators.required]
    })
  }

  private createChangePasswordForm(){
    this.changePasswordForm = this.formBuilder.group({
      oldPassword: [null, Validators.required],
      newPassword: [null, Validators.required]
    })
  }

  private createChangeEmailForm(){
    this.changeEmailForm = this.formBuilder.group({
      newEmail: [null, Validators.required]
    })
  }

}
