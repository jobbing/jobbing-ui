import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { ChangeEmailDto } from "src/app/core/model/change-email-dto";
import { ChangeNameDto } from "src/app/core/model/change-name-dto";
import { ChangePasswordDto } from "src/app/core/model/change-password-dto";
import { ChangeUsernameDto } from "src/app/core/model/change-username-dto";
import { GetDrivingLicencesDto } from "src/app/core/model/get-driving-licences-dto";
import { GetLanguageExamsDto } from "src/app/core/model/get-language-exams-dto";
import { GetMyDatasDto } from "src/app/core/model/get-my-datas-dto";
import { HolderType } from "src/app/core/model/holder-type";
import { JobseekerProfileData } from "src/app/core/model/jobseeker-profile-data";
import { SaveDrivingLicenceDto } from "src/app/core/model/save-driving-licence-dto";
import { SaveLanguageExamDto } from "src/app/core/model/save-language-exam-dto";
import { UpdateIntroductionDto } from "src/app/core/model/update-introduction-dto";
import { UpdatePersonalDataDto } from "src/app/core/model/update-personal-data-dto";
import { UpdateWorkILookForDto } from "src/app/core/model/update-work-i-look-for-dto";
import { UserData } from "src/app/core/model/user-data";

@Injectable()
export class SettingsService {

    private readonly URL = '/user';

    constructor(
        private http: HttpClient
    ) { }

    loadUserData(username: String): Observable<UserData>{
        let reqUrl = `${this.URL}/setting-datas?username=`+username;
        return this.http.get<UserData>(reqUrl);
    }

    changeName(changeNameDto: ChangeNameDto){
        let reqUrl = `${this.URL}/change-name`;
        return this.http.post<void>(reqUrl, changeNameDto);
    }

    changeUsername(changeUsernameDto: ChangeUsernameDto){
        let reqUrl = `${this.URL}/change-username`;
        return this.http.post<void>(reqUrl, changeUsernameDto);
    }

    changePassword(changePasswordDto: ChangePasswordDto){
        let reqUrl = `${this.URL}/change-password`;
        return this.http.post<void>(reqUrl, changePasswordDto);
    }

    changeEmail(changeEmailDto: ChangeEmailDto){
        let reqUrl = `${this.URL}/change-email`;
        return this.http.post<void>(reqUrl, changeEmailDto);
    }

    getMyDatas(holderType: HolderType): Observable<GetMyDatasDto>{
        let reqUrl = `${this.URL}/get-my-datas?holderType=`+holderType;
        return this.http.get<GetMyDatasDto>(reqUrl);
        
    }
} 

