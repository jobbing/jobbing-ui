import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthActions, fromAuthStore } from './core/auth/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'jobbing-ui';

  constructor(private store: Store<fromAuthStore.AuthState>) { }

  ngOnInit(): void {
    this.store.dispatch(AuthActions.autoLogin());
  }
}
