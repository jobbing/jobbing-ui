import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'login',
  loadChildren: () => import('./login/login.module').then(m => m.LoginModule)},
  
  { path: 'register',
  loadChildren: () => import('./register/register.module').then(m => m.RegisterModule)},

  { path: 'profile',
  loadChildren: () => import('./profile/profile.module').then(m => m.ProfileModule)},

  { path: 'settings',
  loadChildren: () => import('./settings/settings.module').then(m => m.SettingsModule)},

  { path: 'job-advertisement',
  loadChildren: () => import('./shared/shared.module').then(m => m.SharedModule)},

  { path: 'manage-jobseekers',
  loadChildren: () => import('./manage-jobseekers/manage-jobseekers.module').then(m => m.ManageJobseekersModule)},

  {path: 'manage-job-advertisements',
  loadChildren: () => import('./manage-job-advertisements/manage-job-advertisements.module').then(m => m.ManageJobAdvertisementsModule)},

  {path: 'jobseeker-applications',
  loadChildren: () => import('./jobseeker-applications/jobseeker-applications.module').then(m => m.JobseekerApplicationsModule)},

  {path: 'jobseeker-find',
  loadChildren: () => import('./jobseeker-find/jobseeker-find.module').then(m => m.JobseekerFindModule)}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
