import { Injectable } from "@angular/core";
import {
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { catchError, Observable, switchMap, take, throwError } from "rxjs";
import { select, Store } from "@ngrx/store";
import { AuthState } from "../auth/store/auth.reducers";
import { AuthSelectors } from "../auth/store";


const TOKEN_HEADER_KEY = 'Authorization';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(
    private store: Store<AuthState>
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authReq = req;
    if (!req.url.includes('public')) {
      return this.getToken().pipe(switchMap(token => {
        if (token) {
          authReq = this.setAuthToken(token, req);
        }
        return next.handle(authReq);
      }))
    }

    return next.handle(authReq);
  }

  private setAuthToken(token: string, req: HttpRequest<any>): HttpRequest<any>{
    return req.clone({
      headers: req.headers.set(TOKEN_HEADER_KEY, `Bearer ${token}`)
    });
  }
    

  private getToken(): Observable<string | null> {
    return this.store.pipe(select(AuthSelectors.selectToken), take(1));
  }

}