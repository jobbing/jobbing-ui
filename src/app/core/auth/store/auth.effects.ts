import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { Store } from "@ngrx/store";
import { catchError, from, map, Observable, of, switchMap, tap } from "rxjs";
import { SettingsService } from "src/app/settings/services/settings.service";
import { AuthActions } from ".";
import { HolderType } from "../../model/holder-type";
import { NotificationService } from "../../notification/notification.service";
import { AuthService } from "../auth.service";

@Injectable()
export class AuthEffects {
    private _TOKEN_KEY = 'token';

    login$ = createEffect(() => this.actions$
        .pipe(
            ofType(AuthActions.login),
            switchMap(action =>
                this.authService.login(action.userLogin, action.holderType)
                    .pipe(
                        map(authResponse => AuthActions.loginSuccess({authResponse}))
                    )
            )
        ));

    autoLogin$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthActions.autoLogin),
            switchMap(() => {
                return of(localStorage.getItem(this._TOKEN_KEY)).pipe(
                    map(token => {
                        if (token) {
                            return AuthActions.loginSuccess(
                                {
                                    authResponse:
                                    {
                                        jwt: token,
                                        holderType: HolderType.JOBSEEKER
                                    }
                                }
                            );
                        }

                        return { type: 'EMPTY' };
                    }));
            })
        )
    );

    getMyDatas$ = createEffect(() => this.actions$
    .pipe(
        ofType(AuthActions.loginSuccess),
        switchMap(action => 
            this.userService.getMyDatas(action.authResponse.holderType)
                .pipe(
                    map(getMyDatasDto => AuthActions.getMyDatasSuccess({getMyDatasDto}))
                ))
    ))

    register$ = createEffect(() => this.actions$
        .pipe(
            ofType(AuthActions.register),
            switchMap(action =>
                this.authService.register(action.register, action.holderType)
                    .pipe(
                        map(() => AuthActions.registerSuccess()),
                        catchError((error) => of(AuthActions.registerFail(error)))
                    )
            ),

        )
    )

    registerSuccess$ = createEffect(() => this.actions$
        .pipe(
            ofType(AuthActions.registerSuccess),
            tap((action) => {
                this.router.navigate(['login', 'jobseeker']);
            })
        ), { dispatch: false });


    loginSuccess$ = createEffect(() => this.actions$
        .pipe(
            ofType(AuthActions.loginSuccess),
            tap((action) => {
                localStorage.setItem(this._TOKEN_KEY, action.authResponse.jwt);

                if (action.authResponse.holderType == HolderType.COMPANY) {
                    this.router.navigate(['profile', 'company']);
                }
                else if (action.authResponse.holderType == HolderType.JOBSEEKER) {
                    this.router.navigate(['profile', 'jobseeker']);
                }
            }),
        ), { dispatch: false }); 


    constructor(
        private notificationService: NotificationService,
        private actions$: Actions,
        private authService: AuthService,
        private userService: SettingsService,
        private store$: Store<any>,
        private router: Router
    ) { }
}
