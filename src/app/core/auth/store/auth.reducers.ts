import { createReducer, on } from "@ngrx/store";
import { AuthActions } from ".";
import { GetMyDatasDto } from "../../model/get-my-datas-dto";

export const authFeatrureKey = 'auth';

export interface AuthState {
  token: string | null;
  mydata: GetMyDatasDto | null;
}

export const initialState: AuthState = {
  token: null,
  mydata: null
};


export const authReducer = createReducer(
  initialState,
  on(AuthActions.loginSuccess, (state, action) => ({
    ...state,
    token: action.authResponse.jwt
  })),
  on(AuthActions.getMyDatasSuccess, (state,action) => ({
    ...state,
    mydata: action.getMyDatasDto
  })),
  on(AuthActions.logout, (state, action) => ({
    ...state,
    token: null
  }))
);
