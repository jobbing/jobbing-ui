import { createAction, props } from "@ngrx/store";
import { AuthResponseData } from "../../model/auth-response-data";
import { FailData } from "../../model/fail-message.model";
import { UserLogin } from "../../model/user-login.model";
import { Register } from "../../model/register.model";
import { HolderType } from "../../model/holder-type";
import { GetMyDatasDto } from "../../model/get-my-datas-dto";

const prefix = '[Auth]';

export const getMyDatas = createAction(
    `${prefix} get my datas`,
    props<{holderType: HolderType}>()
);

export const getMyDatasSuccess = createAction(
    `${prefix} get my datas success`,
    props<{getMyDatasDto: GetMyDatasDto}>()
);

export const login = createAction(
    `${prefix} login`,
    props<{ userLogin: UserLogin, holderType: HolderType}>()
);

export const autoLogin = createAction(
    `${prefix} auto login`,
);

export const loginSuccess = createAction(
    `${prefix} login success`,
    props<{ authResponse: AuthResponseData }>()
);

export const loginFail = createAction(
    `${prefix} login fail`,
    props<{ failData: FailData }>()
);

export const register = createAction(
    `${prefix} register`,
    props<{ register: Register,holderType: HolderType }>()
); 

export const registerSuccess = createAction(
    `${prefix} register success`
);

export const registerFail = createAction(
    `${prefix} register fail`,
    props<{ failData: FailData }>()
); 

export const logout = createAction(
    `${prefix} logout`
)
