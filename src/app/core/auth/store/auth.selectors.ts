import { createFeatureSelector, createSelector } from "@ngrx/store";
import { authFeatrureKey, AuthState } from "./auth.reducers";

const selectAuthState = createFeatureSelector<AuthState>(authFeatrureKey);

export const selectToken = createSelector(selectAuthState, (state) => state.token);

export const selectMyDatas = createSelector(selectAuthState, (state) => state.mydata);

export const selectHolderType = createSelector(selectAuthState, (state) => state.mydata!.holderType);

