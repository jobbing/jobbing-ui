import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthResponseData } from "../model/auth-response-data";
import { UserLogin } from "../model/user-login.model";
import { Register } from "../model/register.model";
import { HolderType } from "../model/holder-type";

@Injectable()
export class AuthService {

    private readonly URL = '/public';

    constructor(
        private http: HttpClient
    ) { }

    login(userLogin: UserLogin, holderType: HolderType) {
        let reqUrl = `${this.URL}/login`;
        return this.http.post<AuthResponseData>(reqUrl, userLogin);
    }

    chooseSide(){
        let reqUrl = `${this.URL}/is-jobseker`;
        return this.http.get<Boolean>(reqUrl);
    }

    register(register: Register, holderType: HolderType){
        if(holderType == HolderType.JOBSEEKER){
            return this.jobseekerRegister(register);
        }else{
            return this.companyRegister(register);

        }
    }

    private jobseekerRegister(register: Register){
        let reqUrl  = `${this.URL}/register-jobseeker`;
        return this.http.post<number>(reqUrl, register);
    }

    private companyRegister(register: Register){
        let reqUrl  = `${this.URL}/register-company`;
        return this.http.post<number>(reqUrl, register);
    }
}