import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule } from '@ngx-translate/core';
import { RouterModule } from "@angular/router";
import { AuthService } from "./auth/auth.service";
import { StoreModule } from "@ngrx/store";
import { fromAuthStore } from "./auth/store";
import { EffectsModule } from "@ngrx/effects";
import { AuthEffects } from "./auth/store/auth.effects";
import { TokenInterceptor } from "./interceptors/token.interceptor";
import { ApiPrefixInterceptor } from "./interceptors/api-prefix.interceptor";
import { SettingsService } from "../settings/services/settings.service";

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        TranslateModule,
        StoreModule.forFeature(fromAuthStore.authFeatrureKey, fromAuthStore.authReducer),
        EffectsModule.forFeature([AuthEffects]),
        RouterModule
    ],
    providers: [
        AuthService,
        SettingsService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ApiPrefixInterceptor,
            multi: true
        }
    ],

})

export class CoreModule {
    constructor() { }
}