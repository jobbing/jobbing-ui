export interface CompanyProfileData{
    
    id: number;
    name: string;
    email: string;
    logo: string;
    introduction: string;
    phoneNumber: string;
    birthTime: Date;
    address: string;
    contactPerson: string;
}