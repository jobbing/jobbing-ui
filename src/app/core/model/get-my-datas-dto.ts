import { PermissionType } from "src/app/shared/enums/permission-type.enum"
import { HolderType } from "./holder-type"

export interface GetMyDatasDto{

    username: string,
    name: string,
    userId: number,
    holderType: HolderType,
    jwt: string,
    jobseekerId: number,
    companyId: number,
    permissions: PermissionType[]
}