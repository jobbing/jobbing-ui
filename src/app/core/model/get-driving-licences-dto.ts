import { DrivingLicence } from "src/app/shared/enums/driving-licence.enum";

export interface GetDrivingLicencesDto{

    value:DrivingLicence

}