import { LanguageType } from "src/app/shared/enums/language-type.enum";
import { LevelType } from "src/app/shared/enums/level-type.enum";

export interface LanguageExam{

    languageExamId: number,
    language: LanguageType,
    level: LevelType

}