import { DrivingLicence } from "src/app/shared/enums/driving-licence.enum";

export interface SaveDrivingLicenceDto{

    jobseekerId: number,
    value: DrivingLicence[]
}