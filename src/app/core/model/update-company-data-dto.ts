export interface UpdateCompanyDataDto{

    companyId: number;
    phoneNumber: string;
    email: string;
    address: string;
    contactPerson: string;
}