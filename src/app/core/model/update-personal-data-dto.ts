export interface UpdatePersonalDataDto{

    jobseekerId: number,
    birthPlace: string,
    birthTime: Date,
    address: string,
    residence: string

}