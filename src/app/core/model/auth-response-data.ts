import { HolderType } from "./holder-type"

export interface AuthResponseData {
    holderType: HolderType;
    jwt: string;
}