export interface JobseekerProfileData{
    
    id: number;
    name: string;
    picture: string;
    cv: string;
    introduction: string;
    birthPlace: string;
    birthTime: Date;
    email: string;
    address: string;
    residence: string;
    workILookFor: string;

}