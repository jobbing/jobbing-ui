export interface Register {

    name: string;
    companyName?: string;
    username: string;
    email: string;
    password: string;

}