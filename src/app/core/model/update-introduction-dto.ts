export interface UpdateIntroductionDto{

    jobseekerId: number,
    introduction: string
}