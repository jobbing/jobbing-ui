import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

export interface NotificationData {
    title: string;
    message: string;
}

@Injectable({
    providedIn: 'root'
})
export class NotificationService {
    constructor(
        private toastrService: ToastrService
        ) { }

    public notificationError(notificationData: NotificationData) {
        this.toastrService.error(notificationData.message, notificationData.title, {
            positionClass: 'toast-bottom-left'
        });
    }

    public notificationSuccess(notificationData: NotificationData) {
        this.toastrService.success(notificationData.message, notificationData.title, {
            positionClass: 'toast-bottom-left'
        });
    }

    public notificationWarning(notificationData: NotificationData) {
        this.toastrService.warning(notificationData.message, notificationData.title, {
            positionClass: 'toast-bottom-left'
        });
    }

    public notificationInfo(notificationData: NotificationData) {
        this.toastrService.info(notificationData.message, notificationData.title, {
            positionClass: 'toast-bottom-left'
        });
    }
}
