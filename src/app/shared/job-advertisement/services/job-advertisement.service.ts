import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { GetListValuesDto } from "src/app/manage-job-advertisements/model/get-list-values-dto";
import { CompanyGetJobAdvertisementDto } from "../model/company-get-job-advertisement-dto";
import { JobseekerGetJobAdvertisementDto } from "../model/jobseeker-get-job-advertisement-dto";
import { SaveJobAdvertisementDto } from "../model/save-job-advertisement-dto";


@Injectable()
export class JobAdvertisementService {

    private readonly URL = '/job-advertisement';

    constructor(
        private http: HttpClient
    ){}

    saveJobAdvertisement(saveJobAdvertisementDto: SaveJobAdvertisementDto){
        let reqUrl = `${this.URL}/save`;
        return this.http.post<void>(reqUrl, saveJobAdvertisementDto);
    }

    companyGetJobAdvertisement(jobAdvertisementId: number){
        let reqUrl = `${this.URL}/company-get?jobAdvertisementId=`+jobAdvertisementId;
        return this.http.get<CompanyGetJobAdvertisementDto>(reqUrl);
    }

    jobseekerGetJobAdvertisement(jobAdvertisementId: number){
        let reqUrl = `${this.URL}/jobseeker-get?jobAdvertisementId=`+jobAdvertisementId;
        return this.http.get<JobseekerGetJobAdvertisementDto>(reqUrl);
    }

    deleteJobAdvertisement(jobAdvertisementId: number){
        let reqUrl = `${this.URL}/delete?jobAdvertisementId=`+jobAdvertisementId;
        return this.http.delete<void>(reqUrl);
    }

    getListValues(){
        let reqUrl =`${this.URL}/get-list-values`;
        return this.http.get<GetListValuesDto>(reqUrl);
    }
}