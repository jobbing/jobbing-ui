import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobseekerLoadJobAdvertisementComponent } from './jobseeker-load-job-advertisement.component';

describe('JobseekerLoadJobAdvertisementComponent', () => {
  let component: JobseekerLoadJobAdvertisementComponent;
  let fixture: ComponentFixture<JobseekerLoadJobAdvertisementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobseekerLoadJobAdvertisementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobseekerLoadJobAdvertisementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
