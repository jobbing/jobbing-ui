import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { JobseekerGetJobAdvertisementDto } from '../../model/jobseeker-get-job-advertisement-dto';
import { fromJobAdvertisementStore, JobAdvertisementActions, JobAdvertisementSelectors } from '../../store';

@Component({
  selector: 'app-jobseeker-load-job-advertisement',
  templateUrl: './jobseeker-load-job-advertisement.component.html',
  styleUrls: ['./jobseeker-load-job-advertisement.component.scss']
})
export class JobseekerLoadJobAdvertisementComponent implements OnInit {

  jobAdvertisementData$!: Observable<JobseekerGetJobAdvertisementDto | null>;
  id!: number;

  constructor(
    private store: Store<fromJobAdvertisementStore.JobAdvertisementState>,
    private activatedRoute: ActivatedRoute
  ) { 
    this.jobAdvertisementData$ = this.store.pipe(select(JobAdvertisementSelectors.selectJobseekerJobAdvertisement));
  }

  ngOnInit(): void {
    this.getId();
    this.store.dispatch(
      JobAdvertisementActions.jobseekerGetJobAdvertisement({
        jobAdvertisementId: this.id
      })
    )
  }

  getId(){
    this.id =+ this.activatedRoute.snapshot.paramMap.get('id')!;
  }

}
