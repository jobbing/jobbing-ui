import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementModifyComponent } from './job-advertisement-modify.component';

describe('JobAdvertisementModifyComponent', () => {
  let component: JobAdvertisementModifyComponent;
  let fixture: ComponentFixture<JobAdvertisementModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
