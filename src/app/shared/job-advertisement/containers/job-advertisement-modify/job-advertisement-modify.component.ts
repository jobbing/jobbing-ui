import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthActions, AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { HolderType } from 'src/app/core/model/holder-type';
import { GetListValuesDto } from 'src/app/manage-job-advertisements/model/get-list-values-dto';
import { CompanyGetJobAdvertisementDto } from '../../model/company-get-job-advertisement-dto';
import { fromJobAdvertisementStore, JobAdvertisementActions, JobAdvertisementSelectors } from '../../store';

@Component({
  selector: 'app-job-advertisement-modify',
  templateUrl: './job-advertisement-modify.component.html',
  styleUrls: ['./job-advertisement-modify.component.scss']
})
export class JobAdvertisementModifyComponent implements OnInit {

  id!: number | null;
  jobAdvertisementDataForCompany$!: Observable<CompanyGetJobAdvertisementDto | null>;
  jobAdvertisement!: CompanyGetJobAdvertisementDto | null;
  myData$!: Observable<GetMyDatasDto | null>;
  companyId!: number;
  newWorkCategoryNameList = [];
  listValues$: Observable<GetListValuesDto | null>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<fromJobAdvertisementStore.JobAdvertisementState>,
    private authStore: Store<fromAuthStore.AuthState>,
    private router: Router
  ) { 
    this.myData$ = this.authStore.pipe(select(AuthSelectors.selectMyDatas));
    this.listValues$ = this.store.pipe(select(JobAdvertisementSelectors.selectListValues));
    this.jobAdvertisementDataForCompany$ = this.store.pipe(select(JobAdvertisementSelectors.selectCompanyJobAdvertisement));
    this.jobAdvertisementDataForCompany$.subscribe(event => this.jobAdvertisement = event)
    
  }

  ngOnInit(): void {
    this.id = +this.activatedRoute.snapshot.paramMap.get('id')!;
    this.store.dispatch(
      JobAdvertisementActions.getListValues()
    )
    this.authStore.dispatch(
      AuthActions.getMyDatas({
        holderType: HolderType.COMPANY
      })
    )
    this.store.dispatch(
      JobAdvertisementActions.companyGetJobAdvertisement({
        jobAdvertisementId: this.id
      })
    )
  }

  getCompanyId(){
    this.myData$.subscribe(event => this.companyId = event!.companyId);
  }

  modifyEvent(modifyJobAdvertisementDto: any){
    this.getCompanyId();
    if(this.id){
      this.store.dispatch(
        JobAdvertisementActions.saveJobAdvertisement({
          saveJobAdvertisementDto:{
            id: this.id,
            companyId: this.companyId,
            name: modifyJobAdvertisementDto.modifyForm.name,
            place: modifyJobAdvertisementDto.modifyForm.place,
            summary: modifyJobAdvertisementDto.modifyForm.summary,
            textEditor: modifyJobAdvertisementDto.modifyForm.textEditor,
            newWorkCategoryNameList: [],
            workCategoryIdList: modifyJobAdvertisementDto.workCategorySelectedList,
            workTypeIdList: modifyJobAdvertisementDto.workingTypeSelectedList,
            cityIdList: modifyJobAdvertisementDto.citySelectedList
            
          }
        })
      )
    }
    this.router.navigate(["manage-job-advertisements","my-job-advertisements","company"])
  }

}
