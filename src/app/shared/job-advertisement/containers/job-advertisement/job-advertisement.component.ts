import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { HolderType } from 'src/app/core/model/holder-type';
import { CompanyGetJobAdvertisementDto } from '../../model/company-get-job-advertisement-dto';
import { JobseekerGetJobAdvertisementDto } from '../../model/jobseeker-get-job-advertisement-dto';
import { fromJobAdvertisementStore, JobAdvertisementActions, JobAdvertisementSelectors } from '../../store';

@Component({
  selector: 'app-job-advertisement',
  templateUrl: './job-advertisement.component.html',
  styleUrls: ['./job-advertisement.component.scss']
})
export class JobAdvertisementComponent implements OnInit {

  id!: number | null;
  jobAdvertisementDataForCompany$!: Observable<CompanyGetJobAdvertisementDto | null>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<fromJobAdvertisementStore.JobAdvertisementState>,
  ) { 
    this.jobAdvertisementDataForCompany$ = this.store.pipe(select(JobAdvertisementSelectors.selectCompanyJobAdvertisement));
  }

  ngOnInit(): void {
    this.id = +this.activatedRoute.snapshot.paramMap.get('id')!;
    this.store.dispatch(
      JobAdvertisementActions.companyGetJobAdvertisement({
        jobAdvertisementId: this.id
      })
    )
  }

  deleteJobAdvertisement(id: number){
    this.store.dispatch(
      JobAdvertisementActions.deleteJobAdvertisement({
        jobAdvertisementId: id
      }
      )
    )
  }

}
