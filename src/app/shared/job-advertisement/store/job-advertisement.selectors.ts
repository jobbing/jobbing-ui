import { createFeatureSelector, createSelector } from "@ngrx/store";
import { JobAdvertisementFeatureKey, JobAdvertisementState } from "./job-advertisement.reducers";

const selectJobAdvertisementState = createFeatureSelector<JobAdvertisementState>(JobAdvertisementFeatureKey);

export const selectCompanyJobAdvertisement = createSelector(selectJobAdvertisementState, (state) => state.companyGetJobAdvertisement);

export const selectJobseekerJobAdvertisement = createSelector(selectJobAdvertisementState, (state) => state.jobseekerGetJobAdvertisement);

export const selectListValues = createSelector(selectJobAdvertisementState, (state) => state.getListValuesDto);