import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, switchMap, tap } from "rxjs";
import { NotificationService } from "src/app/core/notification/notification.service";
import { ProfileActions } from "src/app/profile/store";
import { JobAdvertisementActions } from ".";
import { JobAdvertisementService } from "../services/job-advertisement.service";

@Injectable()
export class JobAdvertisementEffects{

    saveJobAdvertisement$ = createEffect(() => this.actions$
        .pipe(
            ofType(JobAdvertisementActions.saveJobAdvertisement),
            switchMap(action =>
                this.jobAdvertisementService.saveJobAdvertisement(action.saveJobAdvertisementDto)
                    .pipe(
                        map(() => JobAdvertisementActions.saveJobAdvertisementSuccess())
                    )
            )
        ));

    companyGetJobAdvertisement$ = createEffect(() => this.actions$
        .pipe(
            ofType(JobAdvertisementActions.companyGetJobAdvertisement),
            switchMap(action =>
                this.jobAdvertisementService.companyGetJobAdvertisement(action.jobAdvertisementId)
                    .pipe(
                        map(companyGetJobAdvertisementDto => JobAdvertisementActions.companyGetJobAdvertisementSuccess({companyGetJobAdvertisementDto}))
                    )
            )
        ));

    jobseekerGetJobAdvertisement$ = createEffect(() => this.actions$
        .pipe(
            ofType(JobAdvertisementActions.jobseekerGetJobAdvertisement),
            switchMap(action =>
                this.jobAdvertisementService.jobseekerGetJobAdvertisement(action.jobAdvertisementId)
                    .pipe(
                        map(jobseekerGetJobAdvertisementDto => JobAdvertisementActions.jobseekerGetJobAdvertisementSuccess({jobseekerGetJobAdvertisementDto}))
                    )
            )
        ));

    deleteJobAdvertisement$ = createEffect(() => this.actions$
        .pipe(
            ofType(JobAdvertisementActions.deleteJobAdvertisement),
            switchMap(action =>
                this.jobAdvertisementService.deleteJobAdvertisement(action.jobAdvertisementId)
                    .pipe(
                        map(() => JobAdvertisementActions.deleteGetJobAdvertisementSuccess())
                    )
            )
        ));

    deleteJobAdvertisementSuccess$ = createEffect(() => this.actions$
    .pipe(
        ofType(JobAdvertisementActions.deleteGetJobAdvertisementSuccess),
        tap((action) => {
            this.router.navigate(['profile','company']);
        })
    ), {dispatch: false})

    getListValues$ = createEffect(() => this.actions$
    .pipe(
        ofType(JobAdvertisementActions.getListValues),
        switchMap(action =>
            this.jobAdvertisementService.getListValues()
            .pipe(
                map(getListValuesDto => JobAdvertisementActions.getListValuesSuccess({getListValuesDto}))
            ))
    ));
    
    constructor(
        private notificationService: NotificationService,
        private actions$: Actions,
        private router: Router,
        private jobAdvertisementService: JobAdvertisementService
    ) { }

}