import { createReducer, on } from "@ngrx/store";
import { GetListValuesDto } from "src/app/manage-job-advertisements/model/get-list-values-dto";
import { JobAdvertisementActions } from ".";
import { CompanyGetJobAdvertisementDto } from "../model/company-get-job-advertisement-dto";
import { JobseekerGetJobAdvertisementDto } from "../model/jobseeker-get-job-advertisement-dto";

export const JobAdvertisementFeatureKey = 'jobAdvertisement';

export interface JobAdvertisementState{
    companyGetJobAdvertisement: CompanyGetJobAdvertisementDto | null;
    jobseekerGetJobAdvertisement: JobseekerGetJobAdvertisementDto | null;
    getListValuesDto: GetListValuesDto | null;
}

export const initialState: JobAdvertisementState = {
    companyGetJobAdvertisement: null,
    jobseekerGetJobAdvertisement: null,
    getListValuesDto: null

}

export const JobAdvertisementReducer = createReducer(
    initialState,
    on(JobAdvertisementActions.companyGetJobAdvertisementSuccess, (state, action) => ({
        ...state, 
        companyGetJobAdvertisement: action.companyGetJobAdvertisementDto
    })),
    on(JobAdvertisementActions.jobseekerGetJobAdvertisementSuccess, (state, action) => ({
        ...state,
        jobseekerGetJobAdvertisement: action.jobseekerGetJobAdvertisementDto
    })),
    on(JobAdvertisementActions.getListValuesSuccess, (state, action) => ({
        ...state,
        getListValuesDto: action.getListValuesDto
    }))
)