import { createAction, props } from "@ngrx/store";
import { FailData } from "src/app/core/model/fail-message.model";
import { GetListValuesDto } from "src/app/manage-job-advertisements/model/get-list-values-dto";
import { CompanyGetJobAdvertisementDto } from "../model/company-get-job-advertisement-dto";
import { JobseekerGetJobAdvertisementDto } from "../model/jobseeker-get-job-advertisement-dto";
import { SaveJobAdvertisementDto } from "../model/save-job-advertisement-dto";

const prefix = '[jobAdvertisement]';

export const saveJobAdvertisement = createAction(
    `${prefix} save job advertisement`,
    props<{ saveJobAdvertisementDto: SaveJobAdvertisementDto}>()
);

export const saveJobAdvertisementSuccess = createAction(
    `${prefix} save job advertisement success` 
);

export const saveJobAdvertisementFail = createAction(
    `${prefix} save job advertisement fail`,
    props<{ failData: FailData}>()
);

export const companyGetJobAdvertisement = createAction(
    `${prefix} company get job advertisement`,
    props<{ jobAdvertisementId: number}>()
);

export const companyGetJobAdvertisementSuccess = createAction(
    `${prefix} company get job advertisement success`,
    props<{ companyGetJobAdvertisementDto: CompanyGetJobAdvertisementDto}>()
);

export const companyGetJobAdvertisementFail = createAction(
    `${prefix} company get job advertisement fail`,
    props<{ failData: FailData}>()
);

export const jobseekerGetJobAdvertisement = createAction(
    `${prefix} jobseeker get job advertisement`,
    props<{ jobAdvertisementId: number}>()
);

export const jobseekerGetJobAdvertisementSuccess = createAction(
    `${prefix} jobseeker get job advertisement success`,
    props<{ jobseekerGetJobAdvertisementDto: JobseekerGetJobAdvertisementDto}>()
);

export const jobseekerGetJobAdvertisementFail = createAction(
    `${prefix} jobseeker get job advertisement fail`,
    props<{ failData: FailData}>()
);

export const deleteJobAdvertisement = createAction(
    `${prefix} delete job advertisement`,
    props<{ jobAdvertisementId: number}>()
);

export const deleteGetJobAdvertisementSuccess = createAction(
    `${prefix} delete job advertisement success`,
);

export const deleteGetJobAdvertisementFail = createAction(
    `${prefix} delete job advertisement fail`,
    props<{ failData: FailData}>()
);

export const getListValues = createAction(
    `${prefix} get list values`
);

export const getListValuesSuccess = createAction(
    `${prefix} get list values success`,
    props<{ getListValuesDto: GetListValuesDto }>()
);

export const getListValuesFail = createAction(
    `${prefix} get list values fail`,
    props<{ failData: FailData }>()
);