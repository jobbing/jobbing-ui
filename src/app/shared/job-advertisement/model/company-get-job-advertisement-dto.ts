import { WorkingType } from "../../enums/work-type.enum";

export interface CompanyGetJobAdvertisementDto{

    id: number,
    name: string,
    place: string,
    summary: string,
    textEditor: string,
    workCategoryNameList: string[],
    workTypeNameList: WorkingType[],
    cityNameList: string[],
}