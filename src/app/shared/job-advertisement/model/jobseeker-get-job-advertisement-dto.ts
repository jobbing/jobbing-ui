import { WorkingType } from "../../enums/work-type.enum";

export interface JobseekerGetJobAdvertisementDto{

    companyId: number,
    companyName: string,
    companyLogo: string,
    companyIntroduction: string,
    id: number,
    name: string,
    place: string,
    summary: string,
    textEditor: string,
    workCategoryNameList: string[],
    workTypeNameList: WorkingType[],
    cityNameList: string[],
}