export interface SaveJobAdvertisementDto{

    companyId: number,
    id?: number,
    name: string,
    place: string,
    summary: string,
    textEditor: string,
    workCategoryIdList: number[],
    workTypeIdList: number[],
    cityIdList: number[],
    newWorkCategoryNameList: string[],
}