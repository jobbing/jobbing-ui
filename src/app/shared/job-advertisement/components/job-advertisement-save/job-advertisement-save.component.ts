import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { EditorChangeContent, EditorChangeSelection } from 'ngx-quill';
import { Observable } from 'rxjs';
import { AuthActions, fromAuthStore } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { HolderType } from 'src/app/core/model/holder-type';
import { GetListValuesDto } from 'src/app/manage-job-advertisements/model/get-list-values-dto';
import { CityType, CITY_TYPES } from 'src/app/shared/enums/city-type.enum';
import { WorkCategory, WORK_CATEGORIES } from 'src/app/shared/enums/work-category.enum';
import { WorkingType, WORKING_TYPES } from 'src/app/shared/enums/work-type.enum';
import { CompanyGetJobAdvertisementDto } from '../../model/company-get-job-advertisement-dto';
import { fromJobAdvertisementStore, JobAdvertisementActions } from '../../store';

@Component({
  selector: 'app-job-advertisement-save',
  templateUrl: './job-advertisement-save.component.html',
  styleUrls: ['./job-advertisement-save.component.scss']
})
export class JobAdvertisementSaveComponent implements OnInit {

  @Input() jobAdvertisementDataForCompany!: CompanyGetJobAdvertisementDto | null;
  @Input() listValues!: GetListValuesDto | null;
  @Output() saveJobAdvertisementEvent: EventEmitter<any>;
  myData$!: Observable<GetMyDatasDto>;

  mainText = this.jobAdvertisementDataForCompany?.textEditor;
  workingTypes = WORKING_TYPES;
  workTypeDtoSelectedList: number[] = [];
  workingTypeSelectedList: WorkingType[] = [];
  cities = CITY_TYPES;
  cityDtoSelectedList: number[] = [];
  citySelectedList: CityType[] = [];
  workCategories = WORK_CATEGORIES;
  workCategoryDtoSelectedList: number[] = [];
  workCategorySelectedList: WorkCategory[] = [];
  type = false;
  category = false;
  city = false;
  companyId!: number;

  modifyForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<fromJobAdvertisementStore.JobAdvertisementState>,
    private authStore: Store<fromAuthStore.AuthState>,
  ) {
    this.saveJobAdvertisementEvent = new EventEmitter();
    this.authStore.dispatch(
      AuthActions.getMyDatas({
        holderType: HolderType.COMPANY
      })
    )
    
   }

  ngOnInit(): void {
    this.createForm();
  }

  getCompanyId(){
    this.myData$.subscribe(event => this.companyId= event.companyId);
  }

  modify(){
    if(this.modifyForm.valid){
      for(let workTypeName of this.workingTypeSelectedList){
        if(this.listValues?.workTypes[this.workingTypeSelectedList.indexOf(workTypeName)])
        this.workTypeDtoSelectedList.push(this.listValues?.workTypes[this.workingTypeSelectedList.indexOf(workTypeName)].id);
      }
      for(let cityName of this.citySelectedList){
        if(this.listValues?.cities[this.citySelectedList.indexOf(cityName)])
        this.cityDtoSelectedList.push(this.listValues?.cities[this.citySelectedList.indexOf(cityName)].id);
      }
      for(let workCategoryName of this.workCategorySelectedList){
        if(this.listValues?.workCategories[this.workCategorySelectedList.indexOf(workCategoryName)])
        this.workCategoryDtoSelectedList.push(this.listValues?.workCategories[this.workCategorySelectedList.indexOf(workCategoryName)].id);
      }
      this.saveJobAdvertisementEvent.emit({
        modifyForm: this.modifyForm.value,
        citySelectedList: this.cityDtoSelectedList,
        workCategorySelectedList: this.workCategoryDtoSelectedList,
        workingTypeSelectedList: this.workTypeDtoSelectedList
      })
    }
  }

  selectedCities(value: any){
    if(this.citySelectedList.includes(value)){
      return true;
    }
    else{
      return false;
    }
  }

  selectedTypes(value: any){
    if(this.workingTypeSelectedList.includes(value)){
      return true;
    }
    else{
      return false;
    }
  }

  selectedCategoryes(value: any){
    if(this.workCategorySelectedList.includes(value)){
      return true;
    }
    else{
      return false;
    }
  }

  checkCitySelected(value: any){
    if(this.citySelectedList.includes(value)){
      this.removeCityFromList(value);
    }
    else{
      this.addCityToList(value);
    }
  }

  checkWorkingTypeSelected(value: any){
    if(this.workingTypeSelectedList.includes(value)){
      this.removeWorkingTypeFromList(value);
    }
    else{
      this.addWorkingTypeToList(value);
    }
  }

  checkWorkCategorySelected(value: any){
    if(this.workCategorySelectedList.includes(value)){
      this.removeWorkCategoryFromList(value);
    }
    else{
      this.addWorkCategoryToList(value);
    }
  }

  private addWorkCategoryToList(value: any){
    this.workCategorySelectedList.push(value);
  }
  
  private removeWorkCategoryFromList(value: any){
    const index: number = this.workCategorySelectedList.indexOf(value);
    this.workCategorySelectedList.splice(index, 1);

  }

  private addCityToList(value: any){
    this.citySelectedList.push(value);
  }
  
  private removeCityFromList(value: any){
    const index: number = this.citySelectedList.indexOf(value);
    this.citySelectedList.splice(index, 1);
  }

  private addWorkingTypeToList(value: any){
    this.workingTypeSelectedList.push(value);
  }
  
  private removeWorkingTypeFromList(value: any){
    const index: number = this.workingTypeSelectedList.indexOf(value);
    this.workingTypeSelectedList.splice(index, 1);
  }

  openCity(){
    this.city = true;
    this.category = false;
    this.type = false;
  }

  openCategory(){
    this.city = false;
    this.category = true;
    this.type = false;
  }
  
  openType(){
    this.city = false;
    this.category = false;
    this.type = true;
  }

  private createForm() {
    this.modifyForm = this.formBuilder.group({
      name: new FormControl(this.jobAdvertisementDataForCompany?.name, Validators.required),
      place: new FormControl(this.jobAdvertisementDataForCompany?.place, Validators.required),
      summary: new FormControl(this.jobAdvertisementDataForCompany?.summary, Validators.required),
      textEditor: new FormControl(this.jobAdvertisementDataForCompany?.textEditor)
    });
  }

}
