import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementSaveComponent } from './job-advertisement-save.component';

describe('JobAdvertisementSaveComponent', () => {
  let component: JobAdvertisementSaveComponent;
  let fixture: ComponentFixture<JobAdvertisementSaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementSaveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
