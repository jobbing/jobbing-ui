import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementEditComponent } from './job-advertisement-edit.component';

describe('JobAdvertisementEditComponent', () => {
  let component: JobAdvertisementEditComponent;
  let fixture: ComponentFixture<JobAdvertisementEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
