import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { CompanyGetJobAdvertisementDto } from '../../model/company-get-job-advertisement-dto';

@Component({
  selector: 'app-job-advertisement-edit',
  templateUrl: './job-advertisement-edit.component.html',
  styleUrls: ['./job-advertisement-edit.component.scss']
})
export class JobAdvertisementEditComponent implements OnInit {

  @Input() jobAdvertisementDataForCompany!: CompanyGetJobAdvertisementDto | null;
  @Output() deleteJobAdvertisementEvent: EventEmitter<number>; 

  constructor(
    private router: Router,
  ) { 
    this.deleteJobAdvertisementEvent = new EventEmitter();
  }

  ngOnInit(): void {
  }

  deleteJobAdvertisement(){
    this.deleteJobAdvertisementEvent.emit(this.jobAdvertisementDataForCompany?.id);
  }

  modifyJobAdvertisement(){
    this.router.navigate(['job-advertisement','modify',this.jobAdvertisementDataForCompany?.id,"company"])
  }

}
