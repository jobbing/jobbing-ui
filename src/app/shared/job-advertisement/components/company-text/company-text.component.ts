import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JobseekerGetJobAdvertisementDto } from '../../model/jobseeker-get-job-advertisement-dto';

@Component({
  selector: 'app-company-text',
  templateUrl: './company-text.component.html',
  styleUrls: ['./company-text.component.scss']
})
export class CompanyTextComponent implements OnInit {

  @Input() jobAdvertisementDataForJobseeker!: JobseekerGetJobAdvertisementDto | null;

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  navigateToCompanyProfile(){
    if(this.jobAdvertisementDataForJobseeker)
    this.router.navigate(['profile','company',this.jobAdvertisementDataForJobseeker.companyId,'jobseeker']);
  }

}
