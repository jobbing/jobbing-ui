import { Component, Input, OnInit } from '@angular/core';
import { CompanyGetJobAdvertisementDto } from '../../model/company-get-job-advertisement-dto';

@Component({
  selector: 'app-job-advertisement-text',
  templateUrl: './job-advertisement-text.component.html',
  styleUrls: ['./job-advertisement-text.component.scss']
})
export class JobAdvertisementTextComponent implements OnInit {

  @Input() jobAdvertisementDataForCompany!: any | null;
  textValue = "";

  constructor() { 
  }

  ngOnInit(): void {
    if(this.jobAdvertisementDataForCompany){  
      this.textValue = this.jobAdvertisementDataForCompany!.textEditor;
    }
  }

}
