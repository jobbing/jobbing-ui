import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementTextComponent } from './job-advertisement-text.component';

describe('JobAdvertisementTextComponent', () => {
  let component: JobAdvertisementTextComponent;
  let fixture: ComponentFixture<JobAdvertisementTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementTextComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
