import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementTopComponent } from './job-advertisement-top.component';

describe('JobAdvertisementTopComponent', () => {
  let component: JobAdvertisementTopComponent;
  let fixture: ComponentFixture<JobAdvertisementTopComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementTopComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
