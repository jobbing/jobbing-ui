import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HolderType } from 'src/app/core/model/holder-type';
import { WorkingType } from 'src/app/shared/enums/work-type.enum';
import { CompanyGetJobAdvertisementDto } from '../../model/company-get-job-advertisement-dto';
import { JobseekerGetJobAdvertisementDto } from '../../model/jobseeker-get-job-advertisement-dto';

@Component({
  selector: 'app-job-advertisement-top',
  templateUrl: './job-advertisement-top.component.html',
  styleUrls: ['./job-advertisement-top.component.scss']
})
export class JobAdvertisementTopComponent implements OnInit {

  @Input() jobAdvertisementDataForCompany!: CompanyGetJobAdvertisementDto | null;
  @Input() jobAdvertisementDataForJobseeker!: JobseekerGetJobAdvertisementDto | null;
  @Input() holderType!: HolderType;

  name = "";
  place!: string;
  workCategoryNameList!: string[];
  workTypeNameList!: WorkingType[];
  cityNameList!: string[];
  

  constructor(
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.fillValues();
  }

  fillValues(){
    this.getHolderType();
    if(this.holderType === HolderType.COMPANY){
      this.name = this.jobAdvertisementDataForCompany!.name;
      this.place = this.jobAdvertisementDataForCompany!.place;
      this.workCategoryNameList = this.jobAdvertisementDataForCompany!.workCategoryNameList;
      this.workTypeNameList = this.jobAdvertisementDataForCompany!.workTypeNameList;
      this.cityNameList = this.jobAdvertisementDataForCompany!.cityNameList;
    }
    else if(this.holderType === HolderType.JOBSEEKER){
      this.name = this.jobAdvertisementDataForJobseeker!.name;
      this.place = this.jobAdvertisementDataForJobseeker!.place;
      this.workCategoryNameList = this.jobAdvertisementDataForJobseeker!.workCategoryNameList;
      this.workTypeNameList = this.jobAdvertisementDataForJobseeker!.workTypeNameList;
      this.cityNameList = this.jobAdvertisementDataForJobseeker!.cityNameList;
    }
  }

  private getHolderType(){
    if(this.router.url.endsWith("jobseeker")){
      this.holderType = HolderType.JOBSEEKER
    }
    else if(this.router.url.endsWith("company")){
      this.holderType = HolderType.COMPANY
    }
  }

}
