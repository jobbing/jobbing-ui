import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { JobAdvertisementModifyComponent } from "./job-advertisement/containers/job-advertisement-modify/job-advertisement-modify.component";
import { JobAdvertisementComponent } from "./job-advertisement/containers/job-advertisement/job-advertisement.component";
import { JobseekerLoadJobAdvertisementComponent } from "./job-advertisement/containers/jobseeker-load-job-advertisement/jobseeker-load-job-advertisement.component";


const routes: Routes = [
    { path: 'job-advertisement/:id/company', component: JobAdvertisementComponent},
    { path: 'job-advertisement/modify/:id/company', component: JobAdvertisementModifyComponent},
    { path: 'job-advertisement/:id/jobseeker', component: JobseekerLoadJobAdvertisementComponent}
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]

})
export class SharedRoutingModule {}