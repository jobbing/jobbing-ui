export enum QualityType{

    EIGHT_CLASS = "8 osztály",
	GRADUATION = "érettségi",
	PROFESSION = "szakma",
	TECHNICUM = "teknikum",
	CERTIFICATE = "diploma"

}