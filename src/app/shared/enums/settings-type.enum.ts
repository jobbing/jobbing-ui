export enum SettingsType{
    
    name = 'Név',
    username = 'Felhasználónév',
    password = 'Jelszó',
    email = 'E-mail-cím'
}

