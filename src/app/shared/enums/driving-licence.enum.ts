export enum DrivingLicence {
    A = 'A',
    A1 = 'A1',
    A2 = 'A2',
    AM = 'AM',
    B = 'B',
    C = 'C',
    D = 'D',
    E = 'E',
    K = 'K', 
    T = 'T', 
    TR = 'TR', 
    V = 'V'
}

export const DRIVINGLICENCE_TYPES = [
    DrivingLicence.A,   
    DrivingLicence.A1,
    DrivingLicence.A2,
    DrivingLicence.AM,
    DrivingLicence.B,    
    DrivingLicence.C,    
    DrivingLicence.D,    
    DrivingLicence.E,    
    DrivingLicence.K,
    DrivingLicence.T,
    DrivingLicence.TR, 
    DrivingLicence.V
]