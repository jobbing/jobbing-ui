export enum LanguageType{

    MANDARIN = 'Mandarin', //kínia
	ENGLISH = 'Angol',
	SPANISH = 'Spanyol',
	ARABIC = 'Arab',
	BENGALI = 'Bengáli',
	RUSSIAN = 'Orosz',
	PORTUGUESE = 'Portugál',
	JAPANESE = 'Japán',
	GERMAN = 'Német',
	WU = 'Wu - Kínai', //kínai
	JAVANESE = 'Jávai',
	KOREAN = 'Kóreai',
	FRENCH = 'francia',
	TURKISH = 'Török',
	DUTCH = 'Holland', //HOLLANDIA
	ROMANCE = 'Rétoromán',
	SLAVIC = 'Szláv',
	GREEK = 'Görög',
	CROATIAN = 'Horvát',
	CZECH = 'Cseh',
	FINNISH = 'Finn',
	HUNGARIAN = 'Magyar',
	IRISH = 'Ír',
	ITALIAN = 'Olasz',
	LATIN = 'LAtin',
	KAZAKH = 'Kazak',
	MACEDONIAN = 'Macedón',
	LUXEMBURGISH = 'Luxemburgi',
	SERBIAN = 'Szerb',
	SWEDISH = 'Svéd',
	UKRAINIAN = 'Urkán',
	SLOVAK = 'Szlovák',
	SLOVENE = 'Szlovén',
	ROMANIAN = 'Román',
	POLISH = 'Lengyel',
	ALBANIAN = 'Albán',
	BULGARIAN = 'Bolgár'

}