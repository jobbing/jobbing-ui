export enum WorkCategory{
    ADMINISTRATION = "Adminisztráció",
    BANK = "Bank",
    MANAGEMENT = "Menedzsment",
    HEALTH = "Egészségügy",
    MARKETING = "Kereskedelem", 
    IT = "Informatika",
    ENGINEER = "Mérnök",
    SPORT = "Sport",
    COUSTOMER_SERVICE = "Ügyfélszolgálat",
    LAW = "jog",
    PHISICAL = "fizikai",
    SCIENCE = "tudomány",
    PRODUCTION = "termelés",
    TRANSPORT = "szállítás"
}

export const WORK_CATEGORIES = [
    WorkCategory.ADMINISTRATION,
    WorkCategory.BANK,
    WorkCategory.MANAGEMENT,
    WorkCategory.HEALTH,
    WorkCategory.MARKETING, 
    WorkCategory.IT,
    WorkCategory.ENGINEER,
    WorkCategory.SPORT,
    WorkCategory.COUSTOMER_SERVICE,
    WorkCategory.LAW,
    WorkCategory.PHISICAL,
    WorkCategory.SCIENCE,
    WorkCategory.PRODUCTION,
    WorkCategory.TRANSPORT
]