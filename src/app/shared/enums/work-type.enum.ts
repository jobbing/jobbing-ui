export enum WorkingType{

    PART_TIME_JOB = "Részmunkaidős",
	STUDENT_JOB = "Diákmunka",
	HOME_JOB = "Otthoni munkavégzés",
	FULL_TIME_JOB = "Teljes munkaidős",
	PRACTICAL_JOB = "Szakmai gyakorlat",  
	ENTRIVERAL_JOB = "Pályakezdő"
}

export const WORKING_TYPES = [
	WorkingType.PART_TIME_JOB,
	WorkingType.STUDENT_JOB,
	WorkingType.HOME_JOB,
	WorkingType.FULL_TIME_JOB,
	WorkingType.PRACTICAL_JOB,
	WorkingType.ENTRIVERAL_JOB,
]