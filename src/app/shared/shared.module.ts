import { NgModule } from "@angular/core";
import { MaterialModule } from "../material.module";
import { PublicHeaderComponent } from "./public-header/public-header.component";
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { CompanyTextComponent } from './job-advertisement/components/company-text/company-text.component';
import { JobAdvertisementTopComponent } from './job-advertisement/components/job-advertisement-top/job-advertisement-top.component';
import { JobAdvertisementTextComponent } from './job-advertisement/components/job-advertisement-text/job-advertisement-text.component';
import { JobAdvertisementComponent } from './job-advertisement/containers/job-advertisement/job-advertisement.component';
import { SharedRoutingModule } from "./shared-routing.module";
import { StoreModule } from "@ngrx/store";
import { fromJobAdvertisementStore } from "./job-advertisement/store";
import { EffectsModule } from "@ngrx/effects";
import { JobAdvertisementEffects } from "./job-advertisement/store/job-advertisement.effects";
import { JobAdvertisementService } from "./job-advertisement/services/job-advertisement.service";
import { JobAdvertisementEditComponent } from './job-advertisement/components/job-advertisement-edit/job-advertisement-edit.component';
import { JobAdvertisementModifyComponent } from './job-advertisement/containers/job-advertisement-modify/job-advertisement-modify.component';
import { JobAdvertisementSaveComponent } from './job-advertisement/components/job-advertisement-save/job-advertisement-save.component';
import { QuillModule } from "ngx-quill";
import { JobseekerLoadJobAdvertisementComponent } from './job-advertisement/containers/jobseeker-load-job-advertisement/jobseeker-load-job-advertisement.component';
@NgModule({
    imports: [
        MaterialModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        SharedRoutingModule,
        StoreModule.forFeature(
            fromJobAdvertisementStore.JobAdvertisementFeatureKey,
            fromJobAdvertisementStore.JobAdvertisementReducer
        ),
        EffectsModule.forFeature([JobAdvertisementEffects]),
        QuillModule
    ],
    declarations: [
        PublicHeaderComponent,
        HeaderComponent,
        CompanyTextComponent,
        JobAdvertisementTopComponent,
        JobAdvertisementTextComponent,
        JobAdvertisementComponent,
        JobAdvertisementEditComponent,
        JobAdvertisementModifyComponent,
        JobAdvertisementSaveComponent,
        JobseekerLoadJobAdvertisementComponent,
    ],
    exports: [
        PublicHeaderComponent,
        MaterialModule,
        HeaderComponent, 
    ],
    providers: [
        JobAdvertisementService
    ]
    
})
export class SharedModule {}