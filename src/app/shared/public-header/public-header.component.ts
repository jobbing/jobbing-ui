import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-public-header',
  templateUrl: './public-header.component.html',
  styleUrls: ['./public-header.component.scss']
})
export class PublicHeaderComponent implements OnInit {

  pageTitle = "";
  leftButtonText = "";
  middleButtonText = "";
  rightButtonText = "";
  leftButtonNavigate = "";
  middleButtonNavigate = "";
  rightButtonNavigate = "";

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.checkCurrentPage();
  }

  public navigateLeftButton() {
    this.router.navigate([this.leftButtonNavigate]);
  }

  public navigateMiddleButton() {
    this.router.navigate([this.middleButtonNavigate]);
  }

  public navigateRightButton() {
    this.router.navigate([this.rightButtonNavigate]);
  }

  private checkCurrentPage() {
    if (this.getUrl().includes("/login/jobseeker")) {
      this.setUpJobseekerLogin();
    }
    else if (this.getUrl().includes("/login/company")) {
      this.setUpCompanyLogin();
    }
    else if (this.getUrl().includes("/register/jobseeker")) {
      this.setUpJobseekerRegister();
    }
    else if (this.getUrl().includes("/register/company")) {
      this.setUpCompanyRegister();
    }
  }

  private getUrl() {
    return this.router.url;
  }

  private setUpJobseekerLogin() {
    this.leftButtonText = "Regisztráció <strong>munkahelyek</strong> számára";
    this.leftButtonNavigate = "/register/company";
    this.middleButtonText = "Regisztráció";
    this.middleButtonNavigate = "/register/jobseeker";
    this.rightButtonText = "Bejelentkezés <strong>munkahelyek</strong> számára";
    this.rightButtonNavigate = "/login/company";
    this.pageTitle = "Álláskeresői bejelentkező felület";
  }

  private setUpCompanyLogin() {
    this.leftButtonText = "Regisztráció <strong>álláskeresők</strong> számára";
    this.leftButtonNavigate = "/register/jobseeker";
    this.middleButtonText = "Regisztráció";
    this.middleButtonNavigate = "/register/company";
    this.rightButtonText = "Bejelentkezés <strong>álláskeresők</strong> számára";
    this.rightButtonNavigate = "/login/jobseeker";
    this.pageTitle = "Munkahelyi bejelentkező felület";
  }

  private setUpJobseekerRegister() {
    this.leftButtonText = "Regisztráció <strong>munkahelyek</strong> számára";
    this.leftButtonNavigate = "/register/company";
    this.middleButtonText = "Bejelentkezés";
    this.middleButtonNavigate = "/login/jobseeker";
    this.rightButtonText = "Bejelentkezés <strong>munkahelyek</strong> számára";
    this.rightButtonNavigate = "/login/company";
    this.pageTitle = "Álláskeresői regisztrációs felület";
  }

  private setUpCompanyRegister() {
    this.leftButtonText = "Regisztráció <strong>álláskeresők</strong> számára";
    this.leftButtonNavigate = "/register/jobseeker";
    this.middleButtonText = "Bejelentkezés";
    this.middleButtonNavigate = "/login/company";
    this.rightButtonText = "Bejelentkezés <strong>álláskeresők</strong> számára";
    this.rightButtonNavigate = "/login/jobseeker";
    this.pageTitle = "Munkahelyi regisztrációs felület";
  }

}
