import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthActions, AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { HolderType } from 'src/app/core/model/holder-type';
import { JobseekerProfileData } from 'src/app/core/model/jobseeker-profile-data';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  myData$: Observable<GetMyDatasDto | null>;
  myName = "";
  isJobseeker = false;
  isCompany = true;

  constructor(
    private router: Router,
    private store: Store<fromAuthStore.AuthState>
  ) { 
    this.myData$ = this.store.pipe(select(AuthSelectors.selectMyDatas));
    this.setMyName();
  }

  ngOnInit(): void {
    this.setHeaderTitles();
  }

  navigateToManageJobseekers(){
    this.router.navigate(["manage-jobseekers","find-jobseekers","company"]);
  }

  navigateToManageJobAdvertisements(){
    this.router.navigate(["manage-job-advertisements","my-job-advertisements","company"]);
  }
  
  navigateToCompanyProfile(){
    this.router.navigate(["profile","company"]);

  }
  
  NavigateToApplications(){
    this.router.navigate(["jobseeker-applications","job-advertisement-applications","jobseeker"]);
  }

  NavigateToFindJobAdvertisements(){
    this.router.navigate(["jobseeker-find","jobseeker"]);
  }

  navigateToJobseekerProfile(){
    this.router.navigate(["profile","jobseeker"]);

  }

  public singOutNavigate(){
    if(this.checkSide() == HolderType.JOBSEEKER){
      this.router.navigate(["login","jobseeker"]);
    }
    else if(this.checkSide() == HolderType.COMPANY){
      this.router.navigate(["login","company"]);
    }

    this.store.dispatch(
      AuthActions.logout()
    )
  }

  public settingsNavigate(){
    if(this.checkSide() == HolderType.JOBSEEKER){
      this.router.navigate(["settings","jobseeker"]);
    }
    else if(this.checkSide() == HolderType.COMPANY){
      this.router.navigate(["settings","company"]);
    }
  }

  private setHeaderTitles(){
    if(this.checkSide() == HolderType.JOBSEEKER){
      this.isCompany = false;
      this.isJobseeker = true;
    } else if(this.checkSide() == HolderType.COMPANY){
      this.isJobseeker = false;
      this.isCompany = true;
    }
  }

  private checkSide(){
    if (this.getUrl().endsWith("jobseeker")) {
      return HolderType.JOBSEEKER;
    }
    else{
      return HolderType.COMPANY;
    }
  }

  private getUrl() {
    return this.router.url;
  }

  private setMyName(){
    this.myData$.subscribe(event => this.myName = event?.name!)
  }

}
