import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { HeaderComponent } from "../shared/header/header.component";
import { SharedModule } from "../shared/shared.module";
import { ProfileRoutingModule } from "./profile-routing.module";
import { JobseekerProfileComponent } from './containers/jobseeker-profile/jobseeker-profile.component';
import { ProfileIdeablockComponent } from './components/profile-ideablock/profile-ideablock.component';
import { fromProfileStore } from "./store";
import { ProfileEffects } from "./store/profile.effects";
import { EffectsModule } from "@ngrx/effects";
import { StoreModule } from "@ngrx/store";
import { ProfileService } from "./services/profile.service";
import { CompanyProfileComponent } from './containers/company-profile/company-profile.component';
import { JobAdvertisementListComponent } from './components/job-advertisement-list/job-advertisement-list.component';
import { DrivingLicenceBlockComponent } from './containers/driving-licence-block/driving-licence-block.component';
import { DrivingLicencePopupComponent } from './components/driving-licence-popup/driving-licence-popup.component';
import { DrivingLicenceOneComponent } from './components/driving-licence-one/driving-licence-one.component';
import { DrivingLicenceModifyComponent } from './components/driving-licence-modify/driving-licence-modify.component';
import { LanguageExamBlockComponent } from './containers/language-exam-block/language-exam-block.component';
import { LanguageExamOneComponent } from './components/language-exam-one/language-exam-one.component';
import { LanguageExamModifyComponent } from './components/language-exam-modify/language-exam-modify.component';
import { LanguageExamPopupComponent } from './components/language-exam-popup/language-exam-popup.component';
import { QualityBlockComponent } from './containers/quality-block/quality-block.component';
import { QualityListComponent } from './components/quality-list/quality-list.component';
import { QualityOneComponent } from './components/quality-one/quality-one.component';
import { QualityPopupComponent } from './components/quality-popup/quality-popup.component';
import { ExperienceBlockComponent } from './containers/experience-block/experience-block.component';
import { ExperienceListComponent } from './components/experience-list/experience-list.component';
import { ExperienceOneComponent } from './components/experience-one/experience-one.component';
import { ExperiencePopupComponent } from './components/experience-popup/experience-popup.component';
import { TopBlockComponent } from './containers/top-block/top-block.component';
import { CompanyTopBlockComponent } from './containers/company-top-block/company-top-block.component';
import { CompanyAccountComponent } from './components/company-account/company-account.component';
import { CompanyBasicDatasComponent } from './components/company-basic-datas/company-basic-datas.component';
import { CompanyIntroductionComponent } from './components/company-introduction/company-introduction.component';
import { AccountComponent } from "./components/account/account.component";
import { BasicDatasComponent } from "./components/basic-datas/basic-datas.component";
import { IntroductionComponent } from "./components/introduction/introduction.component";
import { JobAdvertisementBlockComponent } from './containers/job-advertisement-block/job-advertisement-block.component';
import { JobAdvertisementOneComponent } from './components/job-advertisement-one/job-advertisement-one.component';
import { CompanyOpenJobseekerProfileComponent } from './containers/company-open-jobseeker-profile/company-open-jobseeker-profile.component';
import { JobseekerProfileSummaryComponent } from './components/jobseeker-profile-summary/jobseeker-profile-summary.component';
import { JobseekerOpenCompanyProfileComponent } from './containers/jobseeker-open-company-profile/jobseeker-open-company-profile.component';
import { CompanyProfileSummaryComponent } from './components/company-profile-summary/company-profile-summary.component';

@NgModule({
    imports: [
        ReactiveFormsModule,
        ProfileRoutingModule,
        CommonModule,
        SharedModule,
        StoreModule.forFeature(
            fromProfileStore.ProfileFeatrureKey,
            fromProfileStore.ProfileReducer
        ),
        EffectsModule.forFeature([ProfileEffects]),
    ],
    declarations: [
        JobseekerProfileComponent,
        ProfileIdeablockComponent,
        CompanyProfileComponent,
        JobAdvertisementListComponent,
        DrivingLicenceBlockComponent,
        DrivingLicencePopupComponent,
        DrivingLicenceOneComponent,
        DrivingLicenceModifyComponent,
        LanguageExamBlockComponent,
        LanguageExamOneComponent,
        LanguageExamModifyComponent,
        LanguageExamPopupComponent,
        QualityBlockComponent,
        QualityListComponent,
        QualityOneComponent,
        QualityPopupComponent,
        ExperienceBlockComponent,
        ExperienceListComponent,
        ExperienceOneComponent,
        ExperiencePopupComponent,
        TopBlockComponent,
        CompanyTopBlockComponent,
        CompanyAccountComponent,
        CompanyBasicDatasComponent,
        CompanyIntroductionComponent,
        AccountComponent,
        BasicDatasComponent,
        IntroductionComponent,
        JobAdvertisementBlockComponent,
        JobAdvertisementOneComponent,
        CompanyOpenJobseekerProfileComponent,
        JobseekerProfileSummaryComponent,
        JobseekerOpenCompanyProfileComponent,
        CompanyProfileSummaryComponent
    ],
    providers:[
        HeaderComponent,
        ProfileService,
    ]

})
export class ProfileModule { }