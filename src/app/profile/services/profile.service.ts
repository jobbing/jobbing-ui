import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { CompanyProfileData } from "src/app/core/model/company-profile-data";
import { DeleteLanguageExamDto } from "src/app/core/model/delete-language-exam-dto";
import { GetDrivingLicencesDto } from "src/app/core/model/get-driving-licences-dto";
import { GetLanguageExamsDto } from "src/app/core/model/get-language-exams-dto";
import { JobseekerProfileData } from "src/app/core/model/jobseeker-profile-data";
import { SaveDrivingLicenceDto } from "src/app/core/model/save-driving-licence-dto";
import { SaveLanguageExamDto } from "src/app/core/model/save-language-exam-dto";
import { UpdateCompanyDataDto } from "src/app/core/model/update-company-data-dto";
import { UpdateIntroductionDto } from "src/app/core/model/update-introduction-dto";
import { UpdatePersonalDataDto } from "src/app/core/model/update-personal-data-dto";
import { UpdateWorkILookForDto } from "src/app/core/model/update-work-i-look-for-dto";
import { GetExperiencesDto } from "../model/get-experiences-dto";
import { GetJobAdvertisementSummaryDto } from "../model/get-job-advertisement-summary-dto";
import { GetQualitiesDto } from "../model/get-qualities-dto";
import { SaveExperienceDto } from "../model/save-experience-dto";
import { SaveQualityDto } from "../model/save-quality-dto";
import { UpdateCompanyIntroductionDto } from "../model/update-company-introduction-dto";

@Injectable()
export class ProfileService {

    private readonly URL = '/jobseeker-profile';
    private readonly C_URL = '/company-profile';

    constructor(
        private http: HttpClient
    ) { }

    loadProfileData(): Observable<JobseekerProfileData> {
        let reqUrl = `${this.URL}`;
        return this.http.get<JobseekerProfileData>(reqUrl);
    }

    companyloadJobseekerProfileData(jobseekerId: number): Observable<JobseekerProfileData> {
        let reqUrl = `${this.URL}/company-load?jobseekerId=`+jobseekerId;
        return this.http.get<JobseekerProfileData>(reqUrl);
    }
    
    updateIntroduction(updateIntroductionDto: UpdateIntroductionDto){
        let reqUrl = `${this.URL}/update-introduction`;
        return this.http.post<void>(reqUrl, updateIntroductionDto);
    }

    updateCompanyIntroduction(updateCompanyIntroductionDto: UpdateCompanyIntroductionDto){
        let reqUrl = `${this.C_URL}/update-introduction`;
        return this.http.post<void>(reqUrl, updateCompanyIntroductionDto);
    }

    updateWorkILookFor(updateWorkILookForDto: UpdateWorkILookForDto){
        let reqUrl = `${this.URL}/update-work-i-look-for`;
        return this.http.post<void>(reqUrl, updateWorkILookForDto);

    }

    updatePersonalData(updatePersonalDataDto: UpdatePersonalDataDto){
        let reqUrl = `${this.URL}/update-personal-data`;
        return this.http.post<void>(reqUrl, updatePersonalDataDto);
    }

    updateProfilePicture(profilePicture: File){
        let reqUrl = `${this.URL}/save-picture`;
        return this.http.post<void>(reqUrl, profilePicture);
    }

    getDrivingLicences(jobseekerId: number){
        let reqUrl = `${this.URL}/get-driving-licences?id=`+jobseekerId;
        return this.http.get<GetDrivingLicencesDto[]>(reqUrl);
    }

    getQualities(jobseekerId: number){
        let reqUrl = `${this.URL}/get-qualities?id=`+jobseekerId;
        return this.http.get<GetQualitiesDto[]>(reqUrl);
    }

    deleteQuality(qualityId: number){
        let reqUrl = `${this.URL}/delete-quality?id=`+qualityId;
        return this.http.delete<number>(reqUrl);
    }

    saveQuality(saveQualityDto: SaveQualityDto){
        let reqUrl = `${this.URL}/save-quality`;
        return this.http.post<number>(reqUrl, saveQualityDto);
    }


    getExperiences(jobseekerId: number){
        let reqUrl = `${this.URL}/get-experiences?id=`+jobseekerId;
        return this.http.get<GetExperiencesDto[]>(reqUrl);
    }

    deleteExperience(experienceId: number){
        let reqUrl = `${this.URL}/delete-experience?id=`+experienceId;
        return this.http.delete<number>(reqUrl);
    }

    saveExperience(saveExperienceDto: SaveExperienceDto){
        let reqUrl = `${this.URL}/save-experience`;
        return this.http.post<number>(reqUrl, saveExperienceDto);
    }


    getLanguageExams(jobseekerId: number){
        let reqUrl = `${this.URL}/get-language-exams?id=`+jobseekerId;
        return this.http.get<GetLanguageExamsDto[]>(reqUrl);
    }
    
    deleteLanguageExam(deleteLanguageExamDto: DeleteLanguageExamDto){
        let reqUrl = `${this.URL}/delete-language-exam?jobseekerId=`+ deleteLanguageExamDto.jobseekerId 
        + "&languageExamId=" + deleteLanguageExamDto.languageExamId;
        return this.http.delete<number>(reqUrl);
    }

    saveDrivingLicence(saveDrivingLicenceDto: SaveDrivingLicenceDto){
        return this.http.post<number>(`${this.URL}/save-driving-licence`, saveDrivingLicenceDto);
    }

    saveLanguageExam(saveLanguageExamDto: SaveLanguageExamDto){
        return this.http.post<number>(`${this.URL}/save-language-exam`, saveLanguageExamDto);
    }

    loadCompanyProfileData(companyId: number): Observable<CompanyProfileData> {
        let reqUrl = `${this.C_URL}?companyId=`+companyId;
        return this.http.get<CompanyProfileData>(reqUrl);
    }

    updateCompanyData(updateCompanyDataDto: UpdateCompanyDataDto){
        let reqUrl = `${this.C_URL}/update-personal-data`;
        return this.http.post<number>(reqUrl, updateCompanyDataDto);
    }

    getJobAdvertisementSummaries(companyId: number){
        let reqUrl = `${this.C_URL}/load-jobAdvertisement-summaries?companyId=`+companyId;
        return this.http.get<GetJobAdvertisementSummaryDto[]>(reqUrl);
    }


} 

