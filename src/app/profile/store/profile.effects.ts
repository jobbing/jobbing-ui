import { Injectable } from "@angular/core";
import { Actions, concatLatestFrom, createEffect, ofType } from "@ngrx/effects";
import { map, switchMap } from "rxjs";
import { NotificationService } from "src/app/core/notification/notification.service";
import { DrivingLicence } from "src/app/shared/enums/driving-licence.enum";
import { ProfileActions } from ".";
import { ProfileService } from "../services/profile.service";

@Injectable()
export class ProfileEffects {
    
    loadProfileData$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.loadProfileData),
            switchMap(action =>
                this.profileService.loadProfileData()
                    .pipe(
                        map(jobseekerProfileData => ProfileActions.loadProfileDataSuccess({ jobseekerProfileData }))
                    )
            )
        ));

    companyloadJobseekerProfileData$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.companyLoadJobseekerProfileData),
            switchMap(action =>
                this.profileService.companyloadJobseekerProfileData(action.jobseekerId)
                    .pipe(
                        map(jobseekerProfileData => ProfileActions.loadProfileDataSuccess({ jobseekerProfileData }))
                    )
            )
        ));

    updateIntroduction$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.updateIntroduction),
            switchMap(action =>
                this.profileService.updateIntroduction(action.updateIntroductionDto)
                    .pipe(
                        map(() => ProfileActions.updateIntroductionSuccess())
                    )
            )
        )) 

    updateCompanyIntroduction$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.updateCompanyIntroduction),
            switchMap(action =>
                this.profileService.updateCompanyIntroduction(action.updateCompanyIntroductionDto)
                    .pipe(
                        map(() => ProfileActions.updateCompanyIntroductionSuccess())
                    )
            )
        )) 

    updateWorkILookFor$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.updateWorkILookFor),
            switchMap(action =>
                this.profileService.updateWorkILookFor(action.updateWorkILookForDto)
                    .pipe(
                        map(() => ProfileActions.updateWorkILookForSuccess())
                    )
            )
        ));

    updatePersonalData$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.updatePersonalData),
            switchMap(action =>
                this.profileService.updatePersonalData(action.updatePersonalDataDto)
                    .pipe(
                        map(() => ProfileActions.updatePersonalDataSuccess())
                    )
            )
        ));

    updatePersonalDataSuccess$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.updatePersonalDataSuccess),
            switchMap(action =>
                this.profileService.loadProfileData()
                    .pipe(
                        map(jobseekerProfileData => ProfileActions.loadProfileDataSuccess({ jobseekerProfileData }))
                    )
            )
        ));
    
    updateProfilePicture$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.updateProfilePicture),
            switchMap(action =>
                this.profileService.updateProfilePicture(action.profilePicture)
                    .pipe(
                        map(() => ProfileActions.updateProfilePictureSuccess())
                    )
            )
        ));

    getDrivingLicences$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.getDrivingLicences),
            switchMap(action =>
                this.profileService.getDrivingLicences(action.jobseekerId)
                    .pipe(
                        map(getdrivingLicencesDto => ProfileActions.getDrivingLicencesSuccess({ getdrivingLicencesDto }))
                    )
            )
        ));

    getQualities$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.getQualities),
            switchMap(action =>
                this.profileService.getQualities(action.jobseekerId)
                    .pipe(
                        map(getQualitiesDto => ProfileActions.getQualitiesSuccess({ getQualitiesDto }))
                    )
            )
        ));

    deleteQuality$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.deleteQuality),
            switchMap(action =>
                this.profileService.deleteQuality(action.qualityId)
                    .pipe(
                        map(jobseekerId => ProfileActions.getQualities({ jobseekerId }))
                    )
            )
        ));

    saveQuality$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.saveQuality),
            switchMap(action =>
                this.profileService.saveQuality(action.saveQualityDto)
                    .pipe(
                        map(jobseekerId => ProfileActions.getQualities({ jobseekerId }))
                    )
            )
        ));
    

    getExperiences$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.getExperiences),
            switchMap(action =>
                this.profileService.getExperiences(action.jobseekerId)
                    .pipe(
                        map(getExperiencesDto => ProfileActions.getExperiencesSuccess({ getExperiencesDto }))
                    )
            )
        ));

    deleteExperience$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.deleteExperience),
            switchMap(action =>
                this.profileService.deleteExperience(action.experinceId)
                    .pipe(
                        map(jobseekerId => ProfileActions.getExperiences({ jobseekerId }))
                    )
            )
        ));

    saveExperience$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.saveExperience),
            switchMap(action =>
                this.profileService.saveExperience(action.saveExperinceDto)
                    .pipe(
                        map(jobseekerId => ProfileActions.getExperiences({ jobseekerId }))
                    )
            )
        ));
    

    saveDrivingLicences$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.saveDrivingLicences),
            switchMap(action =>
                this.profileService.saveDrivingLicence(action.saveDrivivingLicenceDto)
                    .pipe(
                        map(jobseekerId => ProfileActions.getDrivingLicences({jobseekerId}))
                    )
            )
        ));

    getLanguageExams$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.getLanguageExams),
            switchMap(action =>
                this.profileService.getLanguageExams(action.jobseekerId)
                    .pipe(
                        map(getLanguageExamsDto => ProfileActions.getLanguageExamsSuccess({ getLanguageExamsDto }))
                    )
            )
        ));

    deleteLanguageExam$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.deleteLanguageExam),
            switchMap(action =>
                this.profileService.deleteLanguageExam(action.deleteLanguageExamDto)
                    .pipe(
                        map(jobseekerId => ProfileActions.getLanguageExams({ jobseekerId }))
                    )
            )
        ));

    saveLanguageExams$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.saveLanguageExam),
            switchMap(action =>
                this.profileService.saveLanguageExam(action.saveLanguageExamDto)
                    .pipe(
                        map(jobseekerId => ProfileActions.getLanguageExams({jobseekerId}))
                    )
            )
        ));

    loadCompanyProfileData$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.loadCompanyProfileData),
            switchMap(action =>
                this.profileService.loadCompanyProfileData(action.companyId)
                    .pipe(
                        map(companyProfileData => ProfileActions.loadCompanyProfileDataSuccess({ companyProfileData }))
                    )
            )
        ));


    updateCompanyProfileData$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.updateCompanyData),
            switchMap(action =>
                this.profileService.updateCompanyData(action.updateCompanyDataDto)
                    .pipe(
                        map(companyId => ProfileActions.loadCompanyProfileData({companyId}))
                    )
            )
        ));

        getJobAdvertisementSummaries$ = createEffect(() => this.actions$
        .pipe(
            ofType(ProfileActions.getJobAdvertisementSummaries),
            switchMap(action =>
                this.profileService.getJobAdvertisementSummaries(action.companyId)
                    .pipe(
                        map(getJobAdvertisementSummaryDto => ProfileActions.getJobAdvertisementSummariesSuccess({getJobAdvertisementSummaryDto}))
                    )
            )
        ));
        
    constructor(
        private notificationService: NotificationService,
        private actions$: Actions,
        private profileService: ProfileService
    ) { }
}