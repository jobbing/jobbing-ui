import { createAction, props } from "@ngrx/store";
import { CompanyProfileData } from "src/app/core/model/company-profile-data";
import { DeleteLanguageExamDto } from "src/app/core/model/delete-language-exam-dto";
import { GetDrivingLicencesDto } from "src/app/core/model/get-driving-licences-dto";
import { GetLanguageExamsDto } from "src/app/core/model/get-language-exams-dto";
import { JobseekerProfileData } from "src/app/core/model/jobseeker-profile-data";
import { SaveDrivingLicenceDto } from "src/app/core/model/save-driving-licence-dto";
import { SaveLanguageExamDto } from "src/app/core/model/save-language-exam-dto";
import { UpdateCompanyDataDto } from "src/app/core/model/update-company-data-dto";
import { UpdateIntroductionDto } from "src/app/core/model/update-introduction-dto";
import { UpdatePersonalDataDto } from "src/app/core/model/update-personal-data-dto";
import { UpdateWorkILookForDto } from "src/app/core/model/update-work-i-look-for-dto";
import { FailData } from "../../core/model/fail-message.model";
import { GetExperiencesDto } from "../model/get-experiences-dto";
import { GetJobAdvertisementSummaryDto } from "../model/get-job-advertisement-summary-dto";
import { GetQualitiesDto } from "../model/get-qualities-dto";
import { SaveExperienceDto } from "../model/save-experience-dto";
import { SaveQualityDto } from "../model/save-quality-dto";
import { UpdateCompanyIntroductionDto } from "../model/update-company-introduction-dto";

const prefix = '[profile]';

export const loadProfileData = createAction(
    `${prefix} load profile data`
);

export const loadProfileDataSuccess = createAction(
    `${prefix} load profile data success`,
    props<{ jobseekerProfileData: JobseekerProfileData }>()
);

export const loadProfileDataFail = createAction(
    `${prefix} load profile data fail`,
    props<{ failData: FailData }>()
);

export const companyLoadJobseekerProfileData = createAction(
    `${prefix} company load jobseeker profile data`,
    props<{jobseekerId: number}>()
);


export const companyLoadJobseekerProfileDataFail = createAction(
    `${prefix} company load jobseeker profile data fail`,
    props<{ failData: FailData }>()
);

export const updateIntroduction = createAction(
    `${prefix} update introduction`,
    props<{ updateIntroductionDto: UpdateIntroductionDto}>()
);

export const updateIntroductionSuccess = createAction(
    `${prefix} update introduction success`
);

export const updateIntroductionFail = createAction(
    `${prefix} update introduction fail`,
    props<{ failData: FailData }>()
);

export const updateCompanyIntroduction = createAction(
    `${prefix} update company introduction`,
    props<{ updateCompanyIntroductionDto: UpdateCompanyIntroductionDto}>()
);

export const updateCompanyIntroductionSuccess = createAction(
    `${prefix} update company introduction success`
);

export const updateCompanyIntroductionFail = createAction(
    `${prefix} update company introduction fail`,
    props<{ failData: FailData }>()
)

export const updateWorkILookFor = createAction(
    `${prefix} update work i look for`,
    props<{ updateWorkILookForDto: UpdateWorkILookForDto}>()
);

export const updateWorkILookForSuccess = createAction(
    `${prefix} update work i look for success`
);

export const updateWorkILookForFail = createAction(
    `${prefix} update work i look for fail`,
    props<{ failData: FailData }>()
);

export const updatePersonalData = createAction(
    `${prefix} update personal data`,
    props<{updatePersonalDataDto: UpdatePersonalDataDto}>()
);

export const updatePersonalDataSuccess = createAction(
    `${prefix} update personal data success`
);

export const updatePersonalDataFail = createAction(
    `${prefix} update personal data fail`,
    props<{ failData: FailData }>()
);

export const updateProfilePicture = createAction(
    `${prefix} update profile picture` ,
    props<{profilePicture: File}>()
);

export const updateProfilePictureSuccess = createAction(
    `${prefix} update profile picture success`,
);

export const updateProfilePictureFail = createAction(
    `${prefix} update profile picture fail`,
    props<{ failData: FailData }>()
);


export const getDrivingLicences = createAction(
    `${prefix} get driving licences`,
    props<{jobseekerId: number}>()
);

export const getDrivingLicencesSuccess = createAction(
    `${prefix} get driving licences success`,
    props<{ getdrivingLicencesDto: GetDrivingLicencesDto[]}>()
);

export const getDrivingLicencesFail = createAction(
    `${prefix} get driving licences fail`,
    props<{ failData: FailData }>()
);

export const getQualities = createAction(
    `${prefix} get qualities`,
    props<{jobseekerId: number}>()
);

export const getQualitiesSuccess = createAction(
    `${prefix} get qualities success`,
    props<{ getQualitiesDto: GetQualitiesDto[]}>()
);

export const getQualitiesFail = createAction(
    `${prefix} get qualities fail`,
    props<{ failData: FailData }>()
);

export const deleteQuality = createAction(
    `${prefix} delete quality`,
    props<{qualityId: number}>()
);

export const deleteQualitySuccess = createAction(
    `${prefix} delete quality success`,
);

export const deleteQualityFail = createAction(
    `${prefix} delete quality fail`,
    props<{ failData: FailData }>()
);

export const saveQuality = createAction(
    `${prefix} save quality`,
    props<{saveQualityDto: SaveQualityDto}>()
);

export const saveQualitySuccess = createAction(
    `${prefix} save quality success`,
);

export const saveQualityFail = createAction(
    `${prefix} save quality fail`,
    props<{ failData: FailData }>()
);


export const getExperiences = createAction(
    `${prefix} get experiences`,
    props<{jobseekerId: number}>()
);

export const getExperiencesSuccess = createAction(
    `${prefix} get experiences success`,
    props<{ getExperiencesDto: GetExperiencesDto[]}>()
);

export const getExperiencesFail = createAction(
    `${prefix} get ExperigetExperiences fail`,
    props<{ failData: FailData }>()
);

export const deleteExperience = createAction(
    `${prefix} delete experince`,
    props<{experinceId: number}>()
);

export const deleteExperienceSuccess = createAction(
    `${prefix} delete experince success`,
);

export const deleteExperienceFail = createAction(
    `${prefix} delete experince fail`,
    props<{ failData: FailData }>()
);

export const saveExperience = createAction(
    `${prefix} save experince`,
    props<{saveExperinceDto: SaveExperienceDto}>()
);

export const saveExperienceSuccess = createAction(
    `${prefix} save experince success`,
);

export const saveExperienceFail = createAction(
    `${prefix} save experince fail`,
    props<{ failData: FailData }>()
);



export const saveDrivingLicences = createAction(
    `${prefix} save driving licences`,
    props<{saveDrivivingLicenceDto: SaveDrivingLicenceDto}>()
);

export const saveDrivingLicencesSuccess = createAction(
    `${prefix} save driving licences success`,
    props<{jobseekerId: number}>()
);

export const saveDrivingLicencesFail = createAction(
    `${prefix} save driving licences fail`,
    props<{ failData: FailData }>()
);

export const getLanguageExams = createAction(
    `${prefix} get language exams`,
    props<{jobseekerId: number}>()
);

export const getLanguageExamsSuccess = createAction(
    `${prefix} get language exams success`,
    props<{ getLanguageExamsDto: GetLanguageExamsDto[]}>()
);

export const getLanguageExamsFail = createAction(
    `${prefix} get language exams fail`,
    props<{ failData: FailData }>()
);

export const deleteLanguageExam = createAction(
    `${prefix} delete language exam`,
    props<{deleteLanguageExamDto: DeleteLanguageExamDto}>()
);

export const deleteLanguageExamSuccess = createAction(
    `${prefix} delete language exam success`,
    props<{ getLanguageExamsDto: GetLanguageExamsDto[] }>()
);

export const deleteLanguageExamFail = createAction(
    `${prefix} delete language exam fail`,
    props<{ failData: FailData }>()
);


export const saveLanguageExam = createAction(
    `${prefix} save language exam`,
    props<{saveLanguageExamDto: SaveLanguageExamDto}>()
);

export const saveLanguageExamSuccess = createAction(
    `${prefix} save language exam success`
);

export const saveLanguageExamFail = createAction(
    `${prefix} save language exam fail`,
    props<{ failData: FailData }>()
);

export const loadCompanyProfileData = createAction(
    `${prefix} load company profile data`,
    props<{companyId: number}>()
);

export const loadCompanyProfileDataSuccess = createAction(
    `${prefix} load company profile data success`,
    props<{ companyProfileData: CompanyProfileData }>()
);

export const loadCompanyProfileDataFail = createAction(
    `${prefix} load company profile data fail`,
    props<{ failData: FailData }>()
);

export const updateCompanyData = createAction(
    `${prefix} update company data`,
    props<{updateCompanyDataDto: UpdateCompanyDataDto}>()
);

export const updateCompanyDataSuccess = createAction(
    `${prefix} update company data success`
);

export const updateCompanyDataFail = createAction(
    `${prefix} update company data fail`,
    props<{ failData: FailData }>()
);

export const getJobAdvertisementSummaries = createAction(
    `${prefix} get job advertisement summaries`,
    props<{companyId: number}>()
);

export const getJobAdvertisementSummariesSuccess = createAction(
    `${prefix} get job advertisement summaries success`,
    props<{ getJobAdvertisementSummaryDto: GetJobAdvertisementSummaryDto[]}>()
);

export const getJobAdvertisementSummariesFail = createAction(
    `${prefix} get job advertisement summaries fail`,
    props<{ failData: FailData }>()
);