import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ProfileFeatrureKey, ProfileState } from "./profile.reducers";



const selectProfileState = createFeatureSelector<ProfileState>(ProfileFeatrureKey);

export const selectProfileData = createSelector(selectProfileState, (state) => state.profileData);

export const selectDrivingLicences = createSelector(selectProfileState, (state) => state.drivingLicences);

export const selectLanguageExams = createSelector(selectProfileState, (state) => state.languageExams);

export const selectCompanyProfileData = createSelector(selectProfileState, (state) => state.companyProfileData);

export const selectQualities = createSelector(selectProfileState, (state) => state.qualities);

export const selectExperiences = createSelector(selectProfileState, (state) => state.experiences);

export const selectJobAdvertisementSummaries = createSelector(selectProfileState, (state) => state.jobAdvertisementSummaries);