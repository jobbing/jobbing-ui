import { createReducer, on } from "@ngrx/store";
import { CompanyProfileData } from "src/app/core/model/company-profile-data";
import { GetDrivingLicencesDto } from "src/app/core/model/get-driving-licences-dto";
import { GetLanguageExamsDto } from "src/app/core/model/get-language-exams-dto";
import { JobseekerProfileData } from "src/app/core/model/jobseeker-profile-data";
import { ProfileActions } from ".";
import { GetExperiencesDto } from "../model/get-experiences-dto";
import { GetJobAdvertisementSummaryDto } from "../model/get-job-advertisement-summary-dto";
import { GetQualitiesDto } from "../model/get-qualities-dto";

export const ProfileFeatrureKey = 'profile';

export interface ProfileState {
    profileData: JobseekerProfileData | null;
    drivingLicences: GetDrivingLicencesDto[];
    languageExams: GetLanguageExamsDto[];
    qualities: GetQualitiesDto[];
    experiences: GetExperiencesDto[];
    jobAdvertisementSummaries: GetJobAdvertisementSummaryDto[];

    companyProfileData: CompanyProfileData | null;
}

export const initialState: ProfileState = {
    profileData: null,
    drivingLicences: [],
    languageExams: [],
    qualities: [],
    experiences: [],
    jobAdvertisementSummaries: [],

    companyProfileData: null
  };



export const ProfileReducer = createReducer(
    initialState,
    on(ProfileActions.loadProfileDataSuccess, (state, action) => ({
        ...state,
        profileData: action.jobseekerProfileData
    })),
    on(ProfileActions.getDrivingLicencesSuccess, (state, action) =>({
        ...state,
        drivingLicences: action.getdrivingLicencesDto
    })),
    on(ProfileActions.getLanguageExamsSuccess, (state, action) =>({
        ...state,
        languageExams: action.getLanguageExamsDto
    })),
    on(ProfileActions.loadCompanyProfileDataSuccess, (state, action) => ({
        ...state,
        companyProfileData: action.companyProfileData
    })),
    on(ProfileActions.getQualitiesSuccess, (state, action) => ({
        ...state,
        qualities: action.getQualitiesDto
    })),
    on(ProfileActions.getExperiencesSuccess, (state, action) => ({
        ...state,
        experiences: action.getExperiencesDto
    })),
    on(ProfileActions.getJobAdvertisementSummariesSuccess, (state, action) => ({
        ...state,
        jobAdvertisementSummaries: action.getJobAdvertisementSummaryDto
    }))
    
);

