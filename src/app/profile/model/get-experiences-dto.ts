export interface GetExperiencesDto{

    id: number,
    name: string,
    startDate: Date,
    endDate: Date

}