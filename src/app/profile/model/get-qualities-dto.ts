import { QualityType } from "src/app/shared/enums/quality.enum";

export interface GetQualitiesDto{

    id: number,
    school: string,
    city: string,
    year: number,
    type: QualityType

}