import { AppliciantDto } from "./appliciant-dto";

export interface JobAdvertisementDto{

    id: number,
    name: string,
    summary: string,
    appliciants: AppliciantDto[];

}