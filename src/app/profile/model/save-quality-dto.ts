import { QualityType } from "src/app/shared/enums/quality.enum";

export interface SaveQualityDto{

    jobseekerId: number,
    city: string,
    school: string, 
    year: number,
    type: QualityType

}