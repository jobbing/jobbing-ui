import { MatchStateType } from "src/app/shared/enums/match-state-type.enum";

export interface AppliciantDto{

    jobseekerId: number,
    jobAdvertisementId: number,
    jobseekerName: string,
    matchState: MatchStateType

}