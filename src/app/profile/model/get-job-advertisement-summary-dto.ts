export interface GetJobAdvertisementSummaryDto{

    id: number,
    name: string,
    summary: string,

}