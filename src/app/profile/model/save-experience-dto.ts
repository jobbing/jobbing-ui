export interface SaveExperienceDto{

    jobseekerId: number,
    name: string,
    startDate: Date,
    endDate: Date,

}