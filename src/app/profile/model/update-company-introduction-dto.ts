export interface UpdateCompanyIntroductionDto{

    companyId: number,
    introduction: string
}