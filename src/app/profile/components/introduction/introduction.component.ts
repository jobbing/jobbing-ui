import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { JobseekerProfileData } from 'src/app/core/model/jobseeker-profile-data';

@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.scss']
})
export class IntroductionComponent implements OnInit {

  @Input() jobseekerProfileData!: JobseekerProfileData;
  @Output() updateIntroductionEvent: EventEmitter<any>;

  introductionForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    ) { 
      this.updateIntroductionEvent = new EventEmitter();
  }

  ngOnInit(): void {
    this.createIntroductionForm();
  }

  updateIntroduction() {
    this.updateIntroductionEvent.emit(this.introductionForm.value);
  }

  private createIntroductionForm() {
    this.introductionForm = this.formBuilder.group({
      introduction: [this.jobseekerProfileData.introduction, Validators.required]
    })
  }

}
