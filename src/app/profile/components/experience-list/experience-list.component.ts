import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { GetExperiencesDto } from '../../model/get-experiences-dto';
import { ExperiencePopupComponent } from '../experience-popup/experience-popup.component';

@Component({
  selector: 'app-experience-list',
  templateUrl: './experience-list.component.html',
  styleUrls: ['./experience-list.component.scss']
})
export class ExperienceListComponent implements OnInit {

  @Input() experiences$!: Observable<GetExperiencesDto[]>;
  @Output() deleteExperienceEvent: EventEmitter<GetExperiencesDto>;
  @Output() saveExperienceEvent: EventEmitter<GetExperiencesDto>;


  constructor(
    private dialog: MatDialog,
  ) { 
    this.deleteExperienceEvent = new EventEmitter();
    this.saveExperienceEvent = new EventEmitter();
  }

  ngOnInit(): void {
  }

  deleteExperience(experience: any){
    this.deleteExperienceEvent.emit(experience);
  }

  saveExperience(){
    this.dialog
      .open(ExperiencePopupComponent, {
        autoFocus: false,
        maxHeight: '95vh',
      }).afterClosed().subscribe(
        data => {
          if(data){
            this.saveExperienceEvent.emit(data);
          }
        }
      )
  }

}
