import { Component, Input, OnInit } from '@angular/core';
import { CompanyProfileData } from 'src/app/core/model/company-profile-data';
import { JobseekerProfileData } from 'src/app/core/model/jobseeker-profile-data';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  @Input() jobseekerProfileData!: JobseekerProfileData;

  constructor() { 
    }
  

  ngOnInit(): void {
  }

}
