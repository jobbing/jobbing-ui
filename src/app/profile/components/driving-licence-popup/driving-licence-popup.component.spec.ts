import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivingLicencePopupComponent } from './driving-licence-popup.component';

describe('DrivingLicencePopupComponent', () => {
  let component: DrivingLicencePopupComponent;
  let fixture: ComponentFixture<DrivingLicencePopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrivingLicencePopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrivingLicencePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
