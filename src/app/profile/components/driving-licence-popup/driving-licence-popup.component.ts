import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GetDrivingLicencesDto } from 'src/app/core/model/get-driving-licences-dto';
import { LanguageExam } from 'src/app/core/model/language-exam-dto';
import { DrivingLicence, DRIVINGLICENCE_TYPES } from 'src/app/shared/enums/driving-licence.enum';

@Component({
  selector: 'app-driving-licence-popup',
  templateUrl: './driving-licence-popup.component.html',
  styleUrls: ['./driving-licence-popup.component.scss']
})
export class DrivingLicencePopupComponent implements OnInit {

  drivingLicenceTypes = DRIVINGLICENCE_TYPES;
  drivingLicenceSelectedList : DrivingLicence[];

  constructor(
    private dialogRef: MatDialogRef<DrivingLicencePopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    this.drivingLicenceSelectedList = data.drivingLicenceSelectedList;
  }

  ngOnInit(): void {
  }

  saveDrivingLicences(){
    this.dialogRef.close({data: this.drivingLicenceSelectedList});
  }

  exitDrivingLicences(){
    this.dialogRef.close();
  }

  selected(value: any){
    if(this.drivingLicenceSelectedList.includes(value)){
      return true;
    }
    else{
      return false;
    }
  }

  checkDrivingLicenceSelected(value: any){
    if(this.drivingLicenceSelectedList.includes(value)){
      this.removeDrivingLicenceFromList(value);
    }
    else{
      this.addDrivingLicenceToList(value);
    }
  }


  private addDrivingLicenceToList(value: any){
    this.drivingLicenceSelectedList.push(value);
  }
  
  private removeDrivingLicenceFromList(value: any){
    const index: number = this.drivingLicenceSelectedList.indexOf(value);
    this.drivingLicenceSelectedList.splice(index, 1);

  }

}
