import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivingLicenceOneComponent } from './driving-licence-one.component';

describe('DrivingLicenceOneComponent', () => {
  let component: DrivingLicenceOneComponent;
  let fixture: ComponentFixture<DrivingLicenceOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrivingLicenceOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrivingLicenceOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
