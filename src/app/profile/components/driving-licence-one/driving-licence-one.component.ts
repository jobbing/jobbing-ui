import { Component, Input, OnInit } from '@angular/core';
import { GetDrivingLicencesDto } from 'src/app/core/model/get-driving-licences-dto';

@Component({
  selector: 'app-driving-licence-one',
  templateUrl: './driving-licence-one.component.html',
  styleUrls: ['./driving-licence-one.component.scss']
})
export class DrivingLicenceOneComponent implements OnInit {

  @Input() drivingLicence!: GetDrivingLicencesDto;
  
  constructor() { }

  ngOnInit(): void {
  }

}
