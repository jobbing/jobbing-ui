import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { GetQualitiesDto } from '../../model/get-qualities-dto';
import { QualityPopupComponent } from '../quality-popup/quality-popup.component';

@Component({
  selector: 'app-quality-list',
  templateUrl: './quality-list.component.html',
  styleUrls: ['./quality-list.component.scss']
})
export class QualityListComponent implements OnInit {

  @Input() qualities$!: Observable<GetQualitiesDto[]>;
  @Output() deleteQualityEvent: EventEmitter<GetQualitiesDto>;
  @Output() saveQualityEvent: EventEmitter<GetQualitiesDto>;

  constructor(
    private dialog: MatDialog,
  ) {
    this.deleteQualityEvent = new EventEmitter();
    this.saveQualityEvent = new EventEmitter();
   }

  ngOnInit(): void {
  }

  deleteQuality(quality: any){
    this.deleteQualityEvent.emit(quality);
  }

  saveQuality(){
    this.dialog
      .open(QualityPopupComponent, {
        autoFocus: false,
        maxHeight: '95vh',
      }).afterClosed().subscribe(
        data => {
          if(data){
            this.saveQualityEvent.emit(data);
          }
        }
      )
  }

}
