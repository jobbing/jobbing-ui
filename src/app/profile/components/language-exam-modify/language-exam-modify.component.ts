import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GetLanguageExamsDto } from 'src/app/core/model/get-language-exams-dto';
import { LanguageExamPopupComponent } from '../language-exam-popup/language-exam-popup.component';

@Component({
  selector: 'app-language-exam-modify',
  templateUrl: './language-exam-modify.component.html',
  styleUrls: ['./language-exam-modify.component.scss']
})
export class LanguageExamModifyComponent implements OnInit {

  @Output() addLanguageExamEvent: EventEmitter<any>;

  constructor(
    private dialog: MatDialog,

  ) {
    this.addLanguageExamEvent = new EventEmitter();
   }

  ngOnInit(): void {
  }

  addLanguageExam(){
    this.dialog
      .open(LanguageExamPopupComponent, {
        autoFocus: false,
        maxHeight: '95vh',

      }).afterClosed().subscribe(
        data => {
          this.addLanguageExamEvent.emit(data)
        }
      )
    }
}
