import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageExamModifyComponent } from './language-exam-modify.component';

describe('LanguageExamModifyComponent', () => {
  let component: LanguageExamModifyComponent;
  let fixture: ComponentFixture<LanguageExamModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguageExamModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageExamModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
