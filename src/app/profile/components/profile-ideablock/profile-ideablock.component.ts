import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { JobseekerProfileData } from 'src/app/core/model/jobseeker-profile-data';
import { fromProfileStore } from '../../store';

@Component({
  selector: 'app-profile-ideablock',
  templateUrl: './profile-ideablock.component.html',
  styleUrls: ['./profile-ideablock.component.scss']
})
export class ProfileIdeablockComponent implements OnInit {

  @Output() updateWorkILookForEvent: EventEmitter<any>;
  @Input() jobseekerProfileData!: JobseekerProfileData;
  workILookForForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,

  ) { 
    this.updateWorkILookForEvent = new EventEmitter();
    this.createWorkILookForForm();
  }

  ngOnInit(): void {
  }

  private createWorkILookForForm(){
    this.workILookForForm = this.formBuilder.group({
      workILookFor: new FormControl( null, Validators.required)
    })
  }

  updateWorkILookFor(){
    this.updateWorkILookForEvent.emit(this.workILookForForm.value);
  }

}
