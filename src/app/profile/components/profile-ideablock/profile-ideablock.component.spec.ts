import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileIdeablockComponent } from './profile-ideablock.component';

describe('ProfileIdeablockComponent', () => {
  let component: ProfileIdeablockComponent;
  let fixture: ComponentFixture<ProfileIdeablockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileIdeablockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileIdeablockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
