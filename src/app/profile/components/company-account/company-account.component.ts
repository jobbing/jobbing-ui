import { Component, Input, OnInit } from '@angular/core';
import { CompanyProfileData } from 'src/app/core/model/company-profile-data';

@Component({
  selector: 'app-company-account',
  templateUrl: './company-account.component.html',
  styleUrls: ['./company-account.component.scss']
})
export class CompanyAccountComponent implements OnInit {

  @Input() companyProfileData!: CompanyProfileData;

  constructor() { }

  ngOnInit(): void {
  }

}
