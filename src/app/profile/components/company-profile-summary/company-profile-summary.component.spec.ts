import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyProfileSummaryComponent } from './company-profile-summary.component';

describe('CompanyProfileSummaryComponent', () => {
  let component: CompanyProfileSummaryComponent;
  let fixture: ComponentFixture<CompanyProfileSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyProfileSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyProfileSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
