import { Component, Input, OnInit } from '@angular/core';
import { CompanyProfileData } from 'src/app/core/model/company-profile-data';
import { loadCompanyProfileData } from '../../store/profile.actions';

@Component({
  selector: 'app-company-profile-summary',
  templateUrl: './company-profile-summary.component.html',
  styleUrls: ['./company-profile-summary.component.scss']
})
export class CompanyProfileSummaryComponent implements OnInit {

  @Input() companyProfileData!: CompanyProfileData | null;

  constructor() { }

  ngOnInit(): void {
  }

}
