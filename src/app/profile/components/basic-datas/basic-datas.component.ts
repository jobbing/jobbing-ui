import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { JobseekerProfileData } from 'src/app/core/model/jobseeker-profile-data';

@Component({
  selector: 'app-basic-datas',
  templateUrl: './basic-datas.component.html',
  styleUrls: ['./basic-datas.component.scss']
})
export class BasicDatasComponent implements OnInit {

  @Input() jobseekerProfileData!: JobseekerProfileData;
  @Output() updatePersonalDataEvent: EventEmitter<any>;

  personalDataForm!: FormGroup;
  modifyPersonalData = false;

  constructor(   
    private formBuilder: FormBuilder,
    ) { 
      this.updatePersonalDataEvent = new EventEmitter();
    }

  ngOnInit(): void {
    this.createPersonalDataForm();
  }

  updatePersonalData() {
    if (this.modifyPersonalData && this.personalDataForm.valid) {
      this.updatePersonalDataEvent.emit(this.personalDataForm.value);
      this.modifyPersonalData = false;
    } else {
      this.personalDataForm.markAllAsTouched();
    }
  }

  editPersonalData() {
    this.modifyPersonalData = true;
  }

  private createPersonalDataForm() {
    this.personalDataForm = this.formBuilder.group({
      birthPlace: new FormControl(this.jobseekerProfileData.birthPlace, Validators.required),
      birthTime: new FormControl(this.jobseekerProfileData.birthTime, Validators.required),
      address: new FormControl(this.jobseekerProfileData.address, Validators.required),
      residence: new FormControl(this.jobseekerProfileData.residence, Validators.required),
    })
  }

}
