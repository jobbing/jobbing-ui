import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementOneComponent } from './job-advertisement-one.component';

describe('JobAdvertisementOneComponent', () => {
  let component: JobAdvertisementOneComponent;
  let fixture: ComponentFixture<JobAdvertisementOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
