import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GetJobAdvertisementSummaryDto } from '../../model/get-job-advertisement-summary-dto';

@Component({
  selector: 'app-job-advertisement-one',
  templateUrl: './job-advertisement-one.component.html',
  styleUrls: ['./job-advertisement-one.component.scss']
})
export class JobAdvertisementOneComponent implements OnInit {

  @Input() jobAdvertisementSummary!: GetJobAdvertisementSummaryDto;

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  NavigateToJobAdvertisement(){
    this.router.navigate(['/job-advertisement', this.jobAdvertisementSummary.id,"company"]);
  }

}
