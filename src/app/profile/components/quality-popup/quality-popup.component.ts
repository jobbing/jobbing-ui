import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { QualityType } from 'src/app/shared/enums/quality.enum';


@Component({
  selector: 'app-quality-popup',
  templateUrl: './quality-popup.component.html',
  styleUrls: ['./quality-popup.component.scss']
})
export class QualityPopupComponent implements OnInit {

  qualityForm!: FormGroup;
  typeList = Object.keys(QualityType)


  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<QualityPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.createForm();
   }

  ngOnInit(): void {
  }

  saveQuality(){
    if(this.qualityForm.valid){

      this.dialogRef.close({data: this.qualityForm.value})
    }
  }

  private createForm(){
    this.qualityForm = this.formBuilder.group({
      city: new FormControl(null, Validators.required),
      school: new FormControl(null, Validators.required),
      year: new FormControl(
        null, (
        Validators.min(1940), 
        Validators.max(2022),
        Validators.required)
        ),
      type: new FormControl(null, Validators.required),
    })
  }

}
