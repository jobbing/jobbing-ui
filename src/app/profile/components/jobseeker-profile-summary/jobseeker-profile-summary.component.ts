import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GetDrivingLicencesDto } from 'src/app/core/model/get-driving-licences-dto';
import { GetLanguageExamsDto } from 'src/app/core/model/get-language-exams-dto';
import { JobseekerProfileData } from 'src/app/core/model/jobseeker-profile-data';
import { GetExperiencesDto } from '../../model/get-experiences-dto';
import { GetQualitiesDto } from '../../model/get-qualities-dto';

@Component({
  selector: 'app-jobseeker-profile-summary',
  templateUrl: './jobseeker-profile-summary.component.html',
  styleUrls: ['./jobseeker-profile-summary.component.scss']
})
export class JobseekerProfileSummaryComponent implements OnInit {

  @Input() profileData!: JobseekerProfileData | null;
  @Input() drivingLicences!: GetDrivingLicencesDto[] | null;
  @Input() languageExams!: GetLanguageExamsDto[] | null;
  @Input() qualities!: GetQualitiesDto[] | null;
  @Input() experiences!: GetExperiencesDto[] | null;

  constructor() { 
  }

  ngOnInit(): void {
  }

}
