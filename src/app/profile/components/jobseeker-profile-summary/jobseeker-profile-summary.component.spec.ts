import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobseekerProfileSummaryComponent } from './jobseeker-profile-summary.component';

describe('JobseekerProfileSummaryComponent', () => {
  let component: JobseekerProfileSummaryComponent;
  let fixture: ComponentFixture<JobseekerProfileSummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobseekerProfileSummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobseekerProfileSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
