import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GetExperiencesDto } from '../../model/get-experiences-dto';

@Component({
  selector: 'app-experience-one',
  templateUrl: './experience-one.component.html',
  styleUrls: ['./experience-one.component.scss']
})
export class ExperienceOneComponent implements OnInit {

  @Input() experience!: GetExperiencesDto;
  @Output() deleteExperienceEvent: EventEmitter<GetExperiencesDto>


  constructor() { 
    this.deleteExperienceEvent = new EventEmitter();
  }

  ngOnInit(): void {
  }

  deleteExperience(){
    this.deleteExperienceEvent.emit(this.experience)
  }
}
