import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceOneComponent } from './experience-one.component';

describe('ExperienceOneComponent', () => {
  let component: ExperienceOneComponent;
  let fixture: ComponentFixture<ExperienceOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExperienceOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
