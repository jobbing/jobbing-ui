import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyBasicDatasComponent } from './company-basic-datas.component';

describe('CompanyBasicDatasComponent', () => {
  let component: CompanyBasicDatasComponent;
  let fixture: ComponentFixture<CompanyBasicDatasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyBasicDatasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyBasicDatasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
