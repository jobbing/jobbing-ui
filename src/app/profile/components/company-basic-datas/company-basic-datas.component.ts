import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { CompanyProfileData } from 'src/app/core/model/company-profile-data';

@Component({
  selector: 'app-company-basic-datas',
  templateUrl: './company-basic-datas.component.html',
  styleUrls: ['./company-basic-datas.component.scss']
})
export class CompanyBasicDatasComponent implements OnInit {

  @Input() companyProfileData!: CompanyProfileData;
  @Output() updatePersonalDataEvent: EventEmitter<any>;

  companyDataForm!: FormGroup;
  modifyPersonalData = false;

  constructor(   
    private formBuilder: FormBuilder,
    ) { 
      this.updatePersonalDataEvent = new EventEmitter();
    }

  ngOnInit(): void {
    this.createCompanyDataForm();
  }

  updateCompanyData() {
    if (this.modifyPersonalData && this.companyDataForm.valid) {
      this.updatePersonalDataEvent.emit(this.companyDataForm.value);
      this.modifyPersonalData = false;
    } else {
      this.companyDataForm.markAllAsTouched();
    }
  } 

  editPersonalData() {
    this.modifyPersonalData = true;
  }

  private createCompanyDataForm() {
    this.companyDataForm = this.formBuilder.group({
      phoneNumber: new FormControl(this.companyProfileData.phoneNumber, [Validators.required]),
      email: new FormControl(this.companyProfileData.email, [Validators.required]),
      address: new FormControl(this.companyProfileData.address, [Validators.required]),
      contactPerson: new FormControl(this.companyProfileData.contactPerson, [Validators.required]),
    })
  }
}
