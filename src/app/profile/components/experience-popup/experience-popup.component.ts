import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-experience-popup',
  templateUrl: './experience-popup.component.html',
  styleUrls: ['./experience-popup.component.scss']
})
export class ExperiencePopupComponent implements OnInit {

  experienceForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ExperiencePopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    this.createForm();
  }

  ngOnInit(): void {
  }

  saveExperience(){
    if(this.experienceForm.valid){
      this.dialogRef.close({data: this.experienceForm.value})
    }
  }

  private createForm(){
    this.experienceForm =  this.formBuilder.group({
      name: new FormControl(null, Validators.required),
      startDate: new FormControl(null, Validators.required),
      endDate: new FormControl(null, (
         Validators.required)
         ),
    })
  }

}
