import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CompanyProfileData } from 'src/app/core/model/company-profile-data';

@Component({
  selector: 'app-company-introduction',
  templateUrl: './company-introduction.component.html',
  styleUrls: ['./company-introduction.component.scss']
})
export class CompanyIntroductionComponent implements OnInit {

  @Input() companyProfileData!: CompanyProfileData;
  @Output() updateIntroductionEvent: EventEmitter<any>;

  introductionForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
  ) {      
    this.updateIntroductionEvent = new EventEmitter();
  }

  ngOnInit(): void {
    this.createIntroductionForm();
  }

  updateIntroduction() {
    this.updateIntroductionEvent.emit(this.introductionForm.value);
  }

  private createIntroductionForm() {
    this.introductionForm = this.formBuilder.group({
      introduction: [this.companyProfileData.introduction, Validators.required]
    })
  }


}
