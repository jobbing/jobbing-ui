import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GetQualitiesDto } from '../../model/get-qualities-dto';

@Component({
  selector: 'app-quality-one',
  templateUrl: './quality-one.component.html',
  styleUrls: ['./quality-one.component.scss']
})
export class QualityOneComponent implements OnInit {

  @Input() quality!: GetQualitiesDto;
  @Output() deleteQualityEvent: EventEmitter<GetQualitiesDto>

  constructor() { 
    this.deleteQualityEvent = new EventEmitter();
  }

  ngOnInit(): void {
  }

  deleteQuality(){
    this.deleteQualityEvent.emit(this.quality)
  }

}
