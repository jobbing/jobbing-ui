import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QualityOneComponent } from './quality-one.component';

describe('QualityOneComponent', () => {
  let component: QualityOneComponent;
  let fixture: ComponentFixture<QualityOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QualityOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QualityOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
