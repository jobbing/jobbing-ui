import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageExamOneComponent } from './language-exam-one.component';

describe('LanguageExamOneComponent', () => {
  let component: LanguageExamOneComponent;
  let fixture: ComponentFixture<LanguageExamOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguageExamOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageExamOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
