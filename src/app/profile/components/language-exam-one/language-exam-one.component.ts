import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GetLanguageExamsDto } from 'src/app/core/model/get-language-exams-dto';
import { LanguageType } from 'src/app/shared/enums/language-type.enum';

@Component({
  selector: 'app-language-exam-one',
  templateUrl: './language-exam-one.component.html',
  styleUrls: ['./language-exam-one.component.scss']
})
export class LanguageExamOneComponent implements OnInit {

  @Input() languageExam!: GetLanguageExamsDto;
  @Output() deleteLanguageExamEvent: EventEmitter<GetLanguageExamsDto>;



  constructor() {
    this.deleteLanguageExamEvent =  new EventEmitter();
   }

  ngOnInit(): void {
  }

  deleteLanguageExam(){
    this.deleteLanguageExamEvent.emit(this.languageExam);
  }


}
