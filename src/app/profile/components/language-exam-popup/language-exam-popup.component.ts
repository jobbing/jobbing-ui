import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LanguageType } from 'src/app/shared/enums/language-type.enum';
import { LevelType } from 'src/app/shared/enums/level-type.enum';

@Component({
  selector: 'app-language-exam-popup',
  templateUrl: './language-exam-popup.component.html',
  styleUrls: ['./language-exam-popup.component.scss']
})
export class LanguageExamPopupComponent implements OnInit {

  languageExamForm!: FormGroup;
  languageList = Object.keys(LanguageType);
  levelList = Object.keys(LevelType);

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<LanguageExamPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.createLanguageExamForm();
   }

  ngOnInit(): void {
  }

  closeLanguageExam(){
    this.dialogRef.close();
  }

  saveLanguageExam(){
    if(this.languageExamForm.valid){
      this.dialogRef.close({data: this.languageExamForm.value});
    }
  }

  private createLanguageExamForm(){
    this.languageExamForm = this.formBuilder.group({
      language: new FormControl(null, Validators.required),
      level: new FormControl(null, Validators.required)
    })
  }
}
