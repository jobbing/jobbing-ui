import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageExamPopupComponent } from './language-exam-popup.component';

describe('LanguageExamPopupComponent', () => {
  let component: LanguageExamPopupComponent;
  let fixture: ComponentFixture<LanguageExamPopupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguageExamPopupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageExamPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
