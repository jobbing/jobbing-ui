import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivingLicenceModifyComponent } from './driving-licence-modify.component';

describe('DrivingLicenceModifyComponent', () => {
  let component: DrivingLicenceModifyComponent;
  let fixture: ComponentFixture<DrivingLicenceModifyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrivingLicenceModifyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrivingLicenceModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
