import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GetDrivingLicencesDto } from 'src/app/core/model/get-driving-licences-dto';
import { DrivingLicence, DRIVINGLICENCE_TYPES } from 'src/app/shared/enums/driving-licence.enum';
import { DrivingLicencePopupComponent } from '../driving-licence-popup/driving-licence-popup.component';

@Component({
  selector: 'app-driving-licence-modify',
  templateUrl: './driving-licence-modify.component.html',
  styleUrls: ['./driving-licence-modify.component.scss']
})
export class DrivingLicenceModifyComponent implements OnInit {

  @Input() drivingLicences!: GetDrivingLicencesDto[];
  @Output() saveDrivingLicenceEvent: EventEmitter<DrivingLicence[]>;

  drivingLicenceSelectedList : DrivingLicence[] = [];

  constructor(
    private dialog: MatDialog,
  ) { 
    this.saveDrivingLicenceEvent = new EventEmitter();
  }

  ngOnInit(): void {
    this.fillListWithSaved();
  }

  editDrivingLicence(){
    this.fillListWithSaved();
    this.dialog
      .open(DrivingLicencePopupComponent, {
        autoFocus: false,
        maxHeight: '95vh',
        data: {
          drivingLicenceSelectedList: this.drivingLicenceSelectedList
        }
      }).afterClosed().subscribe(
        data => this.saveDrivingLicenceEvent.emit(data)
      )
  }

  saveDrivingLicence(){
    this.saveDrivingLicenceEvent.emit(this.drivingLicenceSelectedList);
  }

  private fillListWithSaved(){
    if(this.drivingLicences){
      this.drivingLicenceSelectedList = [];
      for(let drivingLicence of this.drivingLicences){
        this.drivingLicenceSelectedList.push(drivingLicence.value)
      } 
    }
  }

}
