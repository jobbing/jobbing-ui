import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthActions, AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { CompanyProfileData } from 'src/app/core/model/company-profile-data';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { HolderType } from 'src/app/core/model/holder-type';
import { fromProfileStore, ProfileActions, ProfileSelectors } from 'src/app/profile/store';

@Component({
  selector: 'app-company-profile',
  templateUrl: './company-profile.component.html',
  styleUrls: ['./company-profile.component.scss']
})
export class CompanyProfileComponent implements OnInit {

  myData$: Observable<GetMyDatasDto | null>;

  constructor(
    private authStore: Store<fromAuthStore.AuthState>,
  ) { 
    this.myData$ = this.authStore.pipe(select(AuthSelectors.selectMyDatas))
  }

  ngOnInit(): void {
    }

}
