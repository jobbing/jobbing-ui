import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyOpenJobseekerProfileComponent } from './company-open-jobseeker-profile.component';

describe('CompanyOpenJobseekerProfileComponent', () => {
  let component: CompanyOpenJobseekerProfileComponent;
  let fixture: ComponentFixture<CompanyOpenJobseekerProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyOpenJobseekerProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyOpenJobseekerProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
