import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { GetDrivingLicencesDto } from 'src/app/core/model/get-driving-licences-dto';
import { GetLanguageExamsDto } from 'src/app/core/model/get-language-exams-dto';
import { JobseekerProfileData } from 'src/app/core/model/jobseeker-profile-data';
import { GetExperiencesDto } from '../../model/get-experiences-dto';
import { GetQualitiesDto } from '../../model/get-qualities-dto';
import { fromProfileStore, ProfileActions, ProfileSelectors } from '../../store';

@Component({
  selector: 'app-company-open-jobseeker-profile',
  templateUrl: './company-open-jobseeker-profile.component.html',
  styleUrls: ['./company-open-jobseeker-profile.component.scss']
})
export class CompanyOpenJobseekerProfileComponent implements OnInit {
  
  jobseekerId: number;
  profileData$: Observable<JobseekerProfileData | null>;
  drivingLicences$: Observable<GetDrivingLicencesDto[]>;
  languageExams$: Observable<GetLanguageExamsDto[]>;
  qualities$: Observable<GetQualitiesDto[]>;
  experiences$: Observable<GetExperiencesDto[]>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<fromProfileStore.ProfileState>,
  ) { 
    this.jobseekerId = +this.activatedRoute.snapshot.paramMap.get('id')!;
    this.profileData$ = this.store.pipe(select(ProfileSelectors.selectProfileData))
    this.drivingLicences$ = this.store.pipe(select(ProfileSelectors.selectDrivingLicences)); 
    this.languageExams$ = this.store.pipe(select(ProfileSelectors.selectLanguageExams));
    this.qualities$ = this.store.pipe(select(ProfileSelectors.selectQualities)); 
    this.experiences$ = this.store.pipe(select(ProfileSelectors.selectExperiences));
  }

  ngOnInit(): void {
    this.store.dispatch(
      ProfileActions.companyLoadJobseekerProfileData({jobseekerId: this.jobseekerId})
    );
    this.store.dispatch(
      ProfileActions.getDrivingLicences({jobseekerId: this.jobseekerId})
    );
    this.store.dispatch(
      ProfileActions.getLanguageExams({jobseekerId: this.jobseekerId})
    );
    this.store.dispatch(
      ProfileActions.getQualities({jobseekerId: this.jobseekerId})
    );
    this.store.dispatch(
      ProfileActions.getExperiences({jobseekerId: this.jobseekerId})
    );
  }

}
