import { Component, Input, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { select, Store } from '@ngrx/store';
import { filter, Observable, tap } from 'rxjs';
import { GetDrivingLicencesDto } from 'src/app/core/model/get-driving-licences-dto';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { DrivingLicence, DRIVINGLICENCE_TYPES } from 'src/app/shared/enums/driving-licence.enum';
import { DrivingLicencePopupComponent } from '../../components/driving-licence-popup/driving-licence-popup.component';
import { fromProfileStore, ProfileActions, ProfileSelectors } from '../../store';

@Component({
  selector: 'app-driving-licence-block',
  templateUrl: './driving-licence-block.component.html',
  styleUrls: ['./driving-licence-block.component.scss']
})
export class DrivingLicenceBlockComponent implements OnInit {

  drivingLicences$: Observable<GetDrivingLicencesDto[]>;
  drivingLicenceTypes = DRIVINGLICENCE_TYPES;
  drivingLicenceSelectedList : DrivingLicence[] = [];
  
  @Input() myData!: GetMyDatasDto;

  constructor(
    private store: Store<fromProfileStore.ProfileState>,
  ) { 
    this.drivingLicences$ = this.store.pipe(select(ProfileSelectors.selectDrivingLicences)); 
  }

  ngOnInit(): void {
    if(this.myData){
      this.store.dispatch(
        ProfileActions.getDrivingLicences({jobseekerId: this.myData.jobseekerId})
      );
    }

  }

  saveDrivingLicenceEvent(drivingLicences:any){
    this.store.dispatch(
      ProfileActions.saveDrivingLicences(
        {
          saveDrivivingLicenceDto:{
            jobseekerId: this.myData.jobseekerId,
            value: drivingLicences.data
          }
        }
      )
    )
  }

}

