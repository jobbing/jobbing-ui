import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DrivingLicenceBlockComponent } from './driving-licence-block.component';

describe('DrivingLicenceBlockComponent', () => {
  let component: DrivingLicenceBlockComponent;
  let fixture: ComponentFixture<DrivingLicenceBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DrivingLicenceBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DrivingLicenceBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
