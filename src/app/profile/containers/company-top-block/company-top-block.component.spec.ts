import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyTopBlockComponent } from './company-top-block.component';

describe('CompanyTopBlockComponent', () => {
  let component: CompanyTopBlockComponent;
  let fixture: ComponentFixture<CompanyTopBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompanyTopBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyTopBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
