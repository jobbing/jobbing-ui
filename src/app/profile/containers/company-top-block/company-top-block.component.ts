import { Component, Input, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CompanyProfileData } from 'src/app/core/model/company-profile-data';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { fromProfileStore, ProfileActions, ProfileSelectors } from '../../store';

@Component({
  selector: 'app-company-top-block',
  templateUrl: './company-top-block.component.html',
  styleUrls: ['./company-top-block.component.scss']
})
export class CompanyTopBlockComponent implements OnInit {

  profileData$: Observable<CompanyProfileData | null>;
  @Input() myData!: GetMyDatasDto;

  constructor(
    private store: Store<fromProfileStore.ProfileState>,

  ) { 
    this.profileData$ = this.store.pipe(select(ProfileSelectors.selectCompanyProfileData));
    
  }

  ngOnInit(): void {
    this.store.dispatch(
      ProfileActions.loadCompanyProfileData({companyId: this.myData.companyId})
      );
      
  }

  updateCompanyData(companyDataForm:any){
    this.store.dispatch(
      ProfileActions.updateCompanyData({
        
        updateCompanyDataDto:{
          companyId: this.myData.companyId,
          phoneNumber: companyDataForm.phoneNumber,
          email:companyDataForm.email,
          address: companyDataForm.address,
          contactPerson: companyDataForm.contactPerson,
        }
      }
      )
    )
  }

  updateIntroduction(introductionForm: any){
    this.store.dispatch(
      ProfileActions.updateCompanyIntroduction(
        {
          updateCompanyIntroductionDto:{
            companyId: this.myData.companyId,
            introduction: introductionForm.introduction
          }
        }
      )
    )
  }

}
