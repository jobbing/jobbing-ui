import { Component, Input, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { GetQualitiesDto } from '../../model/get-qualities-dto';
import { SaveQualityDto } from '../../model/save-quality-dto';
import { fromProfileStore, ProfileActions, ProfileSelectors } from '../../store';

@Component({
  selector: 'app-quality-block',
  templateUrl: './quality-block.component.html',
  styleUrls: ['./quality-block.component.scss']
})
export class QualityBlockComponent implements OnInit {

  qualities$: Observable<GetQualitiesDto[]>;

  @Input() myData!: GetMyDatasDto;

  constructor(
    private store: Store<fromProfileStore.ProfileState>,
  ) {
    this.qualities$ = this.store.pipe(select(ProfileSelectors.selectQualities));
   }

  ngOnInit(): void {
    if(this.myData){
      this.store.dispatch(
        ProfileActions.getQualities({jobseekerId: this.myData.jobseekerId})
      )
    }
  }

  deleteQuality(quality: any){
    if(this.myData){
      this.store.dispatch(
        ProfileActions.deleteQuality({qualityId: quality.id})
      )
    }
  }

  saveQuality(quality: any){
    if(this.myData){
      this.store.dispatch(
        ProfileActions.saveQuality({
          saveQualityDto: {
            jobseekerId: this.myData.jobseekerId,
            city: quality.data.city,
            school: quality.data.school,
            type: quality.data.type,
            year: quality.data.year,
          }
        })
      )
    }
  }


}
