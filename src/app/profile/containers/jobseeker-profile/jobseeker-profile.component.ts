import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable, Subject } from 'rxjs';
import { AuthActions, AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { GetDrivingLicencesDto } from 'src/app/core/model/get-driving-licences-dto';
import { GetLanguageExamsDto } from 'src/app/core/model/get-language-exams-dto';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { HolderType } from 'src/app/core/model/holder-type';
import { JobseekerProfileData } from 'src/app/core/model/jobseeker-profile-data';
import { fromProfileStore, ProfileActions, ProfileSelectors } from 'src/app/profile/store'

@Component({
  selector: 'app-jobseeker-profile',
  templateUrl: './jobseeker-profile.component.html',
  styleUrls: ['./jobseeker-profile.component.scss']
})
export class JobseekerProfileComponent implements OnInit {

  profileData$: Observable<JobseekerProfileData | null>;
  myData$: Observable<GetMyDatasDto | null>;
  jobseekerId!: number | undefined;

  constructor(
    private store: Store<fromProfileStore.ProfileState>,
    private authStore: Store<fromAuthStore.AuthState>

  ) {
    this.profileData$ = this.store.pipe(select(ProfileSelectors.selectProfileData));
    this.myData$ = this.authStore.pipe(select(AuthSelectors.selectMyDatas)); 
  }

  ngOnInit(): void {
    this.store.dispatch(
      ProfileActions.loadProfileData()
    );
  }

  getJobseekerId(){
    this.myData$.subscribe(event => this.jobseekerId = event?.jobseekerId);
  }

  updateWorkILookFor(workILookForForm: any){
    this.getJobseekerId();
    
    if(this.jobseekerId){
      this.store.dispatch(
        ProfileActions.updateWorkILookFor(
          {
            updateWorkILookForDto:{
              jobseekerId: this.jobseekerId,
              workILookFor: workILookForForm.workILookFor
            }
          }
        )
      )
    }
  }

}
