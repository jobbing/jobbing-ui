import { Component, Input, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { GetExperiencesDto } from '../../model/get-experiences-dto';
import { fromProfileStore, ProfileActions, ProfileSelectors } from '../../store';

@Component({
  selector: 'app-experience-block',
  templateUrl: './experience-block.component.html',
  styleUrls: ['./experience-block.component.scss']
})
export class ExperienceBlockComponent implements OnInit {

  experiences$: Observable<GetExperiencesDto[]>;

  @Input() myData!: GetMyDatasDto;

  constructor(
    private store: Store<fromProfileStore.ProfileState>,
  ) { 
    this.experiences$ = this.store.pipe(select(ProfileSelectors.selectExperiences));
  }

  ngOnInit(): void {
    if(this.myData){
      this.store.dispatch(
        ProfileActions.getExperiences({jobseekerId: this.myData.jobseekerId})
      )
    }
  }

  deleteExperience(experience: any){
    if(this.myData){
      this.store.dispatch(
        ProfileActions.deleteExperience({experinceId: experience.id})
      )
    }
  }

  saveExperience(experience: any){
    if(this.myData){
      this.store.dispatch(
        ProfileActions.saveExperience({
          saveExperinceDto: {
            jobseekerId: this.myData.jobseekerId,
            endDate: experience.data.endDate,
            startDate: experience.data.startDate,
            name: experience.data.name
          }
        })
      )
    }
  }

}
