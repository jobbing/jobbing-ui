import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementBlockComponent } from './job-advertisement-block.component';

describe('JobAdvertisementBlockComponent', () => {
  let component: JobAdvertisementBlockComponent;
  let fixture: ComponentFixture<JobAdvertisementBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
