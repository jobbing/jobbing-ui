import { Component, Input, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { GetJobAdvertisementSummaryDto } from '../../model/get-job-advertisement-summary-dto';
import { fromProfileStore, ProfileActions, ProfileSelectors } from '../../store';

@Component({
  selector: 'app-job-advertisement-block',
  templateUrl: './job-advertisement-block.component.html',
  styleUrls: ['./job-advertisement-block.component.scss']
})
export class JobAdvertisementBlockComponent implements OnInit {

  jobAdvertisements$: Observable<GetJobAdvertisementSummaryDto[] | null>;
  @Input() myData!: GetMyDatasDto;

  constructor(
    private store: Store<fromProfileStore.ProfileState>,
  ) { 
    this.jobAdvertisements$ = this.store.pipe(select(ProfileSelectors.selectJobAdvertisementSummaries));
  }

  ngOnInit(): void {
    if(this.myData){   
      this.store.dispatch(
        ProfileActions.getJobAdvertisementSummaries({
          companyId: this.myData.companyId
        })
      )
    
  }
  }

}
