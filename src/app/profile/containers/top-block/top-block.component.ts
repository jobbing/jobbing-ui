import { Component, Input, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { JobseekerProfileData } from 'src/app/core/model/jobseeker-profile-data';
import { fromProfileStore, ProfileActions, ProfileSelectors } from '../../store';

@Component({
  selector: 'app-top-block',
  templateUrl: './top-block.component.html',
  styleUrls: ['./top-block.component.scss']
})
export class TopBlockComponent implements OnInit {

  @Input() myData!: GetMyDatasDto;
  @Input() profileData$!: Observable<JobseekerProfileData | null>;

  constructor(
    private store: Store<fromProfileStore.ProfileState>,
  ) {}

  ngOnInit(): void {
  }


  updatePersonalData(personalDataForm:any){
    if(this.myData){   
      this.store.dispatch(
        ProfileActions.updatePersonalData({
          
          updatePersonalDataDto:{
            jobseekerId: this.myData.jobseekerId,
            birthPlace: personalDataForm.birthPlace,
            birthTime:personalDataForm.birthTime,
            address: personalDataForm.address,
            residence: personalDataForm.residence,
          }
        }
        )
    )}
  }

  updateIntroduction(introductionForm: any){
    this.store.dispatch(
      ProfileActions.updateIntroduction(
        {
          updateIntroductionDto:{
            jobseekerId: this.myData.jobseekerId,
            introduction: introductionForm.introduction
          }
        }
      )
    )
  }
}
