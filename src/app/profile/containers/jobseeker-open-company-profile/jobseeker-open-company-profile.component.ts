import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CompanyProfileData } from 'src/app/core/model/company-profile-data';
import { fromProfileStore, ProfileActions, ProfileSelectors } from '../../store';

@Component({
  selector: 'app-jobseeker-open-company-profile',
  templateUrl: './jobseeker-open-company-profile.component.html',
  styleUrls: ['./jobseeker-open-company-profile.component.scss']
})
export class JobseekerOpenCompanyProfileComponent implements OnInit {

  companyProfileData$!: Observable<CompanyProfileData | null>;
  id!: number;

  constructor(
    private activatedRoute: ActivatedRoute,
    private store: Store<fromProfileStore.ProfileState>
  ) { 
    this.companyProfileData$ = this.store.pipe(select(ProfileSelectors.selectCompanyProfileData));
  }

  ngOnInit(): void {
    this.getId();
    this.store.dispatch(ProfileActions.loadCompanyProfileData({
      companyId: this.id
    }))
  }

  private getId(){
    this.id = +this.activatedRoute.snapshot.paramMap.get("id")!;
  }

}
