import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobseekerOpenCompanyProfileComponent } from './jobseeker-open-company-profile.component';

describe('JobseekerOpenCompanyProfileComponent', () => {
  let component: JobseekerOpenCompanyProfileComponent;
  let fixture: ComponentFixture<JobseekerOpenCompanyProfileComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobseekerOpenCompanyProfileComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobseekerOpenCompanyProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
