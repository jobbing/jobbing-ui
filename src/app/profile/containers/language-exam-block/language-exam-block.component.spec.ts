import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageExamBlockComponent } from './language-exam-block.component';

describe('LanguageExamBlockComponent', () => {
  let component: LanguageExamBlockComponent;
  let fixture: ComponentFixture<LanguageExamBlockComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguageExamBlockComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LanguageExamBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
