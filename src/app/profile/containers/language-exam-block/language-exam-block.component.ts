import { Component, Input, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { GetLanguageExamsDto } from 'src/app/core/model/get-language-exams-dto';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { fromProfileStore, ProfileActions, ProfileSelectors } from '../../store';

@Component({
  selector: 'app-language-exam-block',
  templateUrl: './language-exam-block.component.html',
  styleUrls: ['./language-exam-block.component.scss']
})
export class LanguageExamBlockComponent implements OnInit {

  @Input() myData!: GetMyDatasDto;

  languageExams$: Observable<GetLanguageExamsDto[]>;

  constructor(
    private store: Store<fromProfileStore.ProfileState>,
  ) { 
    this.languageExams$ = this.store.pipe(select(ProfileSelectors.selectLanguageExams));
  }

  ngOnInit(): void {
    if(this.myData)
    this.store.dispatch(
      ProfileActions.getLanguageExams({jobseekerId: this.myData.jobseekerId })
    );
  }
  
  addLanguageExamEvent(languageExamForm: any){
   this.store.dispatch(
     ProfileActions.saveLanguageExam({
      saveLanguageExamDto:{
        jobseekerId: this.myData.jobseekerId,
        language: languageExamForm.data.language,
        level: languageExamForm.data.level
      }
     }
    )
   )
  }

  deleteLanguageExamEvent(languageExam: GetLanguageExamsDto){
    this.store.dispatch(
      ProfileActions.deleteLanguageExam({
        deleteLanguageExamDto:{
          jobseekerId:this.myData.jobseekerId,
          languageExamId:languageExam.languageExamId
        }
      })
    )
  }

}
