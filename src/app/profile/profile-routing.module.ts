import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CompanyOpenJobseekerProfileComponent } from "./containers/company-open-jobseeker-profile/company-open-jobseeker-profile.component";
import { CompanyProfileComponent } from "./containers/company-profile/company-profile.component";
import { JobseekerOpenCompanyProfileComponent } from "./containers/jobseeker-open-company-profile/jobseeker-open-company-profile.component";
import { JobseekerProfileComponent } from "./containers/jobseeker-profile/jobseeker-profile.component";

const routes: Routes = [
    { path: 'jobseeker', component: JobseekerProfileComponent},
    { path: 'company', component: CompanyProfileComponent},
    { path: 'jobseeker/:id/company', component: CompanyOpenJobseekerProfileComponent},
    { path: 'company/:id/jobseeker', component: JobseekerOpenCompanyProfileComponent}
]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]

})
export class ProfileRoutingModule {}