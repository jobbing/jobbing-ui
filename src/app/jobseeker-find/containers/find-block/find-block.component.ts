import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { fromJobseekApplicationsStore, JobseekerApplicationsSelectors } from 'src/app/jobseeker-applications/store';
import { JobseekerApplicationsState } from 'src/app/jobseeker-applications/store/jobseeker-applications.reducers';
import { GetListValuesDto } from 'src/app/manage-job-advertisements/model/get-list-values-dto';
import { fromJobAdvertisementStore, JobAdvertisementActions, JobAdvertisementSelectors } from 'src/app/shared/job-advertisement/store';
import { JobAdvertisementFindDto } from '../../model/job-advertisement-find-dto';
import { fromJobseekerFindStore, JobseekerFindActions, JobseekerFindSelectors } from '../../store';

@Component({
  selector: 'app-find-block',
  templateUrl: './find-block.component.html',
  styleUrls: ['./find-block.component.scss']
})
export class FindBlockComponent implements OnInit {

  myDatas$: Observable<GetMyDatasDto | null>;
  listValues$: Observable<GetListValuesDto | null>;
  jobseekerId!: number;
  jobAdvertisement$!: Observable<JobAdvertisementFindDto | null>;

  constructor(
    private authStore: Store<fromAuthStore.AuthState>,
    private store: Store<fromJobAdvertisementStore.JobAdvertisementState>,
    private applictionStore: Store<fromJobseekerFindStore.JobseekerFindState>,
    private router: Router
    ) { 
      this.myDatas$ = this.authStore.pipe(select(AuthSelectors.selectMyDatas));
      this.listValues$ = this.store.pipe(select(JobAdvertisementSelectors.selectListValues));
      this.jobAdvertisement$ = this.applictionStore.pipe(select(JobseekerFindSelectors.selectjobseekerPageDto))
  }

  ngOnInit(): void {

    this.store.dispatch(
      JobAdvertisementActions.getListValues()
    )
  }

  rejectJobAdvertisementEvent(jobAdvertisementId: number){
    this.store.dispatch(
      JobseekerFindActions.jobseekerRejectjobAdvertisement({
        jobseekerMatchDto:{
          jobAdvertisementId: jobAdvertisementId,
          jobseekerId: this.jobseekerId
        }
      })
    )
    this.reloadCurrentRoute();
  }

  acceptJobAdvertisementEvent(jobAdvertisementId: number){
    this.store.dispatch(
      JobseekerFindActions.jobseekerAcceptJobAdvertisement({
        jobseekerMatchDto:{
          jobAdvertisementId: jobAdvertisementId,
          jobseekerId: this.jobseekerId
        }
      })
    )
    this.reloadCurrentRoute();
  }

  findJobAdvertisementEvent(lists: any){
    this.getJobseekerId();
    this.store.dispatch(
      JobseekerFindActions.pagejobAdvertisements({
        jobAdvertisementFilter:{
          jobseekerId: this.jobseekerId,
          cityIdList: lists.citySelectedList,
          workCategoryIdList: lists.workCategorySelectedList,
          workTypeIdList: lists.workingTypeSelectedList
        },
        paging:{
          page: 0,
          size: 1
        }
      })
    )
  }

  private reloadCurrentRoute(){
    this.store.dispatch(JobseekerFindActions.removeJobAdvertisementFromStore())
    let currentUrl = this.router.url;
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() => {
        this.router.navigate([currentUrl]);
    });
  }

  navigateToJobAdvertisement(){
    this.router.navigate(["job-advertisement",1,"jobseeker"]);
  }

  private getJobseekerId(){
    this.myDatas$.subscribe(event => this.jobseekerId = event?.jobseekerId!)
  }

}
