import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { JobAdvertisementFindDto } from '../../model/job-advertisement-find-dto';

@Component({
  selector: 'app-job-advertisement-page',
  templateUrl: './job-advertisement-page.component.html',
  styleUrls: ['./job-advertisement-page.component.scss']
})
export class JobAdvertisementPageComponent implements OnInit {

  @Input() jobAdvertisement!: JobAdvertisementFindDto | null;
  @Output() rejectJobAdvertisementEvent: EventEmitter<number>;
  @Output() acceptJobAdvertisementEvent: EventEmitter<number>;

  constructor(
    private router: Router
  ) { 
    this.rejectJobAdvertisementEvent = new EventEmitter();
    this.acceptJobAdvertisementEvent = new EventEmitter();
  }

  ngOnInit(): void {
  }

  navigateToJobAdvertisement(){
    if(this.jobAdvertisement)
    this.router.navigate(["job-advertisement",this.jobAdvertisement.id,"jobseeker"]);
  }

  rejectJobAdvertisement(){
    this.rejectJobAdvertisementEvent.emit(this.jobAdvertisement?.id);
  }

  acceptJobAdvertisement(){
    this.acceptJobAdvertisementEvent.emit(this.jobAdvertisement?.id)
  }

}
