import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementPageComponent } from './job-advertisement-page.component';

describe('JobAdvertisementPageComponent', () => {
  let component: JobAdvertisementPageComponent;
  let fixture: ComponentFixture<JobAdvertisementPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
