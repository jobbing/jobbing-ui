import { ComponentFixture, TestBed } from '@angular/core/testing';

import { JobAdvertisementFilterComponent } from './job-advertisement-filter.component';

describe('JobAdvertisementFilterComponent', () => {
  let component: JobAdvertisementFilterComponent;
  let fixture: ComponentFixture<JobAdvertisementFilterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ JobAdvertisementFilterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(JobAdvertisementFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
