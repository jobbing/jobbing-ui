import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { GetListValuesDto } from 'src/app/manage-job-advertisements/model/get-list-values-dto';
import { CITY_TYPES, CityType } from 'src/app/shared/enums/city-type.enum';
import { WORK_CATEGORIES, WorkCategory } from 'src/app/shared/enums/work-category.enum';
import { WORKING_TYPES, WorkingType } from 'src/app/shared/enums/work-type.enum';

@Component({
  selector: 'app-job-advertisement-filter',
  templateUrl: './job-advertisement-filter.component.html',
  styleUrls: ['./job-advertisement-filter.component.scss']
})
export class JobAdvertisementFilterComponent implements OnInit {

  @Input() listValues!: GetListValuesDto | null;
  @Input() myData!: GetMyDatasDto | null;
  @Output() findJobAdvertisementEvent: EventEmitter<any>;

  workingTypes = WORKING_TYPES;
  workTypeDtoSelectedList: number[] = [];
  workingTypeSelectedList: WorkingType[] = [];
  cities = CITY_TYPES;
  cityDtoSelectedList: number[] = [];
  citySelectedList: CityType[] = [];
  workCategories = WORK_CATEGORIES;
  workCategoryDtoSelectedList: number[] = [];
  workCategorySelectedList: WorkCategory[] = [];
  type = true;
  category = false;
  city = false;
  companyId!: number;

  constructor() {
    this.findJobAdvertisementEvent = new EventEmitter();
   }

  ngOnInit(): void {
  }

  find(){

    for(let workTypeName of this.workingTypeSelectedList){
      if(this.listValues?.workTypes[this.workingTypeSelectedList.indexOf(workTypeName)])
      this.workTypeDtoSelectedList.push(this.listValues?.workTypes[this.workingTypeSelectedList.indexOf(workTypeName)].id);
    }
    for(let cityName of this.citySelectedList){
      if(this.listValues?.cities[this.citySelectedList.indexOf(cityName)])
      this.cityDtoSelectedList.push(this.listValues?.cities[this.citySelectedList.indexOf(cityName)].id);
    }
    for(let workCategoryName of this.workCategorySelectedList){
      if(this.listValues?.workCategories[this.workCategorySelectedList.indexOf(workCategoryName)])
      this.workCategoryDtoSelectedList.push(this.listValues?.workCategories[this.workCategorySelectedList.indexOf(workCategoryName)].id);
    }

    this.findJobAdvertisementEvent.emit({
        citySelectedList: this.cityDtoSelectedList,
        workCategorySelectedList: this.workCategoryDtoSelectedList,
        workingTypeSelectedList: this.workTypeDtoSelectedList
    })
  }

  selectedCities(value: any){
    if(this.citySelectedList.includes(value)){
      return true;
    }
    else{
      return false;
    }
  }

  selectedTypes(value: any){
    if(this.workingTypeSelectedList.includes(value)){
      return true;
    }
    else{
      return false;
    }
  }

  selectedCategoryes(value: any){
    if(this.workCategorySelectedList.includes(value)){
      return true;
    }
    else{
      return false;
    }
  }

  checkCitySelected(value: any){
    if(this.citySelectedList.includes(value)){
      this.removeCityFromList(value);
    }
    else{
      this.addCityToList(value);
    }
  }

  checkWorkingTypeSelected(value: any){
    if(this.workingTypeSelectedList.includes(value)){
      this.removeWorkingTypeFromList(value);
    }
    else{
      this.addWorkingTypeToList(value);
    }
  }

  checkWorkCategorySelected(value: any){
    if(this.workCategorySelectedList.includes(value)){
      this.removeWorkCategoryFromList(value);
    }
    else{
      this.addWorkCategoryToList(value);
    }
  }

  private addWorkCategoryToList(value: any){
    this.workCategorySelectedList.push(value);
  }
  
  private removeWorkCategoryFromList(value: any){
    const index: number = this.workCategorySelectedList.indexOf(value);
    this.workCategorySelectedList.splice(index, 1);

  }

  private addCityToList(value: any){
    this.citySelectedList.push(value);
  }
  
  private removeCityFromList(value: any){
    const index: number = this.citySelectedList.indexOf(value);
    this.citySelectedList.splice(index, 1);

  }

  private addWorkingTypeToList(value: any){
    this.workingTypeSelectedList.push(value);
  }
  
  private removeWorkingTypeFromList(value: any){
    const index: number = this.workingTypeSelectedList.indexOf(value);
    this.workingTypeSelectedList.splice(index, 1);

  }


  openCity(){
    this.city = true;
    this.category = false;
    this.type = false;
  }

  openCategory(){
    this.city = false;
    this.category = true;
    this.type = false;
  }
  
  openType(){
    this.city = false;
    this.category = false;
    this.type = true;
  }
}
