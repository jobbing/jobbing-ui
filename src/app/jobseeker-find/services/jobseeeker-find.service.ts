import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Paging } from "src/app/manage-jobseekers/model/paging-dto";
import { JobAdvertisementFilter } from "../model/job-advertisement-filter";
import { JobAdvertisementFindDto } from "../model/job-advertisement-find-dto";

@Injectable()
export class JobseekerFindService{

    private readonly URL = '/Jobseeker-find-jobAdvertisements';

    constructor(
        private http: HttpClient
    ) { }

    pageJobAdvertisements(jobAdvertisementFilter: JobAdvertisementFilter, paging: Paging){
        let reqUrl = `${this.URL}/page?page=${paging.page}&size=${paging.size}`;
        return this.http.post<JobAdvertisementFindDto>(reqUrl, jobAdvertisementFilter);
    }
}