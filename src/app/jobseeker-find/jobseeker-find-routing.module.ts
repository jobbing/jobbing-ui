import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FindBlockComponent } from "./containers/find-block/find-block.component";

const routes: Routes = [
    {path: 'jobseeker', component: FindBlockComponent},
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class JobseekerFindRoutingModule {}