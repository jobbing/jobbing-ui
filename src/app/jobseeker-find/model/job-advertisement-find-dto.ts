export interface JobAdvertisementFindDto{

    id: number,
    name: string,
    companyLogo: string,
    companyName: string, 
    place: string,
    summary: string

}