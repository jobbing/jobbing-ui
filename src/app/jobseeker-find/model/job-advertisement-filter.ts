export interface JobAdvertisementFilter{

    jobseekerId: number,
    cityIdList: number[],
    workCategoryIdList: number[],
    workTypeIdList: number[],

}