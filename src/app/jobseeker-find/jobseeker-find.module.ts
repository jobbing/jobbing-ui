import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { ReactiveFormsModule } from "@angular/forms";
import { HeaderComponent } from "../shared/header/header.component";
import { SharedModule } from "../shared/shared.module";
import { FindBlockComponent } from "./containers/find-block/find-block.component";
import { JobseekerFindRoutingModule } from "./jobseeker-find-routing.module";
import { JobAdvertisementFilterComponent } from './components/job-advertisement-filter/job-advertisement-filter.component';
import { JobAdvertisementPageComponent } from './components/job-advertisement-page/job-advertisement-page.component';
import { StoreModule } from "@ngrx/store";
import { fromJobseekerFindStore } from "./store";
import { EffectsModule } from "@ngrx/effects";
import { JobseekerFindEffects } from "./store/jobseeker-find.effects";
import { JobseekerFindService } from "./services/jobseeeker-find.service";
import { MatchByJobAdvertisementService } from "../manage-job-advertisements/services/match-by-job-advertisement.service";


@NgModule({
    imports:[
        SharedModule,
        CommonModule,
        ReactiveFormsModule,
        JobseekerFindRoutingModule,
        StoreModule.forFeature(
            fromJobseekerFindStore.JobseekerFindFeatureKey,
            fromJobseekerFindStore.ManageJobAdvertisementsReducer
        ),
        EffectsModule.forFeature([JobseekerFindEffects])
    ],
    declarations:[
        FindBlockComponent,
        JobAdvertisementFilterComponent,
        JobAdvertisementPageComponent
    ],
    providers:[
        MatchByJobAdvertisementService,
        JobseekerFindService,
        HeaderComponent
    ]
})

export class JobseekerFindModule{}