import { createAction, props } from "@ngrx/store";
import { FailData } from "src/app/core/model/fail-message.model";
import { AdvertisementStateFilter } from "src/app/jobseeker-applications/model/advertisement-state-filter";
import { JobAdvertisementDto } from "src/app/jobseeker-applications/model/job-advertisement-dto";
import { jobseekerMatchDto } from "src/app/manage-job-advertisements/model/jobseeker-match-dto";
import { Paging } from "src/app/manage-jobseekers/model/paging-dto";
import { JobAdvertisementFilter } from "../model/job-advertisement-filter";
import { JobAdvertisementFindDto } from "../model/job-advertisement-find-dto";

const prefix = '[jobseeker-find]';

export const removeJobAdvertisementFromStore = createAction(
    `${prefix} remove job advertisement from store`,
)

export const pagejobAdvertisements = createAction(
    `${prefix} page job advertisements`,
    props<{ jobAdvertisementFilter: JobAdvertisementFilter, paging: Paging}>()
);

export const pagejobAdvertisementsSuccess = createAction(
    `${prefix} page job advertisements success`,
    props<{ jobAdvertisementFindDto: JobAdvertisementFindDto }>()
);

export const pagejobAdvertisementsFail = createAction(
    `${prefix} page job advertisements fail`,
    props<{ failData: FailData }>()
);

export const jobseekerAcceptJobAdvertisement = createAction(
    `${prefix} jobseeker accept job advertisements`,
    props<{ jobseekerMatchDto: jobseekerMatchDto }>()
);

export const jobseekerAcceptJobAdvertisementSuccess = createAction(
    `${prefix} jobseeker accept job advertisements success`,
);

export const jobseekerAcceptJobAdvertisementFail = createAction(
    `${prefix} jobseeker accept job advertisements fail`,
    props<{ failData: FailData }>()
);

export const jobseekerRejectjobAdvertisement = createAction(
    `${prefix} jobseeker reject job advertisements`,
    props<{ jobseekerMatchDto: jobseekerMatchDto }>()
);

export const jobseekerRejectjobAdvertisementSuccess = createAction(
    `${prefix} jobseeker reject job advertisements success`
);

export const jobseekerRejectjobAdvertisementFail = createAction(
    `${prefix} jobseeker reject job advertisements fail`,
    props<{ failData: FailData }>()
);