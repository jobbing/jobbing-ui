import { createReducer, on } from "@ngrx/store";
import { JobseekerFindActions } from ".";
import { JobAdvertisementFindDto } from "../model/job-advertisement-find-dto";


export const JobseekerFindFeatureKey = 'jobseeker-find';

export interface JobseekerFindState {
    jobAdvertisementFindDto: JobAdvertisementFindDto | null;
}

export const initialState: JobseekerFindState = {
    jobAdvertisementFindDto: null,
}

export const ManageJobAdvertisementsReducer = createReducer(
    initialState,
    on(JobseekerFindActions.pagejobAdvertisementsSuccess, (state, action) => ({
        ...state,
        jobAdvertisementFindDto: action.jobAdvertisementFindDto
    })),
    on(JobseekerFindActions.removeJobAdvertisementFromStore, (state,action) => ({
        ...state,
        jobAdvertisementFindDto: null
    }))
);