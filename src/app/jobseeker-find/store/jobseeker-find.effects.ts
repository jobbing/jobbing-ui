import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, switchMap } from "rxjs";
import { MatchByJobAdvertisementService } from "src/app/manage-job-advertisements/services/match-by-job-advertisement.service";
import { JobAdvertisementActions } from "src/app/shared/job-advertisement/store";
import { JobseekerFindActions } from ".";
import { JobseekerFindService } from "../services/jobseeeker-find.service";
import { pagejobAdvertisements } from "./jobseeker-find.actions";

@Injectable()
export class JobseekerFindEffects{

    pagejobAdvertisements$ = createEffect(() => this.actions$
    .pipe(
        ofType(JobseekerFindActions.pagejobAdvertisements),
        switchMap(action => 
            this.jobseekerFindService.pageJobAdvertisements(action.jobAdvertisementFilter,action.paging)
            .pipe(
                map(jobAdvertisementFindDto => JobseekerFindActions.pagejobAdvertisementsSuccess({jobAdvertisementFindDto}))
            ))
    ));

    jobseekerAcceptjobAdvertisement$ = createEffect(() => this.actions$
    .pipe(
        ofType(JobseekerFindActions.jobseekerAcceptJobAdvertisement),
        switchMap(action => 
            this.matchByJobAdvertisementService.jobseekerAcceptJobseekerByAdvertisement(action.jobseekerMatchDto)
            .pipe(
                map(() => JobseekerFindActions.jobseekerAcceptJobAdvertisementSuccess())
            ))
    ));

    jobseekerRejectjobAdvertisement$ = createEffect(() => this.actions$
    .pipe(
        ofType(JobseekerFindActions.jobseekerRejectjobAdvertisement),
        switchMap(action => 
            this.matchByJobAdvertisementService.jobseekerRejectJobseekerByAdvertisement(action.jobseekerMatchDto)
            .pipe(
                map(() => JobseekerFindActions.jobseekerRejectjobAdvertisementSuccess())
            ))
    ));

    constructor(
        private actions$: Actions,
        private jobseekerFindService: JobseekerFindService,
        private matchByJobAdvertisementService: MatchByJobAdvertisementService,
    ){}

}