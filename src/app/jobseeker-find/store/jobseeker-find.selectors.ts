import { createFeatureSelector, createSelector } from "@ngrx/store";
import { JobseekerFindFeatureKey, JobseekerFindState } from "./jobseeker-find.reducers";

const selectJobseekerFindState = createFeatureSelector<JobseekerFindState>(JobseekerFindFeatureKey);

export const selectjobseekerPageDto = createSelector(selectJobseekerFindState, (state) => state.jobAdvertisementFindDto);

