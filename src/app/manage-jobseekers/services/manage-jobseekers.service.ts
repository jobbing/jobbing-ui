import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { JobseekerDto } from "../model/jobseeker-dto";
import { JobseekerFilter } from "../model/jobseeker-filter";

@Injectable()
export class ManageJobseekersService {

    private readonly URL = '/company-find-jobseekers';
    
    constructor(
        private http: HttpClient
    ) { }

    pageJobseekers(jobseekerFilter: JobseekerFilter, paging: any){
        let reqUrl = `${this.URL}/page?size=${paging.pageSize}&page=${paging.pageStart}`;
        return this.http.post<JobseekerDto>(reqUrl, jobseekerFilter);
    }

    getMatchedJobseekers(companyId: number){
        let reqUrl = `${this.URL}/get-matched-jobseekers?companyId=`+companyId;
        return this.http.get<JobseekerDto[]>(reqUrl);
    }
}