export interface JobseekerDto{

    id: number,
    name: string,
    picture: string,
    introduction: string, 

}