import { DrivingLicence } from "src/app/shared/enums/driving-licence.enum";
import { LanguageType } from "src/app/shared/enums/language-type.enum";
import { LevelType } from "src/app/shared/enums/level-type.enum";
import { QualityType } from "src/app/shared/enums/quality.enum";

export interface JobseekerFilter{

    companyId: number,
    youngerThan: Date | null,
    olderThan: Date | null,
    language: LanguageType | null,
    level: LevelType | null,
    drivingLicence: DrivingLicence | null,
    qualityType: QualityType | null,

}