import { createReducer, on } from "@ngrx/store";
import { ManageJobseekerActions } from ".";
import { JobseekerDto } from "../model/jobseeker-dto";

export const ManageJobseekersFeatureKey = 'manage-jobseekers';

export interface ManageJobseekersState {
    jobseekers: JobseekerDto | null; 
    matchedJobseekers: JobseekerDto[];
}

export const initialState: ManageJobseekersState = {
    jobseekers: null,
    matchedJobseekers: []
}

export const ManageJobseekersReducer = createReducer(
    initialState,
    on(ManageJobseekerActions.pageJobseekersSuccess, (state, action) => ({
        ...state,
        jobseekers: action.jobseekersDto
    })),
    on(ManageJobseekerActions.getMatchedJobseekersSuccess, (state, action) => ({
        ...state,
        matchedJobseekers: action.jobseekerDto
    })),
);