import { createFeatureSelector, createSelector } from "@ngrx/store";
import { ManageJobseekersFeatureKey, ManageJobseekersState } from "./manage-jobseekers.reducers";


const selectManageJobseekersState = createFeatureSelector<ManageJobseekersState>(ManageJobseekersFeatureKey);

export const selectJobseekers = createSelector(selectManageJobseekersState, (state) => state.jobseekers);

export const selectMatchedJobseekers = createSelector(selectManageJobseekersState, (state) => state.matchedJobseekers);