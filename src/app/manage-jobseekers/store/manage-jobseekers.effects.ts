import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import { map, switchMap } from "rxjs";
import { MatchService } from "src/app/jobseeker-applications/services/match.service";
import { ManageJobseekerActions } from ".";
import { ManageJobseekersService } from "../services/manage-jobseekers.service";

@Injectable()
export class ManageJobseekersEffects {

    pageJobseekers$ = createEffect(() => this.actions$
    .pipe(
        ofType(ManageJobseekerActions.pageJobseekers),
        switchMap(action => 
            this.manageJobseekersService.pageJobseekers( action.jobseekerFilter, action.paging)
            .pipe(
                map(jobseekersDto => ManageJobseekerActions.pageJobseekersSuccess({jobseekersDto}))
            ))
    ));

    companyAcceptJobseeker$ = createEffect(() => this.actions$
    .pipe(
        ofType(ManageJobseekerActions.companyAcceptJobseeker),
        switchMap(action => 
            this.matchService.companyAcceptJobseeker( action.simpleMatchDto)
            .pipe(
                map(() => ManageJobseekerActions.companyAcceptJobseekerSuccess())
            ))
    ));

    companyRejectJobseeker$ = createEffect(() => this.actions$
    .pipe(
        ofType(ManageJobseekerActions.companyRejectJobseeker),
        switchMap(action => 
            this.matchService.companyRejectJobseeker( action.simpleMatchDto)
            .pipe(
                map(() => ManageJobseekerActions.companyRejectJobseekerSuccess())
            ))
    ));

    getMatchedJobseekers$ = createEffect(() => this.actions$
    .pipe(
        ofType(ManageJobseekerActions.getMatchedJobseekers),
        switchMap(action =>
            this.manageJobseekersService.getMatchedJobseekers(action.companyId)
            .pipe(
                map(jobseekerDto => ManageJobseekerActions.getMatchedJobseekersSuccess({jobseekerDto}))
            ))
    ));


    constructor(
        private actions$: Actions,
        private manageJobseekersService: ManageJobseekersService,
        private matchService : MatchService
    ){}

}