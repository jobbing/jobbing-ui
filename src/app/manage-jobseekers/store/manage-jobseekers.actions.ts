import { createAction, props } from "@ngrx/store";
import { FailData } from "src/app/core/model/fail-message.model";
import { SimpleMatchDto } from "src/app/jobseeker-applications/model/simple-match-dto";
import { JobseekerDto } from "../model/jobseeker-dto";
import { JobseekerFilter } from "../model/jobseeker-filter";

const prefix = '[manage-jobseekers]';

export const pageJobseekers = createAction(
    `${prefix} page jobseekers`,
    props<{ jobseekerFilter: JobseekerFilter, paging: any }>()
);

export const pageJobseekersSuccess = createAction(
    `${prefix} page jobseekers success`,
    props<{ jobseekersDto: JobseekerDto }>()
);

export const pageJobseekersFail = createAction(
    `${prefix} page jobseekers fail`,
    props<{ failData: FailData }>()
);

export const companyRejectJobseeker = createAction(
    `${prefix} company reject jobseeker`,
    props<{ simpleMatchDto: SimpleMatchDto}>()
);

export const companyRejectJobseekerSuccess = createAction(
    `${prefix} company reject jobseeker success`,
);

export const companyRejectJobseekerFail = createAction(
    `${prefix} company reject jobseeker fail`,
    props<{ failData: FailData }>()
);

export const companyAcceptJobseeker = createAction(
    `${prefix} company accept jobseeker`,
    props<{ simpleMatchDto: SimpleMatchDto}>()
);

export const companyAcceptJobseekerSuccess = createAction(
    `${prefix} company accept jobseeker success`,
);

export const companyAcceptJobseekerFail = createAction(
    `${prefix} company accept jobseeker fail`,
    props<{ failData: FailData }>()
);

export const getMatchedJobseekers = createAction(
    `${prefix} get matched jobseekers`,
    props<{  companyId: number }>()
);

export const getMatchedJobseekersSuccess = createAction(
    `${prefix} get matched jobseekers success`,
    props<{ jobseekerDto: JobseekerDto[] }>()
);

export const getMatchedJobseekersFail = createAction(
    `${prefix} get matched jobseekers fail`,
    props<{ failData: FailData }>()
);