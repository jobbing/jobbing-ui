import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundJobseekersComponent } from './found-jobseekers.component';

describe('FoundJobseekersComponent', () => {
  let component: FoundJobseekersComponent;
  let fixture: ComponentFixture<FoundJobseekersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoundJobseekersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundJobseekersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
