import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { JobseekerDto } from '../../model/jobseeker-dto';
import { fromManageJobseekersStore, ManageJobseekerActions, ManageJobseekerSelectors } from '../../store';

@Component({
  selector: 'app-found-jobseekers',
  templateUrl: './found-jobseekers.component.html',
  styleUrls: ['./found-jobseekers.component.scss']
})
export class FoundJobseekersComponent implements OnInit {

  jobseekers$: Observable<JobseekerDto[]>;
  myDatas$: Observable<GetMyDatasDto | null>;
  id!: number;

  constructor(
    private router: Router,
    private store: Store<fromManageJobseekersStore.ManageJobseekersState>,
    private authStore: Store<fromAuthStore.AuthState>,
  ) { 
    this.myDatas$ = this.authStore.pipe(select(AuthSelectors.selectMyDatas));
    this.jobseekers$ = this.store.pipe(select(ManageJobseekerSelectors.selectMatchedJobseekers));
  }

  ngOnInit(): void {
    this.getId();
    this.store.dispatch(
      ManageJobseekerActions.getMatchedJobseekers({
        companyId: this.id
      })
    )
  }

  NavigateToFindJobseekers(){
    this.router.navigate(["manage-jobseekers", "find-jobseekers", "company"]);
  }

  NavigateToFoundJobseekers(){
    this.router.navigate(["manage-jobseekers", "found-jobseekers", "company"]);
  }

  private getId(){
    this.myDatas$.subscribe(event => this.id = event?.companyId!)
  }
}
