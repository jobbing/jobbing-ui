import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FindJobseekersComponent } from './find-jobseekers.component';

describe('FindJobseekersComponent', () => {
  let component: FindJobseekersComponent;
  let fixture: ComponentFixture<FindJobseekersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FindJobseekersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FindJobseekersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
