import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AuthSelectors, fromAuthStore } from 'src/app/core/auth/store';
import { GetMyDatasDto } from 'src/app/core/model/get-my-datas-dto';
import { JobseekerDto } from '../../model/jobseeker-dto';
import { fromManageJobseekersStore, ManageJobseekerActions, ManageJobseekerSelectors } from '../../store';

@Component({
  selector: 'app-find-jobseekers',
  templateUrl: './find-jobseekers.component.html',
  styleUrls: ['./find-jobseekers.component.scss']
})
export class FindJobseekersComponent implements OnInit {

  myData$: Observable<GetMyDatasDto | null>;
  jobseeker$: Observable<JobseekerDto | null>;
  companyId!: number;
  olderThan!: Date | null;
  youngerThan!: Date | null;

  constructor(
    private authStore: Store<fromAuthStore.AuthState>,
    private store: Store<fromManageJobseekersStore.ManageJobseekersState>,
    private router: Router,
    private datePipe: DatePipe
  ) { 
    this.myData$ = this.authStore.pipe(select(AuthSelectors.selectMyDatas));
    this.jobseeker$ = this.store.pipe(select(ManageJobseekerSelectors.selectJobseekers));
  }

  ngOnInit(): void {
    this.convertNumToDate(20);
    this.getMyId();
  }

  findJobseekerEvent(form: any){
    this.olderThan = this.convertNumToDate(form.olderThan);
    this.youngerThan = this.convertNumToDate(form.youngerThan);
    if(form.youngerThan > form.olderThan || (form.youngerThan == null && form.olderThan == null) )
      
      if(form.youngerThan == null){
        this.youngerThan = null;
      }

      if(form.olderThan == null){
        this.olderThan = null;
      }

      this.store.dispatch(
        ManageJobseekerActions.pageJobseekers({
          jobseekerFilter:{
            companyId: 1,
            drivingLicence: form.drivingLicence,
            language: form.language,
            level: form.level,
            olderThan: this.olderThan,
            youngerThan: this.youngerThan,
            qualityType: form.quality
          },
          paging:{
            pageStart: 0,
            pageSize: 1,
          }
        })
      )
  }



  acceptJobseekerEvent(jobseekerId: number){
    this.store.dispatch(
      ManageJobseekerActions.companyAcceptJobseeker({
        simpleMatchDto:{
          companyId: this.companyId,
          jobseekerId: jobseekerId
      }})
    )
  }

  rejectJobseekerEvent(jobseekerId: number){
    this.store.dispatch(
      ManageJobseekerActions.companyRejectJobseeker({
        simpleMatchDto:{
          companyId: this.companyId,
          jobseekerId: jobseekerId
      }})
    )
  }

  navigateToFindJobseekers(){
    this.router.navigate(["manage-jobseekers", "find-jobseekers", "company"]);
  }

  navigateToFoundJobseekers(){
    this.router.navigate(["manage-jobseekers", "found-jobseekers", "company"]);
  }

  private getMyId(){
    this.myData$.subscribe(event => this.companyId = event?.companyId!)
  }

  private convertNumToDate(number: number){
    let currentDate = new Date();
    let currentDateString = this.datePipe.transform(currentDate, 'yyyy-MM-dd')
    let currentDateSplit = currentDateString!.split("-");
    let currentYear = +currentDateSplit[0];
    let resultYear = currentYear-number;
    let resultString = resultYear + "-" + currentDateSplit[1] + "-" + currentDateSplit[2];

    return new Date(resultString);
    
  }


}
