import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { JobseekerDto } from '../../model/jobseeker-dto';

@Component({
  selector: 'app-found-jobseeker-one',
  templateUrl: './found-jobseeker-one.component.html',
  styleUrls: ['./found-jobseeker-one.component.scss']
})
export class FoundJobseekerOneComponent implements OnInit {

  @Input() jobseeker!: JobseekerDto;

  constructor(
    private router: Router,
  ) { }

  ngOnInit(): void {
  }

  openJobseeker(){
    this.router.navigate(["profile","jobseeker",this.jobseeker.id,"company"]);
  }

}
