import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FoundJobseekerOneComponent } from './found-jobseeker-one.component';

describe('FoundJobseekerOneComponent', () => {
  let component: FoundJobseekerOneComponent;
  let fixture: ComponentFixture<FoundJobseekerOneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FoundJobseekerOneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FoundJobseekerOneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
