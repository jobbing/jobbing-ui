import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { JobseekerDto } from '../../model/jobseeker-dto';

@Component({
  selector: 'app-page-block',
  templateUrl: './page-block.component.html',
  styleUrls: ['./page-block.component.scss']
})
export class PageBlockComponent implements OnInit {

  @Output() rejectJobseekerEvent: EventEmitter<number>;
  @Output() acceptJobseekerEvent: EventEmitter<number>;
  @Input() jobseeker!: JobseekerDto | null;

  jobseeekerId = 2;

  constructor(
    private router: Router,
  ) { 
    this.rejectJobseekerEvent = new EventEmitter();
    this.acceptJobseekerEvent = new EventEmitter();
  }

  ngOnInit(): void {
  }

  rejectJobseeker(){
    this.rejectJobseekerEvent.emit(this.jobseeekerId);
  }

  acceptJobseeker(){
    this.acceptJobseekerEvent.emit(this.jobseeekerId);
  }

  openJobseeker(){
    this.router.navigate(["profile","jobseeker",this.jobseeekerId,"company"])
  }

}
