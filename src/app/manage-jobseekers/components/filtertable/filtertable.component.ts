import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { LanguageType } from 'src/app/shared/enums/language-type.enum';
import { LevelType } from 'src/app/shared/enums/level-type.enum';
import { WorkCategory } from 'src/app/shared/enums/work-category.enum';
import { WorkingType } from 'src/app/shared/enums/work-type.enum';

@Component({
  selector: 'app-filtertable',
  templateUrl: './filtertable.component.html',
  styleUrls: ['./filtertable.component.scss']
})
export class FiltertableComponent implements OnInit {

  @Output() findJobseekerEvent: EventEmitter<any>;

  jobseekerFilterForm!: FormGroup;
  languageList = Object.keys(LanguageType);
  levelList = Object.keys(LevelType);
  categoryList = Object.keys(WorkCategory);
  typeList = Object.keys(WorkingType);

  constructor(
    private formBuilder: FormBuilder,
  ) {
    this.createJobseekerFilterForm();
    this.findJobseekerEvent = new EventEmitter();
   }

  ngOnInit(): void {
  }

  
  findJobseekers(){
    this.findJobseekerEvent.emit(this.jobseekerFilterForm.value);
  }

  private createJobseekerFilterForm(){
    this.jobseekerFilterForm = this.formBuilder.group({
      youngerThan: new FormControl(null),
      olderThan: new FormControl(null),
      language: new FormControl(null),
      level: new FormControl(null),
      category: new FormControl(null),
      type: new FormControl(null),
    })
  }

}
