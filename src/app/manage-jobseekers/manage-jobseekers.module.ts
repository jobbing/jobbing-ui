import { CommonModule, DatePipe } from "@angular/common";
import { NgModule } from "@angular/core";
import { HeaderComponent } from "../shared/header/header.component";
import { SharedModule } from "../shared/shared.module";
import { ReactiveFormsModule } from "@angular/forms";
import { FindJobseekersComponent } from './containers/find-jobseekers/find-jobseekers.component';
import { FiltertableComponent } from './components/filtertable/filtertable.component';
import { PageBlockComponent } from './components/page-block/page-block.component';
import { ManageJobseekersService } from "./services/manage-jobseekers.service";
import { ManageJobseekersRoutingModule } from "./manage-jobseekers-routing.module";
import { StoreModule } from "@ngrx/store";
import { fromManageJobseekersStore } from "./store";
import { EffectsModule } from "@ngrx/effects";
import { ManageJobseekersEffects } from "./store/manage-jobseekers.effects";
import { FoundJobseekersComponent } from "./containers/found-jobseekers/found-jobseekers.component";
import { FoundJobseekerOneComponent } from './components/found-jobseeker-one/found-jobseeker-one.component';
import { MatchService } from "../jobseeker-applications/services/match.service";


@NgModule({
    imports:[
        ReactiveFormsModule,
        ManageJobseekersRoutingModule,
        SharedModule,
        CommonModule,
        StoreModule.forFeature(
            fromManageJobseekersStore.ManageJobseekersFeatureKey,
            fromManageJobseekersStore.ManageJobseekersReducer
        ),
        EffectsModule.forFeature([ManageJobseekersEffects]),
    ],
    declarations:[
        FindJobseekersComponent,
        FiltertableComponent,
        PageBlockComponent,
        FoundJobseekersComponent,
        FoundJobseekerOneComponent,
  ],
    providers:[
        DatePipe,
        MatchService,
        ManageJobseekersService,
        HeaderComponent,
    ]
})

export class ManageJobseekersModule{ }