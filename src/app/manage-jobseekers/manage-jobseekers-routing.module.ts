import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { FindJobseekersComponent } from "./containers/find-jobseekers/find-jobseekers.component";
import { FoundJobseekersComponent } from "./containers/found-jobseekers/found-jobseekers.component";

const routes: Routes = [
    { path: 'find-jobseekers/company', component: FindJobseekersComponent},
    { path: 'found-jobseekers/company', component: FoundJobseekersComponent},
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class ManageJobseekersRoutingModule {}