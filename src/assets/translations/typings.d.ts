/*
 * Extra typings definitions
 */

// Allow .json files imports
declare module '*.json';

declare module 'pdfmake/build/pdfmake.js';
declare module 'pdfmake/build/vfs_fonts.js';
declare module 'file-saver';

// SystemJS module definition
declare var module: NodeModule;
interface NodeModule {
  id: string;
}
